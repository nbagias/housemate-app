# Housemate App
The Housemate's configuration application on Android.

Version 0.2

## Build with

##### Android Studio

##### Core libraries

* [Retrofit](https://github.com/square/retrofit) - API calls
* [Moshi](https://github.com/square/moshi) - JSON parser/ORM
* [Room](https://developer.android.com/topic/libraries/architecture/room.html) - Database storage/ORM
* [Stetho](https://github.com/facebook/stetho) - Debugging

##### UI libraries
Mostly widgets from the Android Support Library and

* [Calligraphy](https://github.com/chrisjenx/Calligraphy) - Fonts
* [FloatingActionButton](https://github.com/Clans/FloatingActionButton) - Custom floating button
* [SwitchButton](https://github.com/zcweng/SwitchButton) - Custom switch button

## Authors
 Nikos Bagias - email: bagias@entranet.gr