package com.entranet.housemate.housemateapp.ui.main.actions.adapters;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckedTextView;

import com.entranet.housemate.housemateapp.R;
import com.entranet.housemate.housemateapp.data.db.entity.DeviceEntity;
import com.entranet.housemate.housemateapp.data.helpers.ListMenu;

import java.util.ArrayList;
import java.util.List;

/**
 * Adapter class for populating actor's list.
 * Menu items are wrapped {@link DeviceEntity}.
 */

public class DeviceOptionsAdapter extends  RecyclerView.Adapter<DeviceOptionsAdapter.ActorItemViewHolder> {

    private final List<ListMenu.DeviceItem> devices;

    public DeviceOptionsAdapter(List<ListMenu.DeviceItem> devices) {
        this.devices = devices;
    }

    @NonNull
    @Override
    public ActorItemViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new ActorItemViewHolder(LayoutInflater.from(parent.getContext())
                .inflate(R.layout.menuitem_actor, parent, false));
    }

    @Override
    public void onBindViewHolder(@NonNull ActorItemViewHolder holder, int position) {
        ListMenu.DeviceItem item = devices.get(position);
        holder.cxtTitle.setText(item.getName());
        holder.cxtTitle.setChecked(item.isSelected());
    }

    @Override
    public int getItemCount() {
        return devices.size();
    }

    public List<String> getSelectedDeviceIds() {
        List<String> deviceIDs = new ArrayList<>();
        for (ListMenu.DeviceItem item: devices) {
            if (item.isSelected()) {
                deviceIDs.add(item.getId());
            }
        }
        return deviceIDs;
    }

    public void updateMenu(boolean[] actorsSelection) {
        for (int i = 0; i < devices.size(); i++) {
            devices.get(i).setSelected(actorsSelection[i]);
        }
    }

    public boolean[] getCurrentState() {
        boolean[] selectedState = new boolean[devices.size()];
        for (int i = 0; i < devices.size(); i++) {
            selectedState[i] = devices.get(i).isSelected();
        }
        return selectedState;
    }

    class ActorItemViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        private final CheckedTextView cxtTitle;

        ActorItemViewHolder(View itemView) {
            super(itemView);
            cxtTitle = itemView.findViewById(R.id.menu_item_actor_title);
            cxtTitle.setOnClickListener(this);
        }

        @Override
        public void onClick(View v) {
            ListMenu.DeviceItem entity = devices.get(getAdapterPosition());
            // Toggle selection.
            entity.setSelected(!cxtTitle.isChecked());
            cxtTitle.setChecked(!cxtTitle.isChecked());
        }
    }
}

