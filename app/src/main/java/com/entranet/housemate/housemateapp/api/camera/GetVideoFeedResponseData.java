package com.entranet.housemate.housemateapp.api.camera;

import com.entranet.housemate.housemateapp.api.HomeApi;
import com.squareup.moshi.Json;

/**
 * Nested model class for API response {@link HomeApi#getVideoFeed}
 */

public class GetVideoFeedResponseData {
    @Json(name = "ip")
    private String url;

    String getUrl() { return url; }

    public void setUrl(String url) { this.url = url; }
}
