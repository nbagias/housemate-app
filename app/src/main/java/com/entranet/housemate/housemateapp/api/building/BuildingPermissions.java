package com.entranet.housemate.housemateapp.api.building;

import android.arch.persistence.room.ColumnInfo;

public class BuildingPermissions {
    @ColumnInfo(name = "perm_switches")
    private Boolean canAccessSwitches;
    @ColumnInfo(name = "perm_scenes")
    private Boolean canModifyScenes;
    @ColumnInfo(name = "perm_camera")
    private Boolean canAccessCamera;
    @ColumnInfo(name = "perm_actions")
    private Boolean canModifyActions;
    @ColumnInfo(name = "perm_emergency")
    private Boolean canReceiveEmergencyNotification;

    public Boolean getCanAccessSwitches() { return canAccessSwitches; }

    public void setCanAccessSwitches(Boolean canAccessSwitches) { this.canAccessSwitches = canAccessSwitches; }

    public Boolean getCanModifyScenes() { return canModifyScenes; }

    public void setCanModifyScenes(Boolean canModifyScenes) { this.canModifyScenes = canModifyScenes; }

    public Boolean getCanAccessCamera() { return canAccessCamera; }

    public void setCanAccessCamera(Boolean canAccessCamera) { this.canAccessCamera = canAccessCamera; }

    public Boolean getCanModifyActions() { return canModifyActions; }

    public void setCanModifyActions(Boolean canModifyActions) { this.canModifyActions = canModifyActions; }

    public Boolean getCanReceiveEmergencyNotification() { return canReceiveEmergencyNotification; }

    public void setCanReceiveEmergencyNotification(Boolean canReceiveEmergencyNotification) {
        this.canReceiveEmergencyNotification = canReceiveEmergencyNotification;
    }
}
