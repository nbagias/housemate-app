package com.entranet.housemate.housemateapp.data.db.dao;

import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.OnConflictStrategy;
import android.arch.persistence.room.Query;
import android.arch.persistence.room.Update;

import com.entranet.housemate.housemateapp.data.db.entity.SwitchEntity;

import java.util.List;

/**
 * Dao class for accessing the {@link SwitchEntity} table.
 */

@Dao
public interface SwitchDao {
    @Query("SELECT * FROM switches WHERE type='switch' AND building_id=:buildingId")
    List<SwitchEntity> getSwitches(String buildingId);

    @Query("SELECT * FROM switches WHERE type='thermostat' AND building_id=:buildingId LIMIT 1")
    SwitchEntity getThermostat(String buildingId);

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    void insertAllSwitches(List<SwitchEntity> switches);

    @Query("DELETE FROM switches")
    void deleteAll();

    @Update
    void updateSwitch(SwitchEntity updatedSwitch);

    @Query("UPDATE switches SET name=:newName WHERE switch_id =:switchId")
    void updateSwitchNameById(String newName, String switchId);

}
