package com.entranet.housemate.housemateapp.ui.main.actions.callbacks;

import com.entranet.housemate.housemateapp.data.db.entity.ActionEntity;
import com.entranet.housemate.housemateapp.data.helpers.ListMenu;
import com.entranet.housemate.housemateapp.ui.main.actions.ActionEditorActivity;

import java.util.List;

/**
 * Interface for returning control to the caller.
 * Implemented by {@link ActionEditorActivity}
 */

public interface ActionEditorCallback {

    void onScenesLoaded(List<ListMenu.SceneItem> scenes, boolean isInitial, ActionEntity action);

    void onDeviceLoaded(List<ListMenu.DeviceItem> devices, boolean isInitial, ActionEntity action);

    void onActionCreated();

    void onActionCreationFailed(int resId);
}
