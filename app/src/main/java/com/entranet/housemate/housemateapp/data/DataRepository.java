package com.entranet.housemate.housemateapp.data;


import android.content.Context;

import com.entranet.housemate.housemateapp.api.AdminApi;
import com.entranet.housemate.housemateapp.api.ApiClient;
import com.entranet.housemate.housemateapp.api.HomeApi;
import com.entranet.housemate.housemateapp.api.SettingsApi;
import com.entranet.housemate.housemateapp.api.building.Building;
import com.entranet.housemate.housemateapp.api.building.BuildingData;
import com.entranet.housemate.housemateapp.api.building.BuildingPermissions;
import com.entranet.housemate.housemateapp.api.login.CredentialsBody;
import com.entranet.housemate.housemateapp.data.db.AppDatabase;
import com.entranet.housemate.housemateapp.data.db.entity.ActionEntity;
import com.entranet.housemate.housemateapp.data.db.entity.DeviceEntity;
import com.entranet.housemate.housemateapp.data.db.entity.GuestEntity;
import com.entranet.housemate.housemateapp.data.db.entity.SceneEntity;
import com.entranet.housemate.housemateapp.data.db.entity.SwitchEntity;
import com.entranet.housemate.housemateapp.data.db.entity.UserEntity;
import com.entranet.housemate.housemateapp.data.helpers.Constants;
import com.entranet.housemate.housemateapp.data.repos.ActionRepository;
import com.entranet.housemate.housemateapp.data.repos.BuildingRepository;
import com.entranet.housemate.housemateapp.data.repos.SceneRepository;
import com.entranet.housemate.housemateapp.data.repos.SettingsRepository;
import com.entranet.housemate.housemateapp.data.repos.SwitchRepository;
import com.entranet.housemate.housemateapp.data.repos.UserRepository;
import com.entranet.housemate.housemateapp.ui.init.StarterOpsInterface;
import com.entranet.housemate.housemateapp.ui.init.pin.CheckPinCallback;
import com.entranet.housemate.housemateapp.ui.init.splash.CheckUserStatusCallback;
import com.entranet.housemate.housemateapp.ui.main.actions.callbacks.ActionEditorCallback;
import com.entranet.housemate.housemateapp.ui.main.actions.callbacks.ActionRefreshCallback;
import com.entranet.housemate.housemateapp.ui.main.buildings.AddBuildingCallback;
import com.entranet.housemate.housemateapp.ui.main.buildings.GetBuildingsDbCallback;
import com.entranet.housemate.housemateapp.ui.main.custom.CheckAccessCallback;
import com.entranet.housemate.housemateapp.ui.main.settings.accessrules.AddGuestCallback;
import com.entranet.housemate.housemateapp.ui.main.settings.accessrules.LoadGuestsCallback;
import com.entranet.housemate.housemateapp.ui.main.settings.location.LocationSettingsCallback;
import com.entranet.housemate.housemateapp.ui.main.settings.news.NewsCallback;
import com.entranet.housemate.housemateapp.ui.main.smarthome.camera.GetVideoUrlCallback;
import com.entranet.housemate.housemateapp.ui.main.smarthome.camera.LoadDevicesCallback;
import com.entranet.housemate.housemateapp.ui.main.smarthome.scenes.LoadSwitchesCallback;
import com.entranet.housemate.housemateapp.ui.main.smarthome.scenes.SaveSceneCallback;
import com.entranet.housemate.housemateapp.ui.main.smarthome.scenes.SceneActionsCallback;

import java.util.List;

import retrofit2.Retrofit;

/**
 *
 * Repository class for accessing other repositories.
 *
 * Acts as a mediator for the data layer.
 *
 */

public class DataRepository {

    private static DataRepository instance;
    private AppExecutors appExecutors;

    // User-domain parameters, useful for letting the user change buildings dynamically.
    private String currentBuildingId;
    private UserEntity activeUser;
    private BuildingPermissions permissions;

    private SwitchRepository switchRepository;
    private SceneRepository sceneRepository;
    private ActionRepository actionRepository;
    private UserRepository userRepository;
    private BuildingRepository buildingRepository;
    private SettingsRepository settingsRepository;

    private DataRepository(Context appContext) {
        Retrofit retrofit = ApiClient.getClient(appContext).getRetrofit();
        HomeApi homeApi = retrofit.create(HomeApi.class);
        AdminApi adminApi = retrofit.create(AdminApi.class);
        SettingsApi settingsApi = retrofit.create(SettingsApi.class);

        AppDatabase database = AppDatabase.getInstance(appContext);
        this.appExecutors = new AppExecutors();
        this.switchRepository = new SwitchRepository(homeApi, database.switchDao(), appExecutors);
        this.sceneRepository = new SceneRepository(homeApi, database.sceneDao(), appExecutors);
        this.actionRepository = new ActionRepository(homeApi, database.actionDao(), appExecutors);
        this.userRepository = new UserRepository(adminApi, database.userDao(), appExecutors);
        this.buildingRepository = new BuildingRepository(adminApi, database.buildingDao(), appExecutors);
        this.settingsRepository = new SettingsRepository(settingsApi, database.buildingDao(), appExecutors);
    }

    public static synchronized DataRepository getInstance(Context appContext) {
        if (instance == null) {
            instance = new DataRepository(appContext);
        }
        return instance;
    }

    public String getCurrentBuildingId() { return currentBuildingId; }

    public void setCurrentBuildingId(String buildingId) {
        this.currentBuildingId = buildingId;
        appExecutors.getDiskIO().execute(() -> {
            permissions = buildingRepository.getPermissions(currentBuildingId);
            buildingRepository.setDefault(currentBuildingId);
        });
    }

    public void setActiveUser() { appExecutors.getDiskIO().execute(() -> activeUser = userRepository.getUser()); }

    public void checkPinCode(String userInput, CheckPinCallback callback) { userRepository.checkPin(userInput, callback); }

    public void checkPinStatus(CheckPinCallback callback) { userRepository.checkPinStatus(callback); }

    public void setUserPin(String userPin) { userRepository.setPin(userPin); }

    public void checkUserStatus(CheckUserStatusCallback callback) { userRepository.checkStatus(callback); }

    public void registerUser(CredentialsBody body, StarterOpsInterface callback) {userRepository.register(body, callback); }

    public void loginUser(CredentialsBody body, StarterOpsInterface callback) { userRepository.login(body, callback);}

    // SWITCHES
    public void getSwitches(LoadSwitchesCallback callback) {
        switchRepository.load(currentBuildingId, callback);
    }

    public void refreshSwitches(LoadSwitchesCallback callback) {
        switchRepository.refresh(activeUser.getToken(),currentBuildingId, callback);
    }

    public void updateSwitchState(SwitchEntity updatedSwitch) {
        switchRepository.update(updatedSwitch, activeUser.getToken(), currentBuildingId);
    }

    public void changeSwitchName(String newName, String switchId) {
        switchRepository.updateName(newName, switchId, activeUser.getToken(), currentBuildingId);
    }

    public SwitchEntity getThermostat() {
        return switchRepository.getThermostat(currentBuildingId);
    }

    public void updateThermostat(int newValue) {
        switchRepository.updateThermostat(newValue, activeUser.getToken(), currentBuildingId);
    }

    public void getSwitchesForScene(String sceneName, LoadSwitchesCallback callback) {
        appExecutors.getDiskIO().execute(() -> {
            // First get the scene.
            SceneEntity scene = sceneRepository.getByName(sceneName, currentBuildingId);
            // Modify the switches so that they match the scene's list.
            switchRepository.filterForScene(scene, currentBuildingId, callback);
        });
    }

    // SCENES
    public void saveScene(String sceneName, List<SwitchEntity> sceneList, String mode, SaveSceneCallback callback) {
        sceneRepository.saveScene(sceneName, sceneList, mode, activeUser.getToken(), currentBuildingId, callback);
    }

    public void deleteSceneByName(String sceneName, SceneActionsCallback callback) {
        appExecutors.getDiskIO().execute(() -> {
            sceneRepository.delete(sceneName, activeUser.getToken(), currentBuildingId);
            getSceneNames(callback);
        });
    }

    public void deleteAllScenes() {
        sceneRepository.deleteAll(activeUser.getToken(), currentBuildingId);
    }

    public void getSceneNames(SceneActionsCallback callback) {
        sceneRepository.load(currentBuildingId, callback);
    }

    public void activateScene(String sceneName, SceneActionsCallback callback) {
        sceneRepository.activate(sceneName, activeUser.getToken(), currentBuildingId, callback);
    }

    public void getScenesForAction(String mode, String actionId, boolean isInitial, ActionEditorCallback callback) {
        appExecutors.getDiskIO().execute(() -> {
            List<SceneEntity> scenes = sceneRepository.get(currentBuildingId);
            actionRepository.getScenesForAction(scenes, mode, actionId, isInitial, callback);
        });
    }

    // BUILDINGS
    public void getBuildingsFromDb(GetBuildingsDbCallback callback) {
        buildingRepository.load(callback);
    }

    public void addUserToBuilding(String buildingId, String buildingName, AddBuildingCallback callback) {
        buildingRepository.addUser(buildingId,
                buildingName,
                activeUser.getToken(),
                activeUser.getEmail(),
                callback);
    }

    public void addGuestToBuilding(String guestEmail, AddGuestCallback callback) {
        buildingRepository.addGuest(guestEmail, activeUser.getToken(), currentBuildingId, callback);
    }

    // SETTINGS
    public void getNumberOfNews(NewsCallback callback) {
        settingsRepository.getNews(currentBuildingId, callback);
    }

    public void setNumberOfNews(int num) {
        settingsRepository.setNews(num, currentBuildingId, activeUser.getToken());
    }

    public void loadLocationSettings(LocationSettingsCallback callback) {
        settingsRepository.loadLocation(currentBuildingId, callback);
    }

    public void setTimezone(String timezone) {
        settingsRepository.setTimezone(timezone, currentBuildingId, activeUser.getToken());
    }

    public void setLocation (String location) {
        settingsRepository.setLocation(location, currentBuildingId, activeUser.getToken());
    }

    // GUESTS
    public void loadGuests(LoadGuestsCallback callback) {
        buildingRepository.loadGuests(currentBuildingId, callback);
    }

    public void refreshGuests(LoadGuestsCallback callback) {
        buildingRepository.refreshGuests(currentBuildingId, activeUser.getToken(), callback);
    }

    public void removeGuest(GuestEntity guest, LoadGuestsCallback callback) {
        buildingRepository.removeGuest(guest, currentBuildingId, activeUser.getToken(), callback);
    }

    public void updatePermissions(GuestEntity guest) {
        buildingRepository.updatePermissions(guest, currentBuildingId, activeUser.getToken());
    }

    // ACTIONS
    public List<ActionEntity> getActions(String type) {
        return actionRepository.load(type, currentBuildingId);
    }

    public void deleteAction(String type, String actionId, ActionRefreshCallback callback) {
        actionRepository.delete(type, actionId, currentBuildingId, activeUser.getToken(), callback);
    }

    public void addAction(ActionEntity action, String mode, ActionEditorCallback callback) {
        actionRepository.update(action, mode, currentBuildingId, activeUser.getToken(), callback);
    }

    public void refreshActions(String type, ActionRefreshCallback callback) {
        actionRepository.refresh(type, currentBuildingId, activeUser.getToken(), callback);
    }

    public void updateAction(ActionEntity action) {
        actionRepository.update(action, Constants.MODE_EDIT, currentBuildingId, activeUser.getToken(), null);
    }

    public void getDevicesForAction(String mode, String actionId, boolean isInitial, ActionEditorCallback callback) {
        appExecutors.getDiskIO().execute(() -> {
            List<DeviceEntity> devices = buildingRepository.getDevices(currentBuildingId);
            actionRepository.getDevicesForAction(devices, mode, actionId, isInitial, callback);
        });
    }

    // DEVICES
    public void getDeviceList(LoadDevicesCallback callback) {
        buildingRepository.loadDevices(currentBuildingId, callback);
    }

    public void checkAccess(@Constants.MenuItems int id, final CheckAccessCallback callback) {
        appExecutors.getDiskIO().execute(() -> {
            boolean result = false;
            switch (id) {
                case Constants.SWITCHES:
                    result = permissions.getCanAccessSwitches();
                    break;
                case Constants.SCENES:
                    result = permissions.getCanModifyScenes();
                    break;
                case Constants.CAMERA:
                    result = permissions.getCanAccessCamera();
                    break;
                case Constants.ALARM:
                    result = permissions.getCanModifyActions();
                    break;
                case Constants.ALERT:
                    result = permissions.getCanModifyActions();
                    break;
                case Constants.SCENARIOS:
                    result = permissions.getCanModifyActions();
                    break;
                case Constants.ACCESS_RULES:
                    result = buildingRepository.isUserMaster(currentBuildingId);
                    break;
                case Constants.LOCATION:
                    result = buildingRepository.isUserMaster(currentBuildingId);
                    break;
                case Constants.NEWS:
                    result = buildingRepository.isUserMaster(currentBuildingId);
                    break;
                case Constants.NEW_DEVICE:
                    result = buildingRepository.isUserMaster(currentBuildingId);
                    break;
                case Constants.LOGOUT:
                    result = true;
                    break;
                default:
                    break;
            }
            callback.onSelectionChecked(result, id);
        });

    }

    public void getVideoFeed(String deviceId, boolean isCall, GetVideoUrlCallback callback) {
        actionRepository.getVideoFeed(deviceId, isCall, currentBuildingId, activeUser.getToken(), callback);
    }

    public void saveBuildingInfo(BuildingData data, StarterOpsInterface callback) {
        appExecutors.getDiskIO().execute(() -> {
            List<Building> buildings = data.getBuildings();
            // Check for emptiness.
            if (buildings != null && !buildings.isEmpty()) {
                for (Building building : buildings) {
                    // BUILDING -- Construct a building Entity for each building.
                    buildingRepository.add(building);
                    // Save its switches.
                    switchRepository.add(building.getSwitches(), building.getBuildingID());
                    // Save its scenes.
                    sceneRepository.add(building.getScenes(), building.getBuildingID());
                    // Save its actions.
                    actionRepository.add(building.getActions(), building.getBuildingID());
                }
            }
            callback.onSaveBuildingsCompleted();
        });
    }

    public void logout() {
        // Users.
        userRepository.truncate();
        // Buildings - includes Guests and Devices.
        buildingRepository.truncate();
        // Switches.
        switchRepository.truncate();
        // Scenes.
        sceneRepository.truncate();
        // Actions.
        actionRepository.truncate();
    }
}
