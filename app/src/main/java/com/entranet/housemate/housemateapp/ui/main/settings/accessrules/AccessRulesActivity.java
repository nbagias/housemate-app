package com.entranet.housemate.housemateapp.ui.main.settings.accessrules;

import android.app.AlertDialog;
import android.content.Context;
import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckedTextView;
import android.widget.Spinner;

import com.entranet.housemate.housemateapp.R;
import com.entranet.housemate.housemateapp.api.building.BuildingPermissions;
import com.entranet.housemate.housemateapp.data.DataRepository;
import com.entranet.housemate.housemateapp.data.db.entity.GuestEntity;

import java.util.List;

import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

public class AccessRulesActivity extends AppCompatActivity implements
        LoadGuestsCallback,
        AddGuestCallback,
        AddGuestFragment.OnAddGuestListener {

    private DataRepository repository;
    private Spinner spinner;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_access_rules);

        setupUi();
        // Repository reference.
        repository = DataRepository.getInstance(getApplicationContext());
        // Load guests.
        repository.loadGuests(this);
    }

    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }

    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }

    @Override
    public void onGuestsLoaded(final List<GuestEntity> guests) {
        if (guests != null && !guests.isEmpty()) {
            // Set the adapter for spinner.
            runOnUiThread(() -> {
                // Create the adapter.
                ArrayAdapter adapter = new ArrayAdapter<>(AccessRulesActivity.this, R.layout.access_rules_spinner_item, guests);
                adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                spinner.setAdapter(adapter);
                spinner.setOnItemSelectedListener(spinnerListener);
            });
        }
    }

    @Override
    public void onGuestInsertionSuccess() {
        // Show success message to user.
        Snackbar snackbar = Snackbar.make(findViewById(R.id.access_rules_container), R.string.access_rules_invitation_sent, Snackbar.LENGTH_SHORT);
        snackbar.show();
        // Reload guests.
        repository.refreshGuests(this);
    }

    @Override
    public void onGuestInsertionFailure(String errorMessage) {
        Snackbar snackbar = Snackbar.make(findViewById(R.id.access_rules_container), errorMessage, Snackbar.LENGTH_SHORT);
        snackbar.show();
    }

    @Override
    public void onAddGuestEmail(String email) {
        repository.addGuestToBuilding(email, this);
    }

    private void setupUi() {
        Toolbar toolbar = findViewById(R.id.toolbar);
        toolbar.setTitle("");
        setSupportActionBar(toolbar);
        ActionBar actionBar = getSupportActionBar();
        if (actionBar != null) actionBar.setDisplayHomeAsUpEnabled(true);

        // Spinner reference.
        spinner = findViewById(R.id.access_rules_guest_spinner);
        // Add listener for adding guests.
        Button btnAddGuest = findViewById(R.id.access_rules_add_guest_button);
        btnAddGuest.setOnClickListener(addGuestListener);
        Button btnDeleteGuest = findViewById(R.id.access_rules_delete_guest_button);
        btnDeleteGuest.setOnClickListener(removeGuestListener);
    }

    private void setupRules(GuestEntity guestEntity) {
        BuildingPermissions perms = guestEntity.getPermissions();
        // Switches
        CheckedTextView ctvDevices = findViewById(R.id.access_rules_switches);
        ctvDevices.setChecked(perms.getCanAccessSwitches());
        ctvDevices.setOnClickListener(checkedTextListener);
        // Camera
        CheckedTextView ctvCamera = findViewById(R.id.access_rules_camera);
        ctvCamera.setChecked(perms.getCanAccessCamera());
        ctvCamera.setOnClickListener(checkedTextListener);
        // Actions
        CheckedTextView ctvActions = findViewById(R.id.access_rules_actions);
        ctvActions.setChecked(perms.getCanModifyActions());
        ctvActions.setOnClickListener(checkedTextListener);
        // Scenes
        CheckedTextView ctvScenes= findViewById(R.id.access_rules_scenes);
        ctvScenes.setChecked(perms.getCanModifyScenes());
        ctvScenes.setOnClickListener(checkedTextListener);
        // Emergenecy
        CheckedTextView ctvEmergency = findViewById(R.id.access_rules_emergency);
        ctvEmergency.setChecked(perms.getCanReceiveEmergencyNotification());
        ctvEmergency.setOnClickListener(checkedTextListener);
    }

    private final AdapterView.OnItemSelectedListener spinnerListener = new AdapterView.OnItemSelectedListener() {
        @Override
        public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
            // Get the selected guest.
            GuestEntity selectedGuest = (GuestEntity) parent.getItemAtPosition(position);
            if (selectedGuest == null) return;
            // Update access rules.
            setupRules(selectedGuest);
        }

        @Override
        public void onNothingSelected(AdapterView<?> parent) {

        }
    };

    private final View.OnClickListener addGuestListener = v -> {
        AddGuestFragment fragment = AddGuestFragment.newInstance();
        FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
        fragment.show(ft, AddGuestFragment.class.getSimpleName());
    };

    private final View.OnClickListener checkedTextListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            // First toggle the view.
            CheckedTextView view = (CheckedTextView) v;
            if (view.isChecked()) {
                view.setChecked(false);
            } else {
                view.setChecked(true);
            }
            // Update the corresponding rule.
            GuestEntity guest = (GuestEntity) spinner.getSelectedItem();
            if (guest == null) return;
            BuildingPermissions perms = guest.getPermissions();
            switch (v.getId()) {
                case R.id.access_rules_switches:
                    perms.setCanAccessSwitches(view.isChecked());
                    break;
                case R.id.access_rules_camera:
                    perms.setCanAccessCamera(view.isChecked());
                    break;
                case R.id.access_rules_scenes:
                    perms.setCanModifyScenes(view.isChecked());
                    break;
                case R.id.access_rules_actions:
                    perms.setCanModifyActions(view.isChecked());
                    break;
                case R.id.access_rules_emergency:
                    perms.setCanReceiveEmergencyNotification(view.isChecked());
                    break;
                default:
                    return;
            }
            repository.updatePermissions(guest);
        }
    };

    private final View.OnClickListener removeGuestListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            // Get the selected guest.
            final GuestEntity selectedGuest = (GuestEntity) spinner.getSelectedItem();
            // No guests, so return.
            if (selectedGuest == null) return;
            // Ask user for confirmation.
            AlertDialog dialog = new AlertDialog.Builder(AccessRulesActivity.this)
                    .setIcon(android.R.drawable.ic_delete)
                    .setTitle("Guest removal")
                    .setMessage("Are you sure you want to remove guest " + selectedGuest.getEmail())
                    .setPositiveButton(android.R.string.yes,
                            (dialog1, whichButton) -> repository.removeGuest(selectedGuest, AccessRulesActivity.this)
                    )
                    .setNegativeButton(android.R.string.no,
                            (dialog1, whichButton) -> {
                            }
                    ).create();

            dialog.show();
        }
    };
}
