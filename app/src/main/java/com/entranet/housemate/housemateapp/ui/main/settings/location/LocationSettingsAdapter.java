package com.entranet.housemate.housemateapp.ui.main.settings.location;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.entranet.housemate.housemateapp.R;
import com.entranet.housemate.housemateapp.data.helpers.ListMenu;

import java.util.List;

/**
 * Adapter for populating the location settings.
 */

public class LocationSettingsAdapter extends RecyclerView.Adapter<LocationSettingsAdapter.LocationSettingsViewHolder> {

    private final List<ListMenu.SettingsItem> settingsList;
    private final LocationSettingsListener listener;

    LocationSettingsAdapter(@NonNull List<ListMenu.SettingsItem> settingsList, LocationSettingsListener listener) {
        this.settingsList = settingsList;
        this.listener = listener;
    }

    void updateAdapterList(String value, int position) {
        // Get and change the corresponding item.
        ListMenu.SettingsItem item = settingsList.get(position);
        item.setValue(value);
        // Update UI.
        notifyDataSetChanged();
    }

    @Override
    public LocationSettingsViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.menuitem_loc_settings, parent, false);
        return new LocationSettingsViewHolder(view);
    }

    @Override
    public void onBindViewHolder(final LocationSettingsViewHolder holder, int position) {
        ListMenu.SettingsItem item = settingsList.get(position);
        holder.title.setText(item.getTitle());
        holder.title.setCompoundDrawablesWithIntrinsicBounds(0, item.getIcon(), 0, 0);
        holder.value.setText(item.getValue());
        holder.summary.setText(item.getSummary());
    }

    @Override
    public int getItemCount() { return settingsList.size(); }

    public class LocationSettingsViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        private final LinearLayout container;
        public final TextView title;
        public final TextView value;
        final TextView summary;

        LocationSettingsViewHolder(View view) {
            super(view);
            container = view.findViewById(R.id.loc_settings_container);
            title = view.findViewById(R.id.loc_settings_title);
            value = view.findViewById(R.id.loc_settings_value);
            summary = view.findViewById(R.id.loc_settings_summary);
            container.setOnClickListener(this);
        }

        @Override
        public void onClick(View v) {
            if (null != listener) {
                listener.onLocationSettingSelected(settingsList.get(getAdapterPosition()));
            }
        }

        @Override
        public String toString() {
            return super.toString() + " '" + title.getText() + "'";
        }
    }

    public interface LocationSettingsListener {
        void onLocationSettingSelected(ListMenu.SettingsItem item);
    }
}
