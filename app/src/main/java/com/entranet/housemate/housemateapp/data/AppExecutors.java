package com.entranet.housemate.housemateapp.data;

import java.util.concurrent.Executor;
import java.util.concurrent.Executors;

/**
 * Global executor pools for disk operations.
 */

public class AppExecutors {

    private static  final int NUM_OF_THREADS = 5;

    private final Executor diskIO;

    private AppExecutors(Executor diskIO) { this.diskIO = diskIO; }

    AppExecutors() { this(Executors.newFixedThreadPool(NUM_OF_THREADS)); }

    public Executor getDiskIO() { return diskIO; }
}
