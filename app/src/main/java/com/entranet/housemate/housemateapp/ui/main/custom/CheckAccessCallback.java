package com.entranet.housemate.housemateapp.ui.main.custom;

import com.entranet.housemate.housemateapp.data.helpers.Constants;

/**
 * Interface for returning result based on access rules.
 */

public interface CheckAccessCallback {

    void onSelectionChecked(boolean isAllowed, @Constants.MenuItems int id);
}
