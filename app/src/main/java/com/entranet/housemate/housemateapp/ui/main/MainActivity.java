package com.entranet.housemate.housemateapp.ui.main;

import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.PorterDuff;
import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.support.design.widget.TabLayout;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageView;

import com.entranet.housemate.housemateapp.BuildConfig;
import com.entranet.housemate.housemateapp.R;
import com.entranet.housemate.housemateapp.data.DataRepository;
import com.entranet.housemate.housemateapp.data.db.entity.BuildingEntity;
import com.entranet.housemate.housemateapp.data.helpers.Constants;
import com.entranet.housemate.housemateapp.data.helpers.ListMenu;
import com.entranet.housemate.housemateapp.ui.init.StarterActivity;
import com.entranet.housemate.housemateapp.ui.main.actions.ActionsFragment;
import com.entranet.housemate.housemateapp.ui.main.actions.AlarmActivity;
import com.entranet.housemate.housemateapp.ui.main.actions.AlertActivity;
import com.entranet.housemate.housemateapp.ui.main.actions.ScenarioActivity;
import com.entranet.housemate.housemateapp.ui.main.buildings.BuildingFragment;
import com.entranet.housemate.housemateapp.ui.main.buildings.GetBuildingsDbCallback;
import com.entranet.housemate.housemateapp.ui.main.custom.CheckAccessCallback;
import com.entranet.housemate.housemateapp.ui.main.custom.MainMenuPagerAdapter;
import com.entranet.housemate.housemateapp.ui.main.settings.SettingsFragment;
import com.entranet.housemate.housemateapp.ui.main.settings.accessrules.AccessRulesActivity;
import com.entranet.housemate.housemateapp.ui.main.settings.location.LocationActivity;
import com.entranet.housemate.housemateapp.ui.main.settings.news.NewsActivity;
import com.entranet.housemate.housemateapp.ui.main.smarthome.SmartHomeFragment;
import com.entranet.housemate.housemateapp.ui.main.smarthome.camera.CameraActivity;
import com.entranet.housemate.housemateapp.ui.main.smarthome.scenes.ScenesActivity;
import com.entranet.housemate.housemateapp.ui.main.smarthome.switches.SwitchesActivity;
import com.github.clans.fab.FloatingActionButton;
import com.github.clans.fab.FloatingActionMenu;

import java.util.ArrayList;
import java.util.List;

import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

/**
 * Main Activity is the main application panel. It has all the available menus related to
 * Housemate control, plus provides a way to select/rename/add new Housemate Packages (for us
 * this means buildings).
 *
 */
public class MainActivity extends AppCompatActivity implements SmartHomeFragment.OnSmartHomeListener,
        ActionsFragment.OnActionsListener, SettingsFragment.OnSettingsListener,
        GetBuildingsDbCallback, CheckAccessCallback {
    // Logging tag
    private static final String TAG = MainActivity.class.getSimpleName();

    private DataRepository repository;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        // Configure UI elements.
        setupUi();
        // Get and set the available buildings.
        setupBuildings();
    }

    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }

    @Override
    public void onMenuItemSelected(ListMenu.MenuItem item) {
        if (BuildConfig.DEBUG) Log.d(TAG, "Item " + item.getTitle() + " selected");
        // First check the access.
        repository.checkAccess(item.getId(), this);
    }

    @Override
    public void onSelectionChecked(boolean isAllowed, @Constants.MenuItems int id) {
        if (isAllowed) {
            Intent intent;
            switch (id) {
                case Constants.SWITCHES:
                    intent = new Intent(this, SwitchesActivity.class);
                    break;
                case Constants.SCENES:
                    intent = new Intent(this, ScenesActivity.class);
                    break;
                case Constants.CAMERA:
                    intent = new Intent(this, CameraActivity.class);
                    break;
                case Constants.ALARM:
                    intent = new Intent(this, AlarmActivity.class);
                    break;
                case Constants.ALERT:
                    intent = new Intent(this, AlertActivity.class);
                    break;
                case Constants.SCENARIOS:
                    intent = new Intent(this, ScenarioActivity.class);
                    break;
                case Constants.ACCESS_RULES:
                    intent = new Intent(this, AccessRulesActivity.class);
                    break;
                case Constants.LOCATION:
                    intent = new Intent(this, LocationActivity.class);
                    break;
                case Constants.NEWS:
                    intent = new Intent(this, NewsActivity.class);
                    break;
                case Constants.NEW_DEVICE:
                    // Not implemented yet.
                    return;
                case Constants.LOGOUT:
                    showLogout();
                    return;
                default:
                    return;
            }
            startActivity(intent);
        } else {
            Snackbar snackbar = Snackbar.make(findViewById(R.id.main_coordinator), R.string.main_access_restricted, Snackbar.LENGTH_SHORT);
            snackbar.show();
        }
    }


    @Override
    public void onBuildingsExist(final List<BuildingEntity> buildings) {
        runOnUiThread(() -> {
            // Default to the first one, but check if there was set a default by the user.
            String selectedBuilding = buildings.get(0).getId();
            for (BuildingEntity building: buildings) {
                if (building.getDefault()) {
                    selectedBuilding = building.getId();
                    break;
                }
            }
            repository.setCurrentBuildingId(selectedBuilding);
            FloatingActionMenu menu = findViewById(R.id.main_floating_menu);
            List<String> currentTags = getCurrentFabMenuTags(menu);
            for (BuildingEntity userBuilding: buildings) {
                // Avoid duplicates.
                if (currentTags.contains(userBuilding.getId())) continue;
                // Construct a new one.
                FloatingActionButton buildingFab = new FloatingActionButton(MainActivity.this);
                if (userBuilding.getName().isEmpty()) {
                    buildingFab.setLabelText(getString(R.string.fab_menu_select) + userBuilding.getId());
                } else {
                    buildingFab.setLabelText(getString(R.string.fab_menu_select) + userBuilding.getName());
                }
                buildingFab.setImageResource(R.drawable.main_ic_building_id_white);
                buildingFab.setTag(userBuilding.getId());
                buildingFab.setColorNormalResId(R.color.orange_light);
                buildingFab.setColorPressedResId(R.color.colorPrimary);
                buildingFab.setOnClickListener(fabMenuListener);
                menu.addMenuButton(buildingFab);
            }
        });
    }

    @Override
    public void onBuildingsNone() { showBuildingFragment(); }

    private List<String> getCurrentFabMenuTags(FloatingActionMenu menu) {
        List<String> currentFabLabels = new ArrayList<>();
        for (int i = 0; i < menu.getChildCount(); i++) {
            View v = menu.getChildAt(i);
            if (v != menu.getMenuIconView() && v instanceof FloatingActionButton) {
                currentFabLabels.add((String)v.getTag());
            }
        }
        return currentFabLabels;
    }

    private void setupBuildings() {
        repository = DataRepository.getInstance(getApplicationContext());
        repository.setActiveUser();
        // Get the buildings
        repository.getBuildingsFromDb(this);
    }

    private void showBuildingFragment() {
        FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
        DialogFragment buildingFragment = BuildingFragment.newInstance();
        buildingFragment.show(ft, BuildingFragment.class.getSimpleName());
    }

    private void setupUi() {
        Toolbar toolbar = findViewById(R.id.main_toolbar);
        toolbar.setTitle("");
        setSupportActionBar(toolbar);
        // Create the adapter that will return a fragment for each of the three
        // primary sections of the activity.
        MainMenuPagerAdapter menuPagerAdapter = new MainMenuPagerAdapter(getSupportFragmentManager());

        ViewPager viewPager = findViewById(R.id.main_container);
        viewPager.setAdapter(menuPagerAdapter);

        TabLayout tabLayout = findViewById(R.id.main_tabs);
        // Setup viewpager
        tabLayout.setupWithViewPager(viewPager);
        // Note: first setup the view pager, then the tab icons!
        setupTabIcons(tabLayout);
        viewPager.addOnPageChangeListener(new TabLayout.TabLayoutOnPageChangeListener(tabLayout));
        tabLayout.addOnTabSelectedListener(tabSelectedListener);
        // Light up the selected tab.
        TabLayout.Tab selectedTab = tabLayout.getTabAt(tabLayout.getSelectedTabPosition());
        if (selectedTab != null) {
            ImageView tabImage = (ImageView) selectedTab.getCustomView();
            if (tabImage != null) tabImage.setColorFilter(Color.WHITE, PorterDuff.Mode.SRC_IN);
        }

        FloatingActionMenu menu = findViewById(R.id.main_floating_menu);
        // Close menu when user touches somewhere else.
        menu.setClosedOnTouchOutside(true);
        // Set listener for the default floating buttons.
        FloatingActionButton addButton = findViewById(R.id.floating_menu_item_add);
        addButton.setOnClickListener(fabMenuListener);
    }

    private void setupTabIcons(TabLayout tabLayout) {
        @SuppressLint("InflateParams") ImageView tabOne = (ImageView) LayoutInflater.from(this).inflate(R.layout.custom_tab_smart_home,null);
        TabLayout.Tab firstTab = tabLayout.getTabAt(0);
        if (firstTab != null) firstTab.setCustomView(tabOne);

        @SuppressLint("InflateParams") ImageView tabTwo =(ImageView) LayoutInflater.from(this).inflate( R.layout.custom_tab_actions, null);
        TabLayout.Tab secondTab = tabLayout.getTabAt(1);
        if (secondTab != null) secondTab.setCustomView(tabTwo);

        @SuppressLint("InflateParams") ImageView tabThree = (ImageView) LayoutInflater.from(this).inflate( R.layout.custom_tab_settings, null);
        TabLayout.Tab thirdTab = tabLayout.getTabAt(2);
        if (thirdTab != null) thirdTab.setCustomView(tabThree);
    }

    private final TabLayout.ViewPagerOnTabSelectedListener tabSelectedListener = new TabLayout.ViewPagerOnTabSelectedListener(null) {
        @Override
        public void onTabSelected(TabLayout.Tab tab) {
            ImageView tabImage = (ImageView) tab.getCustomView();
            if (tabImage != null) tabImage.setColorFilter(Color.WHITE, PorterDuff.Mode.SRC_IN);
        }

        @Override
        public void onTabUnselected(TabLayout.Tab tab) {
            ImageView tabImage = (ImageView) tab.getCustomView();
            if (tabImage != null) tabImage.clearColorFilter();
        }

        @Override
        public void onTabReselected(TabLayout.Tab tab) {

        }
    };

    private final View.OnClickListener fabMenuListener =  v -> {
            if (v.getId() == R.id.floating_menu_item_add) {
                showBuildingFragment();
            } else {
                if (v instanceof FloatingActionButton) {
                    FloatingActionButton buildingFab = (FloatingActionButton) v;
                    String selectedBuildingId = (String) buildingFab.getTag();
                    Log.d(TAG, "Selected building ID: " + selectedBuildingId);
                    if (repository.getCurrentBuildingId().equals(selectedBuildingId)) {
                        Log.d(TAG, "Selected building is the current one.");
                    } else {
                        Log.d(TAG, "Changing current building.");
                        repository.setCurrentBuildingId(selectedBuildingId);
                        String message = getString(R.string.main_building_changed, buildingFab.getLabelText().replace(getString(R.string.fab_menu_select), ""));
                        Snackbar snackbar = Snackbar.make(findViewById(R.id.main_coordinator), message, Snackbar.LENGTH_SHORT);
                        snackbar.show();
                    }
                }
            }
            FloatingActionMenu menu = findViewById(R.id.main_floating_menu);
            menu.close(true);
        };

    private void showLogout() {
        // Ask user for confirmation.
        runOnUiThread(() -> {
            AlertDialog dialog = new AlertDialog.Builder(MainActivity.this)
                    .setIcon(R.drawable.main_ic_guest)
                    .setTitle(R.string.logout_title)
                    .setMessage(R.string.logout_prompt_message)
                    .setPositiveButton(android.R.string.yes,
                            (dialog1, whichButton) -> {
                                // Clean database.
                                repository.logout();
                                // Start login activity.
                                showStarter();
                            }
                    )
                    .setNegativeButton(android.R.string.no,
                            (dialog1, whichButton) -> {
                            }
                    ).create();

            dialog.show();
        });
    }

    private void showStarter() {
        Intent intent = new Intent(this, StarterActivity.class);
        startActivity(intent);
        finish();
    }
}
