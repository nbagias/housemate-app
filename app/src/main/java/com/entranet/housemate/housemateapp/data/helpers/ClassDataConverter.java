package com.entranet.housemate.housemateapp.data.helpers;

import android.util.Log;

import com.entranet.housemate.housemateapp.api.building.Building;
import com.entranet.housemate.housemateapp.data.db.entity.BuildingEntity;

/**
 * Converts api data to db entities {@link Building} to {@link BuildingEntity}
 */

public class ClassDataConverter {

    private static final String TAG = ClassDataConverter.class.getSimpleName();

    private  ClassDataConverter() {}

    public static BuildingEntity convertBuilding(Building building) {
        // Construct a building Entity
        BuildingEntity buildingEntity = new BuildingEntity();
        // ID
        Log.d(TAG, "Building ID: " + building.getBuildingID());
        buildingEntity.setId(building.getBuildingID());
        // Name
        Log.d(TAG, "Building name: " + building.getName());
        buildingEntity.setName(building.getName());
        // Set if the current user is master or not.
        buildingEntity.setIsMaster(building.getIsMaster() ? 1 : 0);
        // Set the permissions.
        buildingEntity.setPermissions(building.getPermissions());
        buildingEntity.setSettings(building.getSettings());
        buildingEntity.setDefault(false);

        return buildingEntity;
    }
}
