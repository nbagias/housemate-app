package com.entranet.housemate.housemateapp.api.building;

import com.squareup.moshi.Json;

/**
 * Request parameters for the get buildings request.
 */

public class GetBuildingsBody {
    @Json(name = "token")
    private String token;

    public GetBuildingsBody(String token) { this.token = token; }
}
