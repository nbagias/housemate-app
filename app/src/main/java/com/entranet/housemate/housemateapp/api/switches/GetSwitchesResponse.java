package com.entranet.housemate.housemateapp.api.switches;

import com.squareup.moshi.Json;

/**
 * Class representing the format of a get switches response.
 */

public class GetSwitchesResponse {
    @Json(name = "state")
    private Integer state;
    @Json(name = "description")
    private String description;
    @Json(name = "data")
    private SwitchesData data;

    public Integer getState() { return state; }

    public void setState(Integer state) { this.state = state; }

    public String getDescription() { return description; }

    public void setDescription(String description) { this.description = description; }

    public SwitchesData getData() { return data; }

    public void setData(SwitchesData data) { this.data = data; }
}
