package com.entranet.housemate.housemateapp.data.repos;

import android.util.Log;

import com.entranet.housemate.housemateapp.BuildConfig;
import com.entranet.housemate.housemateapp.R;
import com.entranet.housemate.housemateapp.api.ApiResponse;
import com.entranet.housemate.housemateapp.api.SettingsApi;
import com.entranet.housemate.housemateapp.api.building.BuildingSettings;
import com.entranet.housemate.housemateapp.api.settings.NumberOfNewsBody;
import com.entranet.housemate.housemateapp.api.settings.SetLocationBody;
import com.entranet.housemate.housemateapp.api.settings.SetTimezoneBody;
import com.entranet.housemate.housemateapp.data.AppExecutors;
import com.entranet.housemate.housemateapp.data.db.dao.BuildingDao;
import com.entranet.housemate.housemateapp.data.db.entity.BuildingEntity;
import com.entranet.housemate.housemateapp.ui.main.settings.location.LocationSettingsCallback;
import com.entranet.housemate.housemateapp.ui.main.settings.news.NewsCallback;

import java.io.IOException;

import retrofit2.Response;

/**
 * Repository class for handling operations on settings section.
 */

public class SettingsRepository {
    // Logging tag.
    private static final String TAG = SettingsRepository.class.getSimpleName();

    private SettingsApi settingsApi;
    private BuildingDao buildingDao;
    private AppExecutors executors;

    public SettingsRepository(SettingsApi settingsApi, BuildingDao buildingDao, AppExecutors executors) {
        this.settingsApi = settingsApi;
        this.buildingDao = buildingDao;
        this.executors = executors;
    }

    public void getNews(String buildingId, NewsCallback callback) {
        executors.getDiskIO().execute(() -> {
            // Get the stored number of news.
            int num = buildingDao.getNumOfNews(buildingId);
            // Route value to listener.
            callback.onSetNumberPicker(num);
        });
    }

    public void setNews(int num, String currentBuildingId, String token) {
        executors.getDiskIO().execute(() -> {
            // Check if there is a need for update.
            int currentNumNews = buildingDao.getNumOfNews(currentBuildingId);
            if (num == currentNumNews) {
                // Nothing changed.
                Log.d(TAG, "Same number of news selected");
                return;
            }
            // Update database.
            buildingDao.setNumOfNews(num, currentBuildingId);
            // --- Update API ---
            // Create the body request.
            NumberOfNewsBody body = new NumberOfNewsBody(num);
            body.setBuildingID(currentBuildingId);
            body.setToken(token);
            // Issue the request.
            try {
                Response<ApiResponse> newsResponse = settingsApi.setNumberOfNews(body).execute();
                if (newsResponse.isSuccessful()) {
                    ApiResponse data = newsResponse.body();
                    if (data != null) {
                        if (data.getState() == 0) {
                            if (BuildConfig.DEBUG) Log.d(TAG, "Number of news set successfully.");
                        } else {
                            Log.e(TAG, "API Response error description: " + data.getDescription());
                        }
                    } else {
                        Log.e(TAG, "API Response body null");
                    }
                } else {
                    Log.e(TAG, "API Request unsuccessful: " + newsResponse.errorBody());
                }
            } catch (IOException e) {
                Log.e(TAG, "Exception raised: " + e.getMessage());
            }
        });
    }

    public void loadLocation(String buildingId, LocationSettingsCallback callback) {
        executors.getDiskIO().execute(() -> {
            // Get the current location from the database.
            BuildingEntity building = buildingDao.getBuilding(buildingId);
            BuildingSettings settings = building.getSettings();
            // Weather Location
            if (settings.getLocation().isEmpty()) {
                // Database empty. Let's return placeholder.
                callback.onSetDefaultLocation(R.string.location_weather_value);
            } else {
                // Set weather location.
                callback.onSetCurrentLocation(settings.getLocation());
            }
            // Timezone
            if (settings.getTimezone().isEmpty()) {
                // Database empty. Let's return placeholder.
                callback.onSetDefaultTimezone(R.string.location_timezone_value);
            } else {
                // Set timezone.
                callback.onSetCurrentTimezone(settings.getTimezone());
            }
            callback.onLoadSettingsCompleted();
        });
    }

    public void setTimezone(String timezone, String buildingId, String token) {
        executors.getDiskIO().execute(() -> {
            // Get the current settings from the database.
            String previousTimezone = buildingDao.getTimezone(buildingId);
            // Set the new timezone, if different.
            if (previousTimezone.equals(timezone)) {
                Log.i(TAG, "Timezone unchanged.");
            } else {
                // Update database.
                buildingDao.setTimezone(timezone, buildingId);
                // --- Update API ---
                // Create request body.
                SetTimezoneBody body = new SetTimezoneBody(timezone);
                body.setBuildingID(buildingId);
                body.setToken(token);
                // Issue the request.
                try {
                    Response<ApiResponse> response = settingsApi.setTimezone(body).execute();
                    if (response.isSuccessful()) {
                        ApiResponse data = response.body();
                        if (data != null) {
                            if (data.getState() == 0) {
                                if (BuildConfig.DEBUG) Log.d(TAG, "Timezone set successfully.");
                            } else {
                                Log.e(TAG, "API Response error description: " + data.getDescription());
                            }
                        } else {
                            Log.e(TAG, "API Response body null");
                        }
                    } else {
                        Log.e(TAG, "API Request unsuccessful: " + response.errorBody());
                    }
                } catch (IOException e) {
                    Log.e(TAG, "Exception raised: " + e.getMessage());
                }
            }
        });
    }

    public void setLocation(String location, String currentBuildingId, String token) {
        executors.getDiskIO().execute(() -> {
            // Get the current settings from the database.
            String previousLocation = buildingDao.getLocation(currentBuildingId);
            // Set the new location, if different.
            if (previousLocation.equals(location)) {
                Log.i(TAG, "Selected location is already set.");
            } else {
                // Update database.
                buildingDao.setLocation(location, currentBuildingId);
                // --- Update API ---
                // Create request body.
                SetLocationBody body = new SetLocationBody(location);
                body.setBuildingID(currentBuildingId);
                body.setToken(token);
                // Issue the request.
                try {
                    Response<ApiResponse> response = settingsApi.setLocation(body).execute();
                    if (response.isSuccessful()) {
                        ApiResponse data = response.body();
                        if (data != null) {
                            if (data.getState() == 0) {
                                if (BuildConfig.DEBUG) Log.d(TAG, "Location set successfully.");
                            } else {
                                Log.e(TAG, "API Response error description: " + data.getDescription());
                            }
                        } else {
                            Log.e(TAG, "API Response body null");
                        }
                    } else {
                        Log.e(TAG, "API Request unsuccessful: " + response.errorBody());
                    }
                } catch (IOException e) {
                    Log.e(TAG, "Exception raised: " + e.getMessage());
                }
            }
        });
    }
}
