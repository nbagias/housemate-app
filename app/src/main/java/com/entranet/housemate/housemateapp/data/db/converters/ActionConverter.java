package com.entranet.housemate.housemateapp.data.db.converters;

import android.arch.persistence.room.TypeConverter;
import android.util.Log;

import com.entranet.housemate.housemateapp.data.db.entity.ActionEntity;
import com.squareup.moshi.JsonAdapter;
import com.squareup.moshi.Moshi;
import com.squareup.moshi.Types;

import java.io.IOException;
import java.lang.reflect.Type;
import java.util.Collections;
import java.util.List;

/**
 * TypeConverter for mapping {@link ActionEntity#devices} to and from JSON to Room.
 */

public class ActionConverter {

    private static final String TAG = ActionConverter.class.getSimpleName();

    @TypeConverter
    public static List<String> fromString(String data) {
        if (data != null) {
            try {
                Moshi moshi = new Moshi.Builder().build();
                Type listData = Types.newParameterizedType(List.class, String.class);
                JsonAdapter<List<String>> jsonAdapter = moshi.adapter(listData);
                return jsonAdapter.fromJson(data);
            } catch (IOException e) {
                Log.e(TAG, "Could not convert json to list of strings.");
            }
        }
        return Collections.emptyList();
    }

    @TypeConverter
    public static String fromArrayList(List<String> list) {
        Moshi moshi = new Moshi.Builder().build();
        Type listData = Types.newParameterizedType(List.class, String.class);
        JsonAdapter<List<String>> jsonAdapter = moshi.adapter(listData);
        return jsonAdapter.toJson(list);
    }
}
