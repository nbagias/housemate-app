package com.entranet.housemate.housemateapp.data.repos;

import android.util.Log;

import com.entranet.housemate.housemateapp.api.ApiResponse;
import com.entranet.housemate.housemateapp.api.HomeApi;
import com.entranet.housemate.housemateapp.api.switches.GetSwitchesBody;
import com.entranet.housemate.housemateapp.api.switches.GetSwitchesResponse;
import com.entranet.housemate.housemateapp.api.switches.SwitchesData;
import com.entranet.housemate.housemateapp.api.switches.UpdateSwitchNameBody;
import com.entranet.housemate.housemateapp.api.switches.UpdateSwitchStateBody;
import com.entranet.housemate.housemateapp.data.AppExecutors;
import com.entranet.housemate.housemateapp.data.db.dao.SwitchDao;
import com.entranet.housemate.housemateapp.data.db.entity.SceneEntity;
import com.entranet.housemate.housemateapp.data.db.entity.SwitchEntity;
import com.entranet.housemate.housemateapp.data.helpers.Constants;
import com.entranet.housemate.housemateapp.ui.main.smarthome.scenes.LoadSwitchesCallback;

import java.io.IOException;
import java.util.List;

import retrofit2.Response;

/**
 * Repository class for handling {@link SwitchEntity} operations.
 */

public class SwitchRepository {
    // Logging tag.
    private static final String TAG = SwitchRepository.class.getSimpleName();

    private HomeApi homeApi;
    private SwitchDao switchDao;
    private AppExecutors executors;

    public SwitchRepository(HomeApi homeApi, SwitchDao switchDao, AppExecutors executors) {
        this.homeApi = homeApi;
        this.switchDao = switchDao;
        this.executors = executors;
    }

    public void add(List<SwitchEntity> switches, String buildingID) {
        executors.getDiskIO().execute(() -> {
            // Set the building ID.
            for (SwitchEntity switchEntity : switches) switchEntity.setBuildingId(buildingID);
            switchDao.insertAllSwitches(switches);
        });
    }

    public void load(String currentBuildingId, LoadSwitchesCallback callback) {
        executors.getDiskIO().execute(() -> {
            // First load and set the thermostat.
            SwitchEntity thermostat = switchDao.getThermostat(currentBuildingId);
            if (thermostat != null) {
                if (thermostat.getExtra().isEmpty()) {
                    callback.onSetThermostatValue(0, true);
                } else {
                    callback.onSetThermostatValue(Integer.parseInt(thermostat.getExtra()), true);
                }
            } else {
                callback.onSetThermostatDisabled();
            }
            // Load the switches.
            List<SwitchEntity> switchList = switchDao.getSwitches(currentBuildingId);
            if (switchList != null) {
                callback.onLoadSwitchesSuccess(switchList);
            } else {
                callback.onLoadSwitchesFailure();
            }
        });
    }

    public void refresh(String token, String currentBuildingId, LoadSwitchesCallback callback) {
        executors.getDiskIO().execute(() -> {
            try {
                // Construct request's body.
                GetSwitchesBody body = new GetSwitchesBody(token, currentBuildingId);
                // Issue the request synchronously since we are in a background thread.
                Response<GetSwitchesResponse> response = homeApi.getSwitches(body).execute();
                // Get the response and parse the switches list.
                GetSwitchesResponse switchesResponse = response.body();
                if (switchesResponse != null) {
                    SwitchesData data = switchesResponse.getData();
                    List<SwitchEntity> updatedSwitches = data.getSwitches();
                    // Important step: set the building ID.
                    for (SwitchEntity switchEntity: updatedSwitches) {
                        switchEntity.setBuildingId(currentBuildingId);
                    }
                    // Update the database.
                    switchDao.insertAllSwitches(updatedSwitches);
                    // DRY
                    load(currentBuildingId, callback);
                    // Update the refresh state.
                    callback.onRefreshSuccess();
                }
            } catch (IOException e) {
                Log.d(TAG, "IOException: " + e.getMessage());
                callback.onRefreshFailure();
            }
        });
    }

    public void update(SwitchEntity updatedSwitch, String token, String buildingId) {
        executors.getDiskIO().execute(() -> {
            // Construct the request's body.
            UpdateSwitchStateBody body = new UpdateSwitchStateBody(updatedSwitch.getSwitchId(),
                    updatedSwitch.getState(),
                    updatedSwitch.getExtra());
            body.setToken(token);
            body.setBuildingID(buildingId);
            // Issue the call synchronously.
            try {
                Response<ApiResponse> response = homeApi.updateSwitchState(body).execute();
                // Check if the request was successful. At this point we are just logging the problems.
                if (response != null && response.isSuccessful()) {
                    ApiResponse stateResponse = response.body();
                    if (stateResponse != null) {
                        if (stateResponse.getState() == Constants.API_RESPONSE_STATE_SUCCESSFUL) {
                            Log.d(TAG, "Updated switch " + updatedSwitch.getName() + " successfully");
                        } else {
                            Log.e(TAG, "Failed to update switch " + updatedSwitch.getName());
                            Log.e(TAG, "Problem description: " + stateResponse.getDescription());
                        }
                        // We got the info we wanted return.
                        return;
                    }
                }
                Log.e(TAG, "Failed to update switch " + updatedSwitch.getName());
            } catch (IOException e) {
                Log.e(TAG, "IO/Exception: " + e.getMessage());
            }
        });
        // Update the database.
        executors.getDiskIO().execute(() -> switchDao.updateSwitch(updatedSwitch));
    }

    public void updateName(String newName, String switchId, String token, String buildingId) {
        executors.getDiskIO().execute(() -> {
            UpdateSwitchNameBody body = new UpdateSwitchNameBody(switchId, newName);
            body.setBuildingID(buildingId);
            body.setToken(token);
            try {
                Response<ApiResponse> response = homeApi.updateSwitchName(body).execute();
                // Check if the request was successful. At this point we are just logging the problems.
                if (response != null && response.isSuccessful()) {
                    ApiResponse stateResponse = response.body();
                    if (stateResponse != null) {
                        if (stateResponse.getState() == Constants.API_RESPONSE_STATE_SUCCESSFUL) {
                            Log.d(TAG, "Updated switch " + newName + " successfully");
                        } else {
                            Log.d(TAG, "Failed to rename switch to " + newName);
                            Log.d(TAG, "Problem description: " + stateResponse.getDescription());
                        }
                        // We got the info we wanted return.
                        return;
                    }
                }
                Log.e(TAG, "Failed to rename switch to " + newName);
            } catch (IOException e) {
                Log.e(TAG, "IO/Exception: " + e.getMessage());
            }
        });
        // Update database.
        executors.getDiskIO().execute(() -> switchDao.updateSwitchNameById(newName, switchId));
    }

    public List<SwitchEntity> get(String buildingId) { return switchDao.getSwitches(buildingId); }

    public SwitchEntity getThermostat(String buildingId) { return switchDao.getThermostat(buildingId); }

    public void updateThermostat(int newValue, String token, String buildingId) {
        executors.getDiskIO().execute(() -> {
            SwitchEntity thermostat = getThermostat(buildingId);
            thermostat.setExtra(String.valueOf(newValue));
            update(thermostat, token, buildingId);
        });
    }

    public void filterForScene(SceneEntity scene, String buildingId, LoadSwitchesCallback callback) {
        if (scene == null) {
            Log.e(TAG, "Scene name is null.");
            callback.onLoadSwitchesFailure();
            return;
        }
        // Then get all the switches.
        List<SwitchEntity> switchList = get(buildingId);
        if (switchList != null) {
            // Default to unselected.
            for (SwitchEntity switchEntity: switchList) switchEntity.setExtra(Constants.LIST_ITEM_UNSELECTED);
            // Iterate over the scene parts and modify the switchList to match the scene's state.
            for (SwitchEntity scenePart: scene.getSwitches()) {
                for (SwitchEntity switchEntity: switchList) {
                    if (scenePart.getSwitchId().equals(switchEntity.getSwitchId())) {
                        switchEntity.setState(scenePart.getState());
                        switchEntity.setExtra(Constants.LIST_ITEM_SELECTED);
                        break;
                    }
                }
            }
            // Also, if the scene contains a thermostat, display its value, otherwise default to current.
            SwitchEntity thermostat = switchDao.getThermostat(buildingId);
            // Make sure there is a thermostat in general.
            if (thermostat != null) {
                boolean hasThermostat = false;
                for (SwitchEntity scenePart: scene.getSwitches()) {
                    if (scenePart.getSwitchId().equals(thermostat.getSwitchId())) {
                        thermostat.setExtra(scenePart.getExtra());
                        hasThermostat = true;
                        break;
                    }
                }
                if (thermostat.getExtra().isEmpty()) {
                    callback.onSetThermostatValue(0, hasThermostat);
                } else {
                    callback.onSetThermostatValue(Integer.parseInt(thermostat.getExtra()), hasThermostat);
                }
            }
            callback.onLoadSwitchesSuccess(switchList);
        }
    }

    public void truncate() { executors.getDiskIO().execute(switchDao::deleteAll); }
}