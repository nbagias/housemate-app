package com.entranet.housemate.housemateapp.ui.main.smarthome.scenes;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.Snackbar;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.PopupMenu;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;

import com.entranet.housemate.housemateapp.R;
import com.entranet.housemate.housemateapp.data.DataRepository;
import com.entranet.housemate.housemateapp.data.helpers.Constants;
import com.entranet.housemate.housemateapp.ui.main.smarthome.scenes.custom.ScenesRecyclerAdapter;

import java.util.List;

import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

public class ScenesActivity extends AppCompatActivity implements ScenesRecyclerAdapter.SceneItemListener,
        SceneEditorFragment.OnSaveNewSceneListener,
        RemoveItemFragment.OnRemoveItemListener,
        SceneActionsCallback {

    private RecyclerView recyclerView;
    private DataRepository repository;
    private String selectedSceneName;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_scenes);
        // Setup UI.
        setupUi();
        // Get a reference to the repository.
        repository = DataRepository.getInstance(getApplicationContext());
        // Load scenes.
        repository.getSceneNames(this);
    }

    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }

    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }

    @Override
    public void onSceneClicked(String sceneName) { repository.activateScene(sceneName, this); }

    @Override
    public void onSceneLongClicked(View view, String sceneName) {
        this.selectedSceneName = sceneName;
        showPopup(view);
    }

    @Override
    public void onShowSceneActivated(String name) {
        Snackbar snackbar = Snackbar.make(findViewById(R.id.scenes_actions_container), getString(R.string.scenes_msg_activated) + name, Snackbar.LENGTH_SHORT);
        snackbar.show();

        runOnUiThread(() -> {
            ScenesRecyclerAdapter adapter = (ScenesRecyclerAdapter) recyclerView.getAdapter();
            if (adapter != null) adapter.updateLoadingState(name);
        });
    }

    @Override
    public void onShowSceneActivationFailed(String name) {
        Snackbar snackbar = Snackbar.make(findViewById(R.id.scenes_actions_container), getString(R.string.scenes_msg_not_activated) + name, Snackbar.LENGTH_SHORT);
        snackbar.show();

        runOnUiThread(() -> {
            ScenesRecyclerAdapter adapter = (ScenesRecyclerAdapter) recyclerView.getAdapter();
            if (adapter != null) adapter.updateLoadingState(name);
        });
    }

    @Override
    public void onSaveSceneFinished(String sceneName) {
        Snackbar snackbar = Snackbar.make(findViewById(R.id.scenes_actions_container), getString(R.string.scenes_msg_saved) + sceneName, Snackbar.LENGTH_SHORT);
        snackbar.show();
        // Refresh the list.
        repository.getSceneNames(this);
    }

    @Override
    public void onSaveSceneError() {
        Snackbar snackbar = Snackbar.make(findViewById(R.id.scenes_root_container), R.string.scenes_msg_not_saved, Snackbar.LENGTH_SHORT);
        snackbar.show();
    }

    @Override
    public void onUserConfirmedRemoval(String sceneName) {
        if (sceneName == null) {
            repository.deleteAllScenes();
            runOnUiThread(() -> recyclerView.setAdapter(null));
        } else {
            repository.deleteSceneByName(sceneName, this);
        }
    }

    @Override
    public void onScenesLoaded(final List<String> scenes) {
        runOnUiThread(() -> recyclerView.setAdapter(new ScenesRecyclerAdapter(scenes, ScenesActivity.this)));
    }

    private void showRemoveDialog(String sceneName) {
        DialogFragment removeScenesDlg;
        if (sceneName == null) {
            removeScenesDlg = RemoveItemFragment.newInstance(Constants.REMOVE_ALL_SCENES, null);
        } else {
            removeScenesDlg = RemoveItemFragment.newInstance(Constants.REMOVE_SCENE, sceneName);
        }
        // Launch the confirmation dialog.
        removeScenesDlg.show(getSupportFragmentManager(), RemoveItemFragment.class.getSimpleName());
    }

    private void showPopup(View v) {
        PopupMenu popup = new PopupMenu(this, v);
        popup.setOnMenuItemClickListener(menuItemClickListener);
        popup.inflate(R.menu.scene_options);
        popup.show();
    }

    private void showSceneEditor(@Nullable String sceneName) {
        SceneEditorFragment fragment;
        if (sceneName == null) {
            fragment = SceneEditorFragment.newInstance(Constants.MODE_CREATE, null);
        } else {
            fragment = SceneEditorFragment.newInstance(Constants.MODE_EDIT, sceneName);
        }
        FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
        fragment.show(ft, SceneEditorFragment.class.getSimpleName());
    }

    private void setupUi() {
        // Set the toolbar.
        Toolbar toolbar = findViewById(R.id.toolbar);
        toolbar.setTitle("");
        setSupportActionBar(toolbar);
        ActionBar actionBar = getSupportActionBar();
        if (actionBar != null) actionBar.setDisplayHomeAsUpEnabled(true);
        // Setup recycler.
        recyclerView = findViewById(R.id.scenes_recycler);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        // Attach button listener.
        Button btnAddNewScene = findViewById(R.id.scenes_button_add_new);
        btnAddNewScene.setOnClickListener(clickListener);
        Button btnRemoveAll = findViewById(R.id.scenes_button_remove_all);
        btnRemoveAll.setOnClickListener(clickListener);
    }

    private PopupMenu.OnMenuItemClickListener menuItemClickListener = new PopupMenu.OnMenuItemClickListener() {
        @Override
        public boolean onMenuItemClick(MenuItem item) {
            switch (item.getItemId()) {
                case R.id.scene_options_edit:
                    showSceneEditor(selectedSceneName);
                    return true;
                case R.id.scene_options_delete:
                    showRemoveDialog(selectedSceneName);
                    return true;
                default:
                    return false;
            }
        }
    };

    private View.OnClickListener clickListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            switch (v.getId()) {
                case R.id.scenes_button_add_new:
                    showSceneEditor(null);
                    break;
                case R.id.scenes_button_remove_all:
                    // Check if there are any scenes.
                    if (recyclerView.getAdapter().getItemViewType(0) == ScenesRecyclerAdapter.EMPTY_VIEW) return;
                    // Show dialog.
                    showRemoveDialog(null);
                    break;
                default:
                    break;
            }
        }
    };
}
