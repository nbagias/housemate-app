package com.entranet.housemate.housemateapp.ui.main.settings.accessrules;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.TextInputEditText;
import android.support.design.widget.TextInputLayout;
import android.support.v4.app.DialogFragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import com.entranet.housemate.housemateapp.R;
import com.entranet.housemate.housemateapp.data.helpers.InputHelper;

/**
 * A {@link DialogFragment} for adding a guest.
 */
public class AddGuestFragment extends DialogFragment {

    private OnAddGuestListener listener;

    private TextInputLayout inputLayout;
    private TextInputEditText inputEmail;

    public AddGuestFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment.
     */
    @NonNull
    public static AddGuestFragment newInstance() {
        return new AddGuestFragment();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_add_guest, container, false);

        inputLayout = view.findViewById(R.id.add_guest_input_layout);
        inputEmail = view.findViewById(R.id.add_guest_edit_text);

        Button btnAdd = view.findViewById(R.id.add_guest_add_button);
        btnAdd.setOnClickListener(buttonListener);

        return  view;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnAddGuestListener) {
            listener = (OnAddGuestListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnAddGuestListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        listener = null;
    }

    private View.OnClickListener buttonListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            // Get the user's input.
            String guestEmail = inputEmail.getText().toString().trim();
            if (InputHelper.isValidEmail(guestEmail)) {
                listener.onAddGuestEmail(guestEmail);
                dismiss();
            } else {
                inputLayout.setError(getString(R.string.register_error_email_invalid));
            }
        }
    };
    /**
     * Interface for routing control back to the caller.
     */
    public interface OnAddGuestListener {
        void onAddGuestEmail(String email);
    }
}
