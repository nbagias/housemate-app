package com.entranet.housemate.housemateapp.api.settings;

import com.entranet.housemate.housemateapp.api.SettingsApi;
import com.entranet.housemate.housemateapp.data.helpers.Constants;
import com.squareup.moshi.Json;

/**
 * API request body for {@link SettingsApi#setNumberOfNews}
 */

public class NumberOfNewsBody {
    @Json(name = "token")
    private String token;
    @Json(name = "buildingID")
    private String buildingID;
    @Json(name = "origin")
    private final int ORIGIN = Constants.ORIGIN_HOUSEMATE_APP;
    @Json(name = "numberOfNews")
    private int numberOfNews;

    public NumberOfNewsBody(int numberOfNews) { this.numberOfNews = numberOfNews; }

    public String getToken() { return token; }

    public void setToken(String token) { this.token = token; }

    public String getBuildingID() { return buildingID; }

    public void setBuildingID(String buildingID) { this.buildingID = buildingID; }
}
