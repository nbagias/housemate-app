package com.entranet.housemate.housemateapp.api.switches;

import com.entranet.housemate.housemateapp.data.db.entity.SwitchEntity;
import com.squareup.moshi.Json;

import java.util.List;

/**
 * The switches list, as found in the get switches response format.
 */

public class SwitchesData {

    @Json(name = "switches")
    private List<SwitchEntity> switches;

    public List<SwitchEntity> getSwitches() { return switches; }

    public void setSwitches(List<SwitchEntity> switches) { this.switches = switches; }
}
