package com.entranet.housemate.housemateapp.data.repos;

import android.util.Log;

import com.entranet.housemate.housemateapp.BuildConfig;
import com.entranet.housemate.housemateapp.api.AdminApi;
import com.entranet.housemate.housemateapp.api.ApiResponse;
import com.entranet.housemate.housemateapp.api.building.AddUserToBuildingBody;
import com.entranet.housemate.housemateapp.api.building.Building;
import com.entranet.housemate.housemateapp.api.building.BuildingData;
import com.entranet.housemate.housemateapp.api.building.BuildingPermissions;
import com.entranet.housemate.housemateapp.api.building.GetBuildingsBody;
import com.entranet.housemate.housemateapp.api.building.GetBuildingsResponse;
import com.entranet.housemate.housemateapp.api.guest.DeleteGuestBody;
import com.entranet.housemate.housemateapp.api.guest.GetGuestsBody;
import com.entranet.housemate.housemateapp.api.guest.GetGuestsResponse;
import com.entranet.housemate.housemateapp.api.guest.SetGuestPermBody;
import com.entranet.housemate.housemateapp.data.AppExecutors;
import com.entranet.housemate.housemateapp.data.db.dao.BuildingDao;
import com.entranet.housemate.housemateapp.data.db.entity.BuildingEntity;
import com.entranet.housemate.housemateapp.data.db.entity.DeviceEntity;
import com.entranet.housemate.housemateapp.data.db.entity.GuestEntity;
import com.entranet.housemate.housemateapp.data.helpers.ClassDataConverter;
import com.entranet.housemate.housemateapp.ui.main.buildings.AddBuildingCallback;
import com.entranet.housemate.housemateapp.ui.main.buildings.GetBuildingsDbCallback;
import com.entranet.housemate.housemateapp.ui.main.settings.accessrules.AddGuestCallback;
import com.entranet.housemate.housemateapp.ui.main.settings.accessrules.LoadGuestsCallback;
import com.entranet.housemate.housemateapp.ui.main.smarthome.camera.LoadDevicesCallback;

import java.io.IOException;
import java.util.List;

import retrofit2.Call;
import retrofit2.Response;

/**
 * Repository class for accessing storage on {@link BuildingEntity} operations.
 */

public class BuildingRepository {

    private static final String TAG = BuildingRepository.class.getSimpleName();

    private final AdminApi adminApi;
    private final BuildingDao buildingDao;
    private final AppExecutors executors;

    public BuildingRepository(AdminApi adminApi, BuildingDao buildingDao, AppExecutors executors) {
        this.adminApi = adminApi;
        this.buildingDao = buildingDao;
        this.executors = executors;
    }

    public void add(Building building) {
        // First, convert Building to BuildingEntity.
        BuildingEntity newBuilding = ClassDataConverter.convertBuilding(building);
        // Save to the database.
        buildingDao.insertBuilding(newBuilding);
        // For each device we have to set the building ID before saving them.
        for (DeviceEntity device: building.getDevices()) device.setBuildingId(building.getBuildingID());
        buildingDao.insertDevices(building.getDevices());
        // For each guest we have to set the building ID before saving them.
        for (GuestEntity guest: building.getGuests()) guest.setBuildingId(building.getBuildingID());
        buildingDao.insertGuests(building.getGuests());
    }

    public void addGuest(String guestEmail, String token, String currentBuildingId, AddGuestCallback callback) {
        executors.getDiskIO().execute(() -> {
            String buildingName = buildingDao.getCurrentBuildingName(currentBuildingId);
            // Construct the body.
            AddUserToBuildingBody body = new AddUserToBuildingBody(currentBuildingId, token, guestEmail);
            body.setBuildingName(buildingName);
            String errorMessage;
            try {
                // Issue the request.
                Response<ApiResponse> response = adminApi.addUserToBuilding(body).execute();
                if (response.isSuccessful()) {
                    ApiResponse data = response.body();
                    if (data != null) {
                        if (data.getState() == 0) {
                            if (BuildConfig.DEBUG) Log.d(TAG, "Guest added successfully.");
                            callback.onGuestInsertionSuccess();
                            return;
                        } else {
                            errorMessage = "An error occurred. Please try again.";
                            Log.d(TAG, "Guest insertion failed. Description: " + data.getDescription());
                        }
                    } else {
                        errorMessage = "An error occurred. Please try again.";
                        Log.d(TAG, "Add guest response body empty.");
                    }
                } else {
                    errorMessage = "There was a network problem. Please try again.";
                    Log.d(TAG, "Add guest request failed.");
                }
            } catch (IOException e) {
                errorMessage = "There was a network problem. Please try again.";
                Log.d(TAG, "Exception raised: " + e.getMessage());
            }
            callback.onGuestInsertionFailure(errorMessage);
        });
    }

    public void load(GetBuildingsDbCallback callback) {
        executors.getDiskIO().execute(() -> {
            List<BuildingEntity> buildings = buildingDao.getAllBuildings();
            if (buildings == null || buildings.isEmpty()) {
                callback.onBuildingsNone();
            } else {
                callback.onBuildingsExist(buildings);
            }
        });
    }

    public void addUser(String buildingId, String buildingName, String token, String email, AddBuildingCallback callback) {
        executors.getDiskIO().execute(() -> {
            // Construct the body.
            AddUserToBuildingBody body = new AddUserToBuildingBody(buildingId, token, email);
            body.setBuildingName(buildingName);

            String errorMessage;
            try {
                // Issue the request.
                Response<ApiResponse> response = adminApi.addUserToBuilding(body).execute();
                if (response.isSuccessful()) {
                    ApiResponse data = response.body();
                    if (data != null) {
                        if (data.getState() == 0) {
                            if (BuildConfig.DEBUG) Log.d(TAG, "Building added successfully.");
                            updateUserBuildings(token);
                            callback.onAddBuildingSuccess();
                            return;
                        } else {
                            errorMessage = "An error occurred. Please try again.";
                            Log.e(TAG, "Building insertion failed. Description: " + data.getDescription());
                        }
                    } else {
                        errorMessage = "An error occurred. Please try again.";
                        Log.e(TAG, "Add building response body empty.");
                    }
                } else {
                    errorMessage = "There was a network problem. Please try again.";
                    Log.e(TAG, "Add building request failed.");
                }
            } catch (IOException e) {
                errorMessage = "There was a network problem. Please try again.";
                Log.d(TAG, "Exception raised: " + e.getMessage());
            }
            callback.onAddBuildingFailure(errorMessage);
        });
    }

    private void updateUserBuildings(String token) {
        try {
            GetBuildingsBody body = new GetBuildingsBody(token);

            Call<GetBuildingsResponse> call = adminApi.getBuildings(body);

            Response<GetBuildingsResponse> response = call.execute();
            if (response.isSuccessful()) {
                GetBuildingsResponse buildingsResponse = response.body();
                if (buildingsResponse != null) {
                    if (buildingsResponse.getState() == 0) {
                        Log.d(TAG, "Retrived buildings successfully.");
                        // Get the latest buildings.
                        BuildingData buildingData = buildingsResponse.getData();
                        // Get the buildings from the local database.
                        List<BuildingEntity> dbBuildings = buildingDao.getAllBuildings();
                        // TODO Merge to one class and avoid conversions, manual search etc.
                        // Compare and insert the one missing.
                        for (Building building: buildingData.getBuildings()) {
                            String id = building.getBuildingID();
                            boolean exists = false;
                            for (BuildingEntity dbBuilding: dbBuildings) {
                                if (id.equals(dbBuilding.getId())) {
                                    // Old building, stop searching.
                                    exists = true;
                                    break;
                                }
                            }
                            if (!exists) {
                                // Convert to db entity.
                                BuildingEntity newBuilding = ClassDataConverter.convertBuilding(building);
                                // Save it to the database.
                                buildingDao.insertBuilding(newBuilding);
                                // Here we can safely(?) assume that only one building is new, so break.
                                break;
                            }
                        }
                    } else {
                        Log.d(TAG, "Could not get buildings. Description: " + buildingsResponse.getDescription());
                    }
                } else {
                    Log.d(TAG, "Get buildings response body empty.");
                }
            } else {
                Log.d(TAG, "Get buildings request failed.");
            }
        } catch (IOException e) {
            Log.d(TAG, "Exception raised: " + e.getMessage());
        }
    }

    public void loadDevices(String currentBuildingId, LoadDevicesCallback callback) {
        executors.getDiskIO().execute(() -> {
            // Get the devices from the database.
            List<DeviceEntity> devices = buildingDao.getDevices(currentBuildingId);
            // Return to caller.
            callback.onDeviceListLoaded(devices);
        });
    }

    public List<DeviceEntity> getDevices(String currentBuildingId) {
        return buildingDao.getDevices(currentBuildingId);
    }

    public void loadGuests(String currentBuildingId, LoadGuestsCallback callback) {
        executors.getDiskIO().execute(() -> {
            // Get the guests of the current building.
            List<GuestEntity> guests = buildingDao.getGuests(currentBuildingId);
            // Return
            callback.onGuestsLoaded(guests);
        });
    }

    public void truncate() {
        executors.getDiskIO().execute(() -> {
            buildingDao.deleteAll();
            buildingDao.deleteAllGuests();
            buildingDao.deleteDevices();
        });
    }

    public void refreshGuests(String buildingId, String token, LoadGuestsCallback callback) {
        executors.getDiskIO().execute(() -> {
            // Construct body request.
            GetGuestsBody body = new GetGuestsBody();
            body.setBuildingID(buildingId);
            body.setToken(token);

            try {
                Response<GetGuestsResponse> response = adminApi.getGuests(body).execute();
                if (response.isSuccessful()) {
                    GetGuestsResponse data = response.body();
                    if (data != null) {
                        if (data.getState() == 0) {
                            if (BuildConfig.DEBUG) Log.d(TAG, "Get guests request successful.");
                            List<GuestEntity> guests = data.getData();
                            // Save locally.
                            buildingDao.insertGuests(guests);
                            // Return
                            callback.onGuestsLoaded(guests);
                        } else {
                            Log.e(TAG, "Request failed. Error description: " + data.getDescription());
                        }
                    } else {
                        Log.e(TAG, "Response body null.");
                    }
                } else {
                    Log.e(TAG, "Get guests request request unsuccessful.");
                }
            } catch (IOException e) {
                Log.e(TAG, "Exception raised: " + e.getMessage());
            }
        });
    }

    public void removeGuest(GuestEntity guest, String buildingId, String token, LoadGuestsCallback callback) {
        executors.getDiskIO().execute(() -> {
            // Remove from database.
            buildingDao.deleteGuest(guest);
            // Remove from API.
            DeleteGuestBody body = new DeleteGuestBody(guest.getEmail());
            body.setBuildingID(buildingId);
            body.setToken(token);
            // Issue the request.
            try {
                Response<ApiResponse> response = adminApi.deleteGuest(body).execute();
                if (response.isSuccessful()) {
                    ApiResponse data = response.body();
                    if (data != null) {
                        if (data.getState() == 0) {
                            Log.d(TAG, "Delete guest request successful.");
                            callback.onGuestsLoaded(buildingDao.getGuests(buildingId));
                        } else {
                            Log.e(TAG, "Request failed. Error description: " + data.getDescription());
                        }
                    } else {
                        Log.e(TAG, "Response body null.");
                    }
                } else {
                    Log.e(TAG, "Delete guest request unsuccessful.");
                }
            } catch (IOException e) {
                Log.e(TAG, "Exception raised: "  + e.getMessage());
            }
        });
    }

    public void updatePermissions(GuestEntity guest, String currentBuildingId, String token) {
        executors.getDiskIO().execute(() -> {
            // --- Update database ---
            buildingDao.updateGuest(guest);
            // --- Update API ---
            // Create request body.
            SetGuestPermBody body = new SetGuestPermBody(guest.getEmail(), guest.getPermissions());
            body.setBuildingID(currentBuildingId);
            body.setToken(token);
            // Issue the request.
            try {
                Response<ApiResponse> response = adminApi.setPermissionForGuest(body).execute();
                if (response.isSuccessful()) {
                    ApiResponse data = response.body();
                    if (data != null) {
                        if (data.getState() == 0) {
                            if (BuildConfig.DEBUG) Log.d(TAG, "Update permissions request successful.");
                        } else {
                            Log.e(TAG, "Request failed. Error description: " + data.getDescription());
                        }
                    } else {
                        Log.e(TAG, "Response body null.");
                    }
                } else {
                    Log.e(TAG, "Update permissions request unsuccessful.");
                }
            } catch (IOException e) {
                Log.e(TAG, "Exception raised: "  + e.getMessage());
            }
        });
    }

    public BuildingPermissions getPermissions(String currentBuildingId) {
        return buildingDao.getBuilding(currentBuildingId).getPermissions();
    }

    public boolean isUserMaster(String currentBuildingId) {
        return buildingDao.isUserMaster(currentBuildingId) == 1;
    }

    public void setDefault(String buildingId) {
        buildingDao.removeDefault();
        buildingDao.setDefault(buildingId);
    }
}
