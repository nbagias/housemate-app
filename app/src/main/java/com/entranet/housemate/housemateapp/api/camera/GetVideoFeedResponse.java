package com.entranet.housemate.housemateapp.api.camera;

import com.entranet.housemate.housemateapp.api.HomeApi;
import com.squareup.moshi.Json;

/**
 * API response for {@link HomeApi#getVideoFeed}
 */

public class GetVideoFeedResponse {
    @Json(name = "state")
    private Integer state;
    @Json(name = "description")
    private String description;
    @Json(name = "data")
    private GetVideoFeedResponseData data;

    public GetVideoFeedResponseData getData() { return data; }

    public void setData(GetVideoFeedResponseData data) { this.data = data; }

    public String getDescription() { return description; }

    public void setDescription(String description) { this.description = description; }

    public Integer getState() { return state; }

    public void setState(Integer state) { this.state = state; }

    public String getUrl() { return data.getUrl(); }
}
