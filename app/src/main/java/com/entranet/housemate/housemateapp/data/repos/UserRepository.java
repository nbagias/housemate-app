package com.entranet.housemate.housemateapp.data.repos;

import android.util.Log;

import com.entranet.housemate.housemateapp.BuildConfig;
import com.entranet.housemate.housemateapp.R;
import com.entranet.housemate.housemateapp.api.AdminApi;
import com.entranet.housemate.housemateapp.api.ApiResponse;
import com.entranet.housemate.housemateapp.api.building.BuildingData;
import com.entranet.housemate.housemateapp.api.building.GetBuildingsBody;
import com.entranet.housemate.housemateapp.api.building.GetBuildingsResponse;
import com.entranet.housemate.housemateapp.api.login.CredentialsBody;
import com.entranet.housemate.housemateapp.api.login.LoginResponse;
import com.entranet.housemate.housemateapp.data.AppExecutors;
import com.entranet.housemate.housemateapp.data.db.AppDatabase;
import com.entranet.housemate.housemateapp.data.db.dao.UserDao;
import com.entranet.housemate.housemateapp.data.db.entity.UserEntity;
import com.entranet.housemate.housemateapp.data.helpers.Constants;
import com.entranet.housemate.housemateapp.ui.init.StarterOpsInterface;
import com.entranet.housemate.housemateapp.ui.init.pin.CheckPinCallback;
import com.entranet.housemate.housemateapp.ui.init.splash.CheckUserStatusCallback;
import com.google.firebase.iid.FirebaseInstanceId;

import java.io.IOException;

import retrofit2.Response;

/**
 * Repository works as an abstraction level between control objects and the database.
 *
 * This UserRepository is used for the login/register/get buildings operations.
 * These are all user-domain operations, hence the name.
 *
 * Since, it will be used occasionally and doesn't need to stick around it's not
 * a singleton.
 *
 * It, only, needs a reference for the {@link AppDatabase}. It could be done internally, but
 * there is no reason to increase its responsibilities.
 */

public class UserRepository {

    private static final String TAG = UserRepository.class.getSimpleName();

    private final AdminApi adminApi;
    private final UserDao userDao;
    private final AppExecutors executors;

    public UserRepository(AdminApi adminApi, UserDao userDao, AppExecutors executors) {
        this.adminApi = adminApi;
        this.userDao = userDao;
        this.executors = executors;
    }

    public void setPin(String userPin) { executors.getDiskIO().execute(() -> userDao.setPin(userPin)); }

    public void checkPin(String userInput, CheckPinCallback callback) {
        executors.getDiskIO().execute(() -> {
            // Get the saved PIN.
            String userPin = userDao.getPin();
            // Compare.
            if (userPin != null && userPin.equals(userInput)) {
                callback.onPinRight();
            } else {
                callback.onPinWrong();
            }
        });
    }

    public void checkPinStatus(CheckPinCallback callback) {
        executors.getDiskIO().execute(() -> {
            // Get the saved PIN.
            String userPin = userDao.getPin();
            // If null, its the first time.
            if (userPin != null) {
                callback.onUserHasPin();
            } else {
                callback.onUserHasNoPin();
            }
        });
    }

    public void checkStatus(CheckUserStatusCallback callback) {
        executors.getDiskIO().execute(() -> {
            UserEntity activeUser = userDao.getActiveUser();
            if (activeUser == null) {
                Log.d(TAG, "No active user");
                callback.onUserNone();
            } else {
                Log.d(TAG, "Active user ID: " + activeUser.getId());
                callback.onUserExists();
            }
        });
    }

    public UserEntity getUser() { return userDao.getActiveUser(); }

    public void register(CredentialsBody body, StarterOpsInterface callback) {
        executors.getDiskIO().execute(() -> {
            // Perform the register request synchronously.
            try {
                Response<ApiResponse> apiResponse = adminApi.register(body).execute();
                if (apiResponse.isSuccessful()) {
                    ApiResponse response = apiResponse.body();
                    // Register response is simple. There is not much to check.
                    if (response != null) {
                        // Check the state of the response.
                        int state = response.getState();
                        if (state == Constants.API_RESPONSE_STATE_SUCCESSFUL) {
                            if (BuildConfig.DEBUG) Log.d(TAG, "Register success!");
                            login(body, callback);
                            return;
                        } else {
                            Log.e(TAG, "State: " + state);
                            Log.e(TAG, "Description: " + response.getDescription());
                        }
                    }
                }
                callback.onRegisterFailure();
            } catch (IOException e) {
                Log.e(TAG, "Exception raised:" + e.getMessage());
                callback.onRegisterFailure();
            }
        });
    }

    public void login(CredentialsBody body, StarterOpsInterface callback) {
        executors.getDiskIO().execute(() -> {
            // Set the firebase token.
            body.setFirebaseToken(FirebaseInstanceId.getInstance().getToken());
            // Perform the login request asynchronously.
            try {
                Response<LoginResponse> response = adminApi.login(body).execute();
                if (response.isSuccessful()) {
                    LoginResponse loginResponse = response.body();
                    if (loginResponse != null) {
                        // Check the state of the response.
                        int state = loginResponse.getState();
                        if (state == Constants.API_RESPONSE_STATE_SUCCESSFUL) {
                            String token = loginResponse.getToken();
                            if (BuildConfig.DEBUG) Log.d(TAG, "Token: " + token);
                            // Retrieve his buildings.
                            getUserBuildings(token, callback);
                            // Create a new user after getting the buildings to avoid erroneous situations.
                            insertNewUser(token, body.getEmail());
                            return;
                        } else {
                            Log.e(TAG, "State: " + state);
                            Log.e(TAG, "Description: " + loginResponse.getDescription());
                        }
                    } else {
                        Log.e(TAG, "Api login response is empty");
                    }
                }
                callback.onLoginFailure(R.string.login_error_message);
            } catch (IOException e) {
                Log.e(TAG, "Exception raised:" + e.getMessage());
                callback.onLoginFailure(R.string.login_error_message);
            }
        });
    }

    private void getUserBuildings(String token, StarterOpsInterface callback) {
        GetBuildingsBody body = new GetBuildingsBody(token);
        try {
            Response<GetBuildingsResponse> response = adminApi.getBuildings(body).execute();
            if (response.isSuccessful()) {
                GetBuildingsResponse buildingsResponse = response.body();
                if (buildingsResponse != null) {
                    int state = buildingsResponse.getState();
                    if (state == Constants.API_RESPONSE_STATE_SUCCESSFUL) {
                        BuildingData dataBuilding = buildingsResponse.getData();
                        if (dataBuilding != null) {
                            if (BuildConfig.DEBUG) Log.d(TAG, "Got buildings successfully");
                            callback.onLoginSuccess(dataBuilding);
                            return;
                        } else {
                            Log.d(TAG, "Buildings tag is null");
                        }
                    } else {
                        Log.d(TAG, "State: " + state);
                        Log.d(TAG, "Description: " + buildingsResponse.getDescription());
                    }
                } else {
                    Log.d(TAG, "Get buildings response null");
                }
            }
            callback.onLoginFailure(R.string.save_buildings_error_message);
        } catch (IOException e) {
            Log.e(TAG, "Exception raised:" + e.getMessage());
            callback.onLoginFailure(R.string.save_buildings_error_message);
        }
    }

    private void insertNewUser(String token, String email) {
        UserEntity newUser = new UserEntity();
        newUser.setToken(token);
        newUser.setEmail(email);
        newUser.setStatus(Constants.USER_STATUS_ACTIVE);
        // Save to the database.
        executors.getDiskIO().execute(() -> userDao.insertUser(newUser));
    }

    public void truncate() { executors.getDiskIO().execute(userDao::deleteAll); }
}
