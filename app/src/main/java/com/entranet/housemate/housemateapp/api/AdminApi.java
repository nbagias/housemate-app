package com.entranet.housemate.housemateapp.api;

import com.entranet.housemate.housemateapp.api.building.AddUserToBuildingBody;
import com.entranet.housemate.housemateapp.api.building.GetBuildingsBody;
import com.entranet.housemate.housemateapp.api.building.GetBuildingsResponse;
import com.entranet.housemate.housemateapp.api.guest.DeleteGuestBody;
import com.entranet.housemate.housemateapp.api.guest.GetGuestsBody;
import com.entranet.housemate.housemateapp.api.guest.GetGuestsResponse;
import com.entranet.housemate.housemateapp.api.guest.SetGuestPermBody;
import com.entranet.housemate.housemateapp.api.login.CredentialsBody;
import com.entranet.housemate.housemateapp.api.login.LoginResponse;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.POST;

/**
 * Admin API is a subset of Housemate API. It only contains admin-style operations.
 */

public interface AdminApi {

    @POST("/main/user/login")
    Call<LoginResponse> login(@Body CredentialsBody body);

    @POST("/main/user/register")
    Call<ApiResponse> register(@Body CredentialsBody body);

    @POST("/main/building/getBuildingsForUser")
    Call<GetBuildingsResponse> getBuildings(@Body GetBuildingsBody body);

    @POST("/main/building/addUserToBuilding")
    Call<ApiResponse> addUserToBuilding(@Body AddUserToBuildingBody body);

    @POST("/main/building/setPermissionsForUser")
    Call<ApiResponse> setPermissionForGuest(@Body SetGuestPermBody body);

    @POST("/main/building/getGuestsForBuilding")
    Call<GetGuestsResponse> getGuests(@Body GetGuestsBody body);

    @POST("/main/building/deleteGuestFromBuilding")
    Call<ApiResponse> deleteGuest(@Body DeleteGuestBody body);
}
