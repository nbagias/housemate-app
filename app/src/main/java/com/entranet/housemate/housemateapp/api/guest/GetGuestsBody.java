package com.entranet.housemate.housemateapp.api.guest;

import com.entranet.housemate.housemateapp.api.AdminApi;
import com.entranet.housemate.housemateapp.data.helpers.Constants;
import com.squareup.moshi.Json;

/**
 * API body request for {@link AdminApi#getGuests}}
 */

public class GetGuestsBody {
    @Json(name = "token")
    private String token;
    @Json(name = "buildingID")
    private String buildingID;
    @Json(name = "origin")
    private final int ORIGIN = Constants.ORIGIN_HOUSEMATE_APP;

    public void setBuildingID(String buildingID) { this.buildingID = buildingID; }

    public void setToken(String token) { this.token = token; }
}
