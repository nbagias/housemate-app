package com.entranet.housemate.housemateapp.ui.main.settings.location;

/**
 * Interface for routing action on the Location Settings activity.
 */

public interface LocationSettingsCallback {

    void onSetDefaultLocation(int location);
    void onSetDefaultTimezone(int timezone);

    void onSetCurrentLocation(String location);
    void onSetCurrentTimezone(String timezone);

    void onLoadSettingsCompleted();
}
