package com.entranet.housemate.housemateapp.data.db.dao;

import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Delete;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.OnConflictStrategy;
import android.arch.persistence.room.Query;
import android.arch.persistence.room.Update;

import com.entranet.housemate.housemateapp.data.db.entity.BuildingEntity;
import com.entranet.housemate.housemateapp.data.db.entity.DeviceEntity;
import com.entranet.housemate.housemateapp.data.db.entity.GuestEntity;

import java.util.List;

/**
 * Dao class for accessing {@link BuildingEntity}.
 */

@Dao
public interface BuildingDao {

    @Query("SELECT * FROM buildings")
    List<BuildingEntity> getAllBuildings();

    @Query("SELECT * FROM buildings WHERE id=:buildingId")
    BuildingEntity getBuilding(String buildingId);

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    void insertBuilding(BuildingEntity building);

    @Query("SELECT name FROM buildings WHERE id=:buildingId")
    String getCurrentBuildingName(String buildingId);

    @Query("DELETE FROM buildings")
    void deleteAll();

    @Query("DELETE FROM devices")
    void deleteDevices();

    @Query("SELECT master FROM buildings WHERE id=:buildingId")
    int isUserMaster(String buildingId);

    @Query("SELECT num_of_news FROM buildings WHERE id=:buildingId")
    int getNumOfNews(String buildingId);

    @Query("UPDATE buildings SET num_of_news=:value WHERE id=:buildingId")
    void setNumOfNews(int value, String buildingId);

    @Query("SELECT location FROM buildings WHERE id=:buildingId")
    String getLocation(String buildingId);

    @Query("UPDATE buildings SET location=:value WHERE id=:buildingId")
    void setLocation(String value, String buildingId);

    @Query("SELECT timezone FROM buildings WHERE id=:buildingId")
    String getTimezone(String buildingId);

    @Query("UPDATE buildings SET timezone=:value WHERE id=:buildingId")
    void setTimezone(String value, String buildingId);

    // Using building Dao for guest operations.
    @Query("SELECT * FROM guests WHERE building_id=:buildingId")
    List<GuestEntity> getGuests(String buildingId);

    @Query("DELETE FROM guests")
    void deleteAllGuests();

    @Delete
    void deleteGuest(GuestEntity entity);

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    void insertGuests(List<GuestEntity> guests);

    @Update
    void updateGuest(GuestEntity updatedGuest);

    // Devices
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    void insertDevices(List<DeviceEntity> devices);

    @Query("SELECT * FROM devices WHERE building_id=:buildingId")
    List<DeviceEntity> getDevices(String buildingId);

    @Query("UPDATE buildings SET is_default=1 WHERE id=:buildingId")
    void setDefault(String buildingId);

    @Query("UPDATE buildings SET is_default=0 WHERE is_default=1")
    void removeDefault();
}
