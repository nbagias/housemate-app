package com.entranet.housemate.housemateapp.api.action;

import com.entranet.housemate.housemateapp.api.HomeApi;
import com.squareup.moshi.Json;

/**
 * API body request for {@link HomeApi#updateAction}.
 */

public class UpdateActionBody extends CreateActionBody {
    @Json(name = "actionID")
    private String actionId;

    public UpdateActionBody(String type, String text, String time) {
        super(type, text, time);
    }

    public void setActionId(String actionId) { this.actionId = actionId; }
}
