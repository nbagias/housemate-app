package com.entranet.housemate.housemateapp.api;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.util.Log;

import com.entranet.housemate.housemateapp.R;
import com.entranet.housemate.housemateapp.data.helpers.Constants;

import java.io.IOException;
import java.io.InputStream;
import java.security.KeyManagementException;
import java.security.KeyStore;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.security.cert.Certificate;
import java.security.cert.CertificateException;
import java.security.cert.CertificateFactory;
import java.security.cert.X509Certificate;

import javax.net.ssl.HostnameVerifier;
import javax.net.ssl.SSLContext;
import javax.net.ssl.SSLSocketFactory;
import javax.net.ssl.TrustManager;
import javax.net.ssl.TrustManagerFactory;
import javax.net.ssl.X509TrustManager;

import okhttp3.OkHttpClient;
import retrofit2.Retrofit;
import retrofit2.converter.moshi.MoshiConverterFactory;

/**
 * ApiClient is a Retrofit client. All API requests work through here.
 */

public class ApiClient {

    private static final String TAG = ApiClient.class.getSimpleName();
    private static ApiClient instance;

    private Retrofit retrofit;

    private ApiClient(Context context) {
        TrustManager[] trustManagers = getTrustManagers(context);
        SSLSocketFactory socketFactory = getSSLSocketFactory(trustManagers);
        if (socketFactory != null) {
            OkHttpClient client = new OkHttpClient.Builder()
                    .sslSocketFactory(socketFactory, (X509TrustManager) trustManagers[0])
                    .hostnameVerifier(getHostnameVerifier())
                    .build();

            retrofit = new Retrofit.Builder()
                    .baseUrl(Constants.HOUSEMATE_SERVER)
                    .addConverterFactory(MoshiConverterFactory.create())
                    .client(client)
                    .build();
        }
    }

    public static synchronized ApiClient getClient(Context context) {
        if (instance == null) {
            instance = new ApiClient(context);
        }
        return instance;
    }

    private static HostnameVerifier getHostnameVerifier() {
        return (hostname, session) -> Constants.HOUSEMATE_HOSTNAME.equals(hostname);
    }

    public Retrofit getRetrofit() { return retrofit; }

    @Nullable
    private static SSLSocketFactory getSSLSocketFactory(TrustManager[] wrappedTrustManagers) {
        try {
            SSLContext sslContext = SSLContext.getInstance("TLS");
            sslContext.init(null, wrappedTrustManagers, null);

            return sslContext.getSocketFactory();

        } catch (NoSuchAlgorithmException | KeyManagementException e) {
            Log.e(TAG, "getSSLSocketFactory failed. Exception: " + e.getMessage());
        }
        return null;
    }

    @NonNull
    private static TrustManager[] getTrustManagers(Context context) {
        TrustManager[] defaultArr = new TrustManager[]{};
        try {
            CertificateFactory cf = CertificateFactory.getInstance("X.509");
            InputStream caInput = context.getResources().openRawResource(R.raw.cert);
            Certificate ca = cf.generateCertificate(caInput);
            caInput.close();

            Log.i(TAG, "Certificate: " +((X509Certificate) ca).getSubjectDN());
            KeyStore keyStore = KeyStore.getInstance("BKS");
            keyStore.load(null, null);
            keyStore.setCertificateEntry("ca", ca);

            String tmfAlgorithm = TrustManagerFactory.getDefaultAlgorithm();
            TrustManagerFactory tmf = TrustManagerFactory.getInstance(tmfAlgorithm);
            tmf.init(keyStore);

            return getWrappedTrustManagers(tmf.getTrustManagers());
        } catch (CertificateException | NoSuchAlgorithmException | KeyStoreException | IOException e) {
            Log.e(TAG, "getTrustManagers failed. Exception: " + e.getMessage());
        }
        return defaultArr;
    }

    private static TrustManager[] getWrappedTrustManagers(TrustManager[] trustManagers) {
        final X509TrustManager originalTrustManager = (X509TrustManager) trustManagers[0];
        return new TrustManager[]{
                new X509TrustManager() {
                    public X509Certificate[] getAcceptedIssuers() {
                        return originalTrustManager.getAcceptedIssuers();
                    }

                    public void checkClientTrusted(X509Certificate[] certs, String authType) {
                        try {
                            if (certs != null && certs.length > 0){
                                certs[0].checkValidity();
                            } else {
                                originalTrustManager.checkClientTrusted(certs, authType);
                            }
                        } catch (CertificateException e) {
                            Log.e(TAG, e.toString());
                        }
                    }

                    public void checkServerTrusted(X509Certificate[] certs, String authType) {
                        try {
                            if (certs != null && certs.length > 0){
                                certs[0].checkValidity();
                            } else {
                                originalTrustManager.checkServerTrusted(certs, authType);
                            }
                        } catch (CertificateException e) {
                            Log.e(TAG, e.toString());
                        }
                    }
                }
        };
    }
}
