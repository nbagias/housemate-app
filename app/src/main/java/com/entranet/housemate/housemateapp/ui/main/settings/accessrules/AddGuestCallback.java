package com.entranet.housemate.housemateapp.ui.main.settings.accessrules;

/**
 * Interface for retuning control to the caller.
 */

public interface AddGuestCallback {

    void onGuestInsertionSuccess();

    void onGuestInsertionFailure(String errorMessage);
}
