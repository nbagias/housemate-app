package com.entranet.housemate.housemateapp.ui.main.smarthome.scenes;


import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.Fragment;

import com.entranet.housemate.housemateapp.R;
import com.entranet.housemate.housemateapp.data.helpers.Constants;

/**
 * A simple {@link Fragment} subclass.
 * Use the {@link RemoveItemFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class RemoveItemFragment extends DialogFragment {

    private static final String ARG_REMOVE_TYPE = "removeType";
    private static final String ARG_SCENE_NAME = "sceneName";

    private String sceneName;

    private OnRemoveItemListener listener;

    public RemoveItemFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameter.
     *
     * @param removeType Removing all scenes or a specific one.
     * @param sceneName The selected scene's name, if any.
     * @return A new instance of fragment RemoveItemFragment.
     */
    public static RemoveItemFragment newInstance(int removeType, @Nullable String sceneName) {
        RemoveItemFragment fragment = new RemoveItemFragment();
        Bundle args = new Bundle();
        args.putInt(ARG_REMOVE_TYPE, removeType);
        args.putString(ARG_SCENE_NAME, sceneName);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            sceneName = getArguments().getString(ARG_SCENE_NAME);
        }
    }

    @NonNull
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        String title;
        if (getArguments().getInt(ARG_REMOVE_TYPE) == Constants.REMOVE_ALL_SCENES) {
            title = getString(R.string.scenes_remove_all_dialog_title);
        } else {
            title = String.format(getString(R.string.scenes_remove_dialog_title), sceneName);
        }
        return new AlertDialog.Builder(getActivity())
                .setIcon(android.R.drawable.ic_delete)
                .setTitle(title)
                .setPositiveButton(android.R.string.yes,
                        (dialog1, whichButton) -> {
                            if (listener != null) listener.onUserConfirmedRemoval(sceneName);
                            dismiss();
                        }
                )
                .setNegativeButton(android.R.string.no,
                        (dialog1, whichButton) -> dismiss()
                ).create();
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnRemoveItemListener) {
            listener = (OnRemoveItemListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnSaveNewSceneListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        listener = null;
    }

    public interface OnRemoveItemListener {
        void onUserConfirmedRemoval(String sceneName);
    }

}
