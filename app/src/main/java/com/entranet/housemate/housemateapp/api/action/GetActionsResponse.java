package com.entranet.housemate.housemateapp.api.action;

import com.entranet.housemate.housemateapp.api.HomeApi;
import com.squareup.moshi.Json;

/**
 * API response for {@link HomeApi#getActions}.
 */

public class GetActionsResponse {
    @Json(name = "state")
    private Integer state;
    @Json(name = "description")
    private String description;
    @Json(name = "data")
    private ActionsData data;

    public Integer getState() {
        return state;
    }

    public void setState(Integer state) {
        this.state = state;
    }

    public String getDescription() {
        return description;
    }

    public ActionsData getData() {
        return data;
    }

    public void setData(ActionsData data) {
        this.data = data;
    }
}
