package com.entranet.housemate.housemateapp.api;

import com.squareup.moshi.Json;

/**
 * Generic API response class. Useful when we don't expect any results.
 */

public class ApiResponse {
    @Json(name = "state")
    private Integer state;
    @Json(name = "description")
    private String description;

    public Integer getState() { return state; }

    public String getDescription() { return description; }

    public void setState(Integer state) { this.state = state; }

    public void setDescription(String description) { this.description = description; }
}
