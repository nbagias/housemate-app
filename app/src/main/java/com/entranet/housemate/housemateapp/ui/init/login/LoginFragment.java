package com.entranet.housemate.housemateapp.ui.init.login;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.TextInputLayout;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.EditorInfo;
import android.widget.Button;
import android.widget.EditText;

import com.entranet.housemate.housemateapp.R;
import com.entranet.housemate.housemateapp.data.helpers.InputHelper;

/**
 * This {@link Fragment} contains the login UI logic.
 * The validation checks are performed here, but the actual
 * login attempt is handled by the listener, through the
 * {@link OnLoginListener} interface.
 */
public class LoginFragment extends Fragment implements View.OnClickListener {

    private OnLoginListener listener;

    private TextInputLayout inputLayoutEmail;
    private TextInputLayout inputLayoutPassword;
    private EditText textEmail;
    private EditText textPassword;
    private Button btnLogin;

    public LoginFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_login, container, false);
        // Get references for the UI elements.
        inputLayoutEmail = view.findViewById(R.id.login_container_email);
        inputLayoutPassword = view.findViewById(R.id.login_container_password);
        textEmail = view.findViewById(R.id.login_edittext_email);
        textPassword = view.findViewById(R.id.login_edittext_password);
        textPassword.setOnEditorActionListener((v, actionId, event) -> {
            if (actionId == EditorInfo.IME_ACTION_GO) {
                loginUser();
                return true;
            }
            return false;
        });
        btnLogin = view.findViewById(R.id.login_button);
        btnLogin.setOnClickListener(this);

        return view;
    }


    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnLoginListener) {
            listener = (OnLoginListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnLoginListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        listener = null;
    }

    @Override
    public void onClick(View v) {
        // Check abnormal conditions.
        if (v.getId() != R.id.login_button) return;
        if (listener == null) return;
        // Prompt log in operation.
        loginUser();
    }

    /**
     * Performs the operation of validating and sending the info
     * to the listener. Also, wrapping the operation  in a function
     * avoids duplicate code (ime/button).
     */
    private void loginUser() {
        // Disable button. Snackbar listener will enable it again, if needed.
        btnLogin.setEnabled(false);
        // Validate email.
        String email = textEmail.getText().toString().trim();
        if (validateEmail(email)) {
            // Validate password.
            String password = textPassword.getText().toString().trim();
            //if (!validatePassword(password)) return;
            // All check passed, so hide the keyboard.
            InputHelper.hideKeyboard(getActivity());
            // Pass the info to listener activity.
            listener.onLoginClicked(email, password);
            // Return here. Snackbar listener will enable it again.
            return;
        }
        btnLogin.setEnabled(true);
    }

    /**
     * Validates the provided email and sets the error field appropriately.
     * Also, requests/clear focus for better user experience.
     *
     * @param email the provided email
     * @return true if valid, false otherwise
     */
    private boolean validateEmail(String email) {
        if (!InputHelper.isValidEmail(email)) {
            inputLayoutEmail.setError(getString(R.string.login_error_email_invalid));
            inputLayoutEmail.requestFocus();
            return false;
        } else {
            // Remove previous errors, if any.
            inputLayoutEmail.setError("");
            inputLayoutEmail.clearFocus();
            return true;
        }
    }

    /**
     * Validates the provided password and sets the error field appropriately.
     * Also, requests/clear focus for better user experience.
     *
     * @param password the provided password
     * @return true if valid, false otherwise
     */
    private boolean validatePassword(String password) {
        if (!InputHelper.isValidPassword(password)) {
            inputLayoutPassword.setError(getString(R.string.login_error_password_invalid));
            inputLayoutPassword.requestFocus();
            return false;
        } else {
            // Remove previous errors, if any.
            inputLayoutPassword.setError("");
            inputLayoutPassword.clearFocus();
            return true;
        }
    }
    /**
     * This interface must be implemented by StarterActivity, in order to
     * perform the login operation.
     */
    public interface OnLoginListener {
        void onLoginClicked(String email, String password);
    }
}
