package com.entranet.housemate.housemateapp.ui.main.actions;

import android.content.Context;
import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.support.v4.app.DialogFragment;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.entranet.housemate.housemateapp.R;
import com.entranet.housemate.housemateapp.data.DataRepository;
import com.entranet.housemate.housemateapp.data.db.entity.ActionEntity;
import com.entranet.housemate.housemateapp.data.db.entity.Day;
import com.entranet.housemate.housemateapp.data.db.entity.SceneEntity;
import com.entranet.housemate.housemateapp.data.helpers.Constants;
import com.entranet.housemate.housemateapp.data.helpers.ListMenu;
import com.entranet.housemate.housemateapp.ui.main.actions.adapters.DaysAdapter;
import com.entranet.housemate.housemateapp.ui.main.actions.adapters.DeviceOptionsAdapter;
import com.entranet.housemate.housemateapp.ui.main.actions.adapters.SceneOptionsAdapter;
import com.entranet.housemate.housemateapp.ui.main.actions.callbacks.ActionEditorCallback;
import com.entranet.housemate.housemateapp.ui.main.actions.custom.DatePickerFragment;
import com.entranet.housemate.housemateapp.ui.main.actions.custom.TimePickerFragment;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

public class ActionEditorActivity extends AppCompatActivity
        implements DatePickerFragment.DatePickerListener,
        TimePickerFragment.OnUserSeletionListener,
        SceneOptionsAdapter.SceneOptionsListener,
        ActionEditorCallback {

    private static final String EXTRA_DAYS_SELECTION = "selectedDays";
    private static final String EXTRA_ACTORS_SELECTION = "selectedActors";

    private DataRepository repository;

    private boolean[] actorsSelection;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_action_editor);

        repository = DataRepository.getInstance(getApplicationContext());

        setupUi(savedInstanceState == null);
    }

    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }

    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        // Save the current days selection.
        RecyclerView recyclerView = findViewById(R.id.action_editor_repeat_list);
        DaysAdapter adapter = (DaysAdapter) recyclerView.getAdapter();
        if (adapter != null) outState.putBooleanArray(EXTRA_DAYS_SELECTION, adapter.getCurrentState());
        // Save the current actors selection.
        RecyclerView actorsList = findViewById(R.id.action_editor_actors_container);
        String type = getIntent().getStringExtra(Constants.ACTION_TYPE);
        if (Constants.ACTION_TYPE_SCENARIO.equals(type)) {
            SceneOptionsAdapter optionsAdapter = (SceneOptionsAdapter) actorsList.getAdapter();
            if (optionsAdapter != null) outState.putBooleanArray(EXTRA_ACTORS_SELECTION, optionsAdapter.getCurrentState());
        } else {
            DeviceOptionsAdapter optionsAdapter = (DeviceOptionsAdapter) actorsList.getAdapter();
            if (optionsAdapter != null) outState.putBooleanArray(EXTRA_ACTORS_SELECTION, optionsAdapter.getCurrentState());
        }
        super.onSaveInstanceState(outState);
    }

    @Override
    protected void onRestoreInstanceState(Bundle savedInstanceState) {
        super.onRestoreInstanceState(savedInstanceState);
        // Update days
        RecyclerView recyclerView = findViewById(R.id.action_editor_repeat_list);
        DaysAdapter adapter = (DaysAdapter) recyclerView.getAdapter();
        if (adapter != null)adapter.updateMenu(savedInstanceState.getBooleanArray(EXTRA_DAYS_SELECTION));
        // Update actors.
        RecyclerView actorsList = findViewById(R.id.action_editor_actors_container);
        actorsSelection = savedInstanceState.getBooleanArray(EXTRA_ACTORS_SELECTION);
        String type = getIntent().getStringExtra(Constants.ACTION_TYPE);
        if (Constants.ACTION_TYPE_SCENARIO.equals(type)) {
            SceneOptionsAdapter optionsAdapter = (SceneOptionsAdapter) actorsList.getAdapter();
            if (optionsAdapter != null) optionsAdapter.updateMenu(actorsSelection);
        } else {
            DeviceOptionsAdapter optionsAdapter = (DeviceOptionsAdapter) actorsList.getAdapter();
            if (optionsAdapter != null) optionsAdapter.updateMenu(actorsSelection);
        }
    }

    @Override
    public void onDatePickerResult(int year, int month, int day) {
        // Construct the full date.
        final String selectedDate = String.format(Locale.getDefault(),"%02d/%02d/%d",day, month + 1, year);
        // Update UI.
        final TextView txtAddDate = findViewById(R.id.action_editor_date);
        runOnUiThread(() -> txtAddDate.setText(selectedDate));
    }

    @Override
    public void onTimePickerResult(int hoursOfDay, int minutes) {
        // Construct the full time.
        final String selectedTime = String.format(Locale.getDefault(), "%02d:%02d", hoursOfDay, minutes);
        // Update UI.
        final TextView txtAddTime = findViewById(R.id.action_editor_time);
        runOnUiThread(() -> txtAddTime.setText(selectedTime));
    }

    private void setupUi(boolean isInitial) {
        Toolbar toolbar = findViewById(R.id.toolbar);
        toolbar.setTitle("");
        setSupportActionBar(toolbar);
        ActionBar actionBar = getSupportActionBar();
        if (actionBar != null) actionBar.setDisplayHomeAsUpEnabled(true);

        // Time.
        TextView txtAddTime = findViewById(R.id.action_editor_time);
        txtAddTime.setOnClickListener(buttonListener);
        if (isInitial) txtAddTime.setText(R.string.action_editor_time_default);
        // Date.
        TextView txtAddDate = findViewById(R.id.action_editor_date);
        txtAddDate.setOnClickListener(buttonListener);
        if (isInitial) txtAddDate.setText(R.string.action_editor_date_default);
        // Repeat.
        RecyclerView recyclerView = findViewById(R.id.action_editor_repeat_list);
        recyclerView.setLayoutManager(new LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL, false));
        recyclerView.setAdapter(new DaysAdapter(ListMenu.getDaysList()));
        recyclerView.addItemDecoration(new DividerItemDecoration(this, DividerItemDecoration.HORIZONTAL));
        recyclerView.setHasFixedSize(true);
        // Title.
        EditText edtTitle = findViewById(R.id.action_editor_title_text);
        // Actors label.
        TextView txtActors = findViewById(R.id.action_editor_actors_label);
        RecyclerView actorsList = findViewById(R.id.action_editor_actors_container);
        actorsList.addItemDecoration(new DividerItemDecoration(this, DividerItemDecoration.VERTICAL));
        actorsList.setLayoutManager(new LinearLayoutManager(this));
        actorsList.setHasFixedSize(true);
        // Save button listener.
        Button btnSave = findViewById(R.id.bottom_button);
        btnSave.setText(R.string.save_button_title);
        btnSave.setOnClickListener(buttonListener);
        // First check the type and set the type-dependant fields accordingly.
        String type = getIntent().getStringExtra(Constants.ACTION_TYPE);
        String mode = getIntent().getStringExtra(Constants.ACTION_MODE);
        String actionId = getIntent().getStringExtra(Constants.ACTION_ID);
        switch (type) {
            case Constants.ACTION_TYPE_ALARM:
                edtTitle.setHint(R.string.action_editor_alarm_hint);
                edtTitle.setCompoundDrawablesWithIntrinsicBounds(null, getDrawable(R.drawable.main_ic_actions_alarm), null, null);
                txtActors.setText(R.string.action_editor_devices_label);
                repository.getDevicesForAction(mode, actionId, isInitial, this);
                break;
            case Constants.ACTION_TYPE_ALERT:
                edtTitle.setCompoundDrawablesWithIntrinsicBounds(null, getDrawable(R.drawable.main_ic_actions_alert), null, null);
                edtTitle.setHint(R.string.action_editor_alert_hint);
                txtActors.setText(R.string.action_editor_devices_label);
                repository.getDevicesForAction(mode, actionId, isInitial,this);
                break;
            case Constants.ACTION_TYPE_SCENARIO:
                edtTitle.setCompoundDrawablesWithIntrinsicBounds(null, getDrawable(R.drawable.main_ic_actions_scenario), null, null);
                edtTitle.setEnabled(false);
                txtActors.setText(R.string.action_editor_scenes_label);
                repository.getScenesForAction(mode, actionId, isInitial, this);
                break;
            default:
                break;
        }
    }

    @Override
    public void onScenesLoaded(List<ListMenu.SceneItem> scenes, boolean isInitial, ActionEntity action) {
        // Create the adapter.
        final SceneOptionsAdapter adapter = new SceneOptionsAdapter(scenes, this);
        // Set as source.
        final RecyclerView actorsList = findViewById(R.id.action_editor_actors_container);
        runOnUiThread(() -> actorsList.setAdapter(adapter));
        // If we are on edit mode and its the first load, setup UI.
        if (action != null && isInitial) setupActionUi(action);
    }

    @Override
    public void onDeviceLoaded(List<ListMenu.DeviceItem> devices, boolean isInitial, ActionEntity action) {
        // Create the adapter.
        final DeviceOptionsAdapter adapter = new DeviceOptionsAdapter(devices);
        // Set as source.
        final RecyclerView actorsList = findViewById(R.id.action_editor_actors_container);
        runOnUiThread(() ->  {
            actorsList.setAdapter(adapter);
            if (actorsSelection != null) adapter.updateMenu(actorsSelection);
        });   
        // If we are on edit mode, setup UI.
        if (action != null && isInitial) setupActionUi(action);
    }

    @Override
    public void onActionCreationFailed(final int resId) {
        runOnUiThread(() -> {
            showMessage(resId);
            Button btnSave = findViewById(R.id.bottom_button);
            btnSave.setEnabled(true);
        });
    }

    @Override
    public void onActionCreated() {
        // Indicate success.
        setResult(RESULT_OK);
        // Exit
        finish();
    }

    private void setupActionUi(ActionEntity action) {
        // Set the  actions's name.
        final String title = action.getText();
        if (title != null && !title.isEmpty()) {
            final TextView edtTitle = findViewById(R.id.action_editor_title_text);
            runOnUiThread(() -> edtTitle.setText(title));
        }
        // Set the action's time.
        final TextView txtAddTime = findViewById(R.id.action_editor_time);
        runOnUiThread(() -> txtAddTime.setText(action.getTime()));
        // If it is repeated leave the date placeholder as is and change the weekdays.
        if (action.isRepeated()) {
            RecyclerView weekdaysList = findViewById(R.id.action_editor_repeat_list);
            DaysAdapter adapter = (DaysAdapter) weekdaysList.getAdapter();
            adapter.updateMenu(action.getDay());
        } else {
            // Set the action's date.
            final TextView txtAddDate = findViewById(R.id.action_editor_date);
            runOnUiThread(() -> txtAddDate.setText(action.getDate()));
        }
    }

    private final View.OnClickListener buttonListener = v -> {
        switch (v.getId()) {
            case R.id.action_editor_date:
                DialogFragment dateFragment = new DatePickerFragment();
                dateFragment.show(getSupportFragmentManager(), DatePickerFragment.class.getSimpleName());
                break;
            case R.id.action_editor_time:
                DialogFragment timeFragment = new TimePickerFragment();
                timeFragment.show(getSupportFragmentManager(), TimePickerFragment.class.getSimpleName());
                break;
            case R.id.bottom_button:
                String type = getIntent().getStringExtra(Constants.ACTION_TYPE);
                String mode = getIntent().getStringExtra(Constants.ACTION_MODE);
                String actionId = getIntent().getStringExtra(Constants.ACTION_ID);
                saveCurrentAction(type, mode, actionId);
                break;
            default:
                break;
        }
    };

    private void saveCurrentAction(String type, String mode, String id) {
        // Disable save button.
        Button btnSave = findViewById(R.id.bottom_button);
        btnSave.setEnabled(false);
        // First make sure to invalidate the required fields.
        ActionEntity action = new ActionEntity();
        action.setActionId(id);
        action.setType(type);
        action.setEnabled(true);
        EditText edtTitle = findViewById(R.id.action_editor_title_text);
        if (Constants.ACTION_TYPE_ALERT.equals(type)) {
            // Naming is enforced only for alerts.
            String name = edtTitle.getText().toString().trim();
            if (name.isEmpty()) {
                showMessage(R.string.action_editor_title_error);
                btnSave.setEnabled(true);
                return;
            }
            action.setText(name);
        } else {
            action.setText(edtTitle.getText().toString().trim());
        }
        // Time.
        TextView txtTime = findViewById(R.id.action_editor_time);
        String time = txtTime.getText().toString();
        if (time.isEmpty() || time.equals(getString(R.string.action_editor_time_default))) {
            showMessage(R.string.action_editor_time_error);
            btnSave.setEnabled(true);
            return;
        }
        action.setTime(time);
        // First check for repeated mode.
        RecyclerView weekdaysList = findViewById(R.id.action_editor_repeat_list);
        DaysAdapter daysAdapter = (DaysAdapter) weekdaysList.getAdapter();
        Day actionDay = daysAdapter.getActionDay();
        // If null, its non-repeated mode, otherwise read the date field.
        if (actionDay != null) {
            action.setDay(actionDay);
            action.setRepeated(true);
        } else {
            TextView txtDate = findViewById(R.id.action_editor_date);
            String date = txtDate.getText().toString();
            if (date.isEmpty() || date.equals(getString(R.string.action_editor_date_default))) {
                showMessage(R.string.action_editor_date_error);
                btnSave.setEnabled(true);
                return;
            }
            // Also, check whether the selected time belongs to the past.
            Calendar selectedTime = Calendar.getInstance();
            Calendar c = Calendar.getInstance();
            DateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy hh:mm", Locale.getDefault());
            try {
                Date selectedDate = dateFormat.parse(date + " " + time);
                selectedTime.setTime(selectedDate);
            } catch (ParseException e) {
                showMessage(R.string.action_editor_date_error);
                btnSave.setEnabled(true);
                return;
            }
            if (selectedTime.getTimeInMillis() < c.getTimeInMillis()){
                showMessage(R.string.action_editor_date_error_past);
                btnSave.setEnabled(true);
                return;
            }
            Day day = new Day();
            day.setDate(date);
            action.setDay(day);
            action.setRepeated(false);
        }
        // Actors.
        List<String> selectedDeviceIds = new ArrayList<>();
        RecyclerView actorsList = findViewById(R.id.action_editor_actors_container);
        if (Constants.ACTION_TYPE_SCENARIO.equals(type)) {
            SceneOptionsAdapter adapter = (SceneOptionsAdapter) actorsList.getAdapter();
            SceneEntity selectedScene = adapter.getSelectedScene();
            if (selectedScene == null) {
                showMessage(R.string.action_editor_scene_options_error);
                btnSave.setEnabled(true);
                return;
            }
        } else {
            DeviceOptionsAdapter adapter = (DeviceOptionsAdapter) actorsList.getAdapter();
            selectedDeviceIds = adapter.getSelectedDeviceIds();
            if (selectedDeviceIds.isEmpty()) {
                showMessage(R.string.action_editor_device_options_error);
                btnSave.setEnabled(true);
                return;
            }
        }
        action.setDevices(selectedDeviceIds);
        // Action constructed, now update the server.
        repository.addAction(action, mode, this);
    }

    private void showMessage(int resId) {
        Snackbar popup = Snackbar.make(findViewById(R.id.action_editor_root_container), resId, Snackbar.LENGTH_SHORT);
        popup.show();
    }

    @Override
    public void onSceneSelected(String sceneName) {
        EditText edtTitle = findViewById(R.id.action_editor_title_text);
        edtTitle.setText(sceneName);
    }
}
