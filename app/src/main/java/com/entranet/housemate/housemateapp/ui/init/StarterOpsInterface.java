package com.entranet.housemate.housemateapp.ui.init;

import com.entranet.housemate.housemateapp.api.building.BuildingData;

/**
 * Interface for taking action after the starter operations.
 */

public interface StarterOpsInterface {

    void onRegisterFailure();

    void onLoginSuccess(BuildingData data);

    void onLoginFailure(int errorId);

    void onSaveBuildingsCompleted();

}
