package com.entranet.housemate.housemateapp.api;

import com.entranet.housemate.housemateapp.api.action.CreateActionBody;
import com.entranet.housemate.housemateapp.api.action.DeleteActionBody;
import com.entranet.housemate.housemateapp.api.action.GetActionsBody;
import com.entranet.housemate.housemateapp.api.action.GetActionsResponse;
import com.entranet.housemate.housemateapp.api.action.UpdateActionBody;
import com.entranet.housemate.housemateapp.api.camera.GetVideoFeedBody;
import com.entranet.housemate.housemateapp.api.camera.GetVideoFeedResponse;
import com.entranet.housemate.housemateapp.api.scene.ActivateSceneBody;
import com.entranet.housemate.housemateapp.api.scene.DeleteSceneBody;
import com.entranet.housemate.housemateapp.api.scene.GetScenesBody;
import com.entranet.housemate.housemateapp.api.scene.GetScenesResponse;
import com.entranet.housemate.housemateapp.api.scene.InsertSceneBody;
import com.entranet.housemate.housemateapp.api.switches.GetSwitchesBody;
import com.entranet.housemate.housemateapp.api.switches.GetSwitchesResponse;
import com.entranet.housemate.housemateapp.api.switches.UpdateSwitchNameBody;
import com.entranet.housemate.housemateapp.api.switches.UpdateSwitchStateBody;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.POST;

/**
 * HomeApi API is a subset of Housemate API. It contains operations related to scenes, switches and actions.
 */

public interface HomeApi {

    @POST("/housemate/home/switchGetList")
    Call<GetSwitchesResponse> getSwitches(@Body GetSwitchesBody body);

    @POST("/housemate/home/switchChangeState")
    Call<ApiResponse> updateSwitchState(@Body UpdateSwitchStateBody body);

    @POST("/housemate/home/switchChangeName")
    Call<ApiResponse> updateSwitchName(@Body UpdateSwitchNameBody body);

    @POST("/housemate/home/sceneGetList")
    Call<GetScenesResponse> getScenes(@Body GetScenesBody body);

    @POST("/housemate/home/sceneActivate")
    Call<ApiResponse> activateScene(@Body ActivateSceneBody body);

    @POST("/housemate/home/sceneInsert")
    Call<ApiResponse> insertScene(@Body InsertSceneBody body);

    @POST("/housemate/home/sceneDelete")
    Call<ApiResponse> deleteScene(@Body DeleteSceneBody body);

    @POST("/housemate/action/createAction")
    Call<ApiResponse> createAction(@Body CreateActionBody body);

    @POST("/housemate/action/updateAction")
    Call<ApiResponse> updateAction(@Body UpdateActionBody body);

    @POST("/housemate/action/deleteAction")
    Call<ApiResponse> deleteAction(@Body DeleteActionBody body);

    @POST("/housemate/action/getActions")
    Call<GetActionsResponse> getActions(@Body GetActionsBody body);

    @POST("/housemate/video/requestVideoFeed")
    Call<GetVideoFeedResponse> getVideoFeed(@Body GetVideoFeedBody body);

    @POST("/housemate/video/requestVideoCall")
    Call<GetVideoFeedResponse> getVideoCall(@Body GetVideoFeedBody body);
}
