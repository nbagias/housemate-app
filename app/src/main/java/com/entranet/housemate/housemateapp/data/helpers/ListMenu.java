package com.entranet.housemate.housemateapp.data.helpers;

import com.entranet.housemate.housemateapp.R;
import com.entranet.housemate.housemateapp.data.db.entity.DeviceEntity;
import com.entranet.housemate.housemateapp.data.db.entity.SceneEntity;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

/**
 * Helper class
 * Populates all the static menus as a list of objects.
 */

public class ListMenu {

    private ListMenu() {}

    public static List<MenuItem> getMainList() {
        ArrayList<MenuItem> mainList = new ArrayList<>();
        mainList.add(new MenuItem(Constants.SWITCHES, R.string.menu_switches, R.drawable.main_ic_smart_home_devices));
        mainList.add(new MenuItem(Constants.SCENES, R.string.menu_scenes, R.drawable.main_ic_smart_home_scenes));
        mainList.add(new MenuItem(Constants.CAMERA, R.string.menu_camera, R.drawable.main_ic_smart_home_camera));
        return mainList;
    }

    public static List<MenuItem> getActionsList() {
        ArrayList<MenuItem> actionsList = new ArrayList<>();
        actionsList.add(new MenuItem(Constants.ALARM, R.string.menu_alarm, R.drawable.main_ic_actions_alarm));
        actionsList.add(new MenuItem(Constants.ALERT, R.string.menu_alert, R.drawable.main_ic_actions_alert));
        actionsList.add(new MenuItem(Constants.SCENARIOS, R.string.menu_scenarios, R.drawable.main_ic_actions_scenario));
        return actionsList;
    }

    public static List<MenuItem> getSettingsList() {
        ArrayList<MenuItem> settingsList = new ArrayList<>();
        settingsList.add(new MenuItem(Constants.ACCESS_RULES, R.string.menu_access_rules, R.drawable.main_ic_settings_access_rules));
        settingsList.add(new MenuItem(Constants.LOCATION, R.string.menu_location, R.drawable.main_ic_location));
        settingsList.add(new MenuItem(Constants.NEWS, R.string.menu_news, R.drawable.main_ic_settings_news));
        settingsList.add(new MenuItem(Constants.NEW_DEVICE, R.string.menu_new_device, R.drawable.main_ic_settings_new_device));
        settingsList.add(new MenuItem(Constants.LOGOUT, R.string.menu_logout, R.drawable.main_ic_logout));
        return settingsList;
    }

    public static List<SettingsItem> getLocationSettingsList() {
        ArrayList<SettingsItem> locSettingsList = new ArrayList<>();
        locSettingsList.add(new SettingsItem(Constants.LOCATION_WEATHER, R.string.location_weather_title, "",
                R.string.location_weather_summary, R.drawable.main_ic_settings_weather));
        locSettingsList.add(new SettingsItem(Constants.LOCATION_TIMEZONE, R.string.location_timezone_title, "",
                R.string.location_timezone_summary, R.drawable.main_ic_settings_timezone));
        return locSettingsList;
    }

    public static List<DayItem> getDaysList() {
        ArrayList<DayItem> week = new ArrayList<>();

        week.add(new DayItem(Calendar.MONDAY, "M", R.drawable.round_button, false));
        week.add(new DayItem(Calendar.TUESDAY, "T", R.drawable.round_button, false));
        week.add(new DayItem(Calendar.WEDNESDAY, "W", R.drawable.round_button, false));
        week.add(new DayItem(Calendar.THURSDAY, "T", R.drawable.round_button, false));
        week.add(new DayItem(Calendar.FRIDAY, "F", R.drawable.round_button, false));
        week.add(new DayItem(Calendar.SATURDAY, "S", R.drawable.round_button, false));
        week.add(new DayItem(Calendar.SUNDAY, "S", R.drawable.round_button, false));

        return week;
    }

    public static class DayItem {
        private int id;
        private String title;
        private int icon;
        private boolean isSelected;

        DayItem(int id, String title, int icon, boolean isSelected) {
            this.id = id;
            this.title = title;
            this.icon = icon;
            this.isSelected = isSelected;
        }

        public int getId() { return id; }

        public String getTitle() { return title; }

        public int getIcon() { return icon; }

        public boolean isSelected() { return isSelected; }

        public void setSelected(boolean selected) { isSelected = selected; }
    }

    public static class DeviceItem {
        private DeviceEntity device;
        private boolean isSelected;

        public DeviceItem(DeviceEntity device, boolean isSelected) {
            this.device = device;
            this.isSelected = isSelected;
        }

        public boolean isSelected() {
            return isSelected;
        }

        public void setSelected(boolean selected) {
            isSelected = selected;
        }

        public String getName() {
            return device.getName();
        }

        public String getId() { return device.getId(); }
    }

    public static class SceneItem {
        private SceneEntity scene;
        private boolean isSelected;

        public SceneItem(SceneEntity scene, boolean isSelected) {
            this.scene = scene;
            this.isSelected = isSelected;
        }

        public boolean isSelected() {
            return isSelected;
        }

        public void setSelected(boolean selected) {
            isSelected = selected;
        }

        public String getName() {
            return scene.getName();
        }

        public void setName(String name) {
            scene.setName(name);
        }

        public SceneEntity getScene() {
            return scene;
        }
    }

    public static class MenuItem {
        private int id;
        private int title;
        private int icon;

        MenuItem(int id, int title, int icon) {
            this.id = id;
            this.title = title;
            this.icon = icon;
        }

        public int getId() {
            return id;
        }

        public int getIcon() {
            return icon;
        }

        public int getTitle() {
            return title;
        }
    }

    public static class SettingsItem {
        private int id;
        private int title;
        private String value;
        private int summary;
        private int icon;

        SettingsItem(int id, int title, String value, int summary, int icon) {
            this.id = id;
            this.title = title;
            this.value = value;
            this.summary = summary;
            this.icon = icon;
        }

        public int getId() {
            return id;
        }

        public int getTitle() {
            return title;
        }

        public int getSummary() { return summary; }

        public String getValue() { return value; }

        public void setValue(String value) { this.value = value; }

        public int getIcon() {
            return icon;
        }
    }
}