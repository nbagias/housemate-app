package com.entranet.housemate.housemateapp.api.building;

import com.squareup.moshi.Json;

/**
 * Model class for the API response on the get buildings request.
 */

public class GetBuildingsResponse {
    @Json(name = "state")
    private Integer state;
    @Json(name = "description")
    private String description;
    @Json(name = "data")
    private BuildingData data;

    public Integer getState() {
        return state;
    }

    public void setState(Integer state) {
        this.state = state;
    }

    public String getDescription() {
        return description;
    }

    public BuildingData getData() {
        return data;
    }

    public void setData(BuildingData data) {
        this.data = data;
    }
}
