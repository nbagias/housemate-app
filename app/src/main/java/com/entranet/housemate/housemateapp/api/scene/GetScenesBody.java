package com.entranet.housemate.housemateapp.api.scene;

import com.entranet.housemate.housemateapp.api.HomeApi;
import com.entranet.housemate.housemateapp.data.helpers.Constants;
import com.squareup.moshi.Json;

/**
 * API body request for {@link HomeApi#getScenes}.
 */

public class GetScenesBody {
    @Json(name = "token")
    private String token;
    @Json(name = "buildingID")
    private String buildingID;
    @Json(name = "origin")
    private final int ORIGIN = Constants.ORIGIN_HOUSEMATE_APP;

    public GetScenesBody(String token, String buildingID) {
        this.token = token;
        this.buildingID = buildingID;
    }

    public String getToken() { return token; }

    public void setToken(String token) { this.token = token; }

    public String getBuildingID() { return buildingID; }

    public void setBuildingID(String buildingID) { this.buildingID = buildingID; }
}
