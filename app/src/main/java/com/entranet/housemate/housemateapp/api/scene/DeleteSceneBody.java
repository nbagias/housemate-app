package com.entranet.housemate.housemateapp.api.scene;

import com.entranet.housemate.housemateapp.data.helpers.Constants;
import com.squareup.moshi.Json;

/**
 * API request body for scene deletion.
 */

public class DeleteSceneBody {
    @Json(name = "token")
    private String token;
    @Json(name = "buildingID")
    private String buildingID;
    @Json(name = "name")
    private String name;
    @Json(name = "origin")
    private final int ORIGIN = Constants.ORIGIN_HOUSEMATE_APP;

    public DeleteSceneBody(String name) { this.name = name; }

    public void setToken(String token) { this.token = token; }

    public void setBuildingID(String buildingID) { this.buildingID = buildingID; }
}
