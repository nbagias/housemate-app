package com.entranet.housemate.housemateapp.ui.init.splash;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;

import com.entranet.housemate.housemateapp.data.DataRepository;
import com.entranet.housemate.housemateapp.ui.init.StarterActivity;
import com.entranet.housemate.housemateapp.ui.init.pin.PinActivity;

/**
 * Splash Activity has two uses:
 * a) provides a splash screen
 * b) decides whether a user registration/login is required
 */
public class SplashActivity extends Activity implements CheckUserStatusCallback {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        if (!isTaskRoot()) {
            finish();
            return;
        }
        // Initialize repository in the background since it's the first call and it could take a while.
        Thread initThread = new Thread(() -> {
            DataRepository repository = DataRepository.getInstance(getApplicationContext());
            // Check user status.
            repository.checkUserStatus(SplashActivity.this);
        });
        initThread.start();
    }

    @Override
    public void onUserExists() {
        Intent intent = new Intent(SplashActivity.this, PinActivity.class);
        startActivity(intent);
        finish();
    }

    @Override
    public void onUserNone() {
        Intent intent = new Intent(SplashActivity.this, StarterActivity.class);
        startActivity(intent);
        finish();
    }
}
