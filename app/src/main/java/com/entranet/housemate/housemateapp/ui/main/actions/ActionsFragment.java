package com.entranet.housemate.housemateapp.ui.main.actions;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.entranet.housemate.housemateapp.R;
import com.entranet.housemate.housemateapp.data.helpers.ListMenu;
import com.entranet.housemate.housemateapp.ui.main.custom.MenuItemAdapter;
import com.entranet.housemate.housemateapp.ui.main.custom.SelectionsListener;

/**
 * A fragment representing a list of Items.
 * <p/>
 * Activities containing this fragment MUST implement the {@link OnActionsListener}
 * interface.
 */
public class ActionsFragment extends Fragment {

    private OnActionsListener listener;

    /**
     * Mandatory empty constructor for the fragment manager to instantiate the
     * fragment (e.g. upon screen orientation changes).
     */
    public ActionsFragment() {
    }

    @SuppressWarnings("unused")
    public static ActionsFragment newInstance(int columnCount) {
        return new ActionsFragment();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_tab_actions_list, container, false);

        // Set the adapter
        if (view instanceof RecyclerView) {
            Context context = view.getContext();
            RecyclerView recyclerView = (RecyclerView) view;
            recyclerView.setLayoutManager(new LinearLayoutManager(context));
            recyclerView.setAdapter(new MenuItemAdapter(ListMenu.getActionsList(), listener));
            recyclerView.setHasFixedSize(true);
        }
        return view;
    }


    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnActionsListener) {
            listener = (OnActionsListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnActionsListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        listener = null;
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p/>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnActionsListener extends SelectionsListener {
        @Override
        void onMenuItemSelected(ListMenu.MenuItem item);
    }
}
