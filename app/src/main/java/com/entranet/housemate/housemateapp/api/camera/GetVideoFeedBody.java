package com.entranet.housemate.housemateapp.api.camera;

import com.entranet.housemate.housemateapp.api.HomeApi;
import com.squareup.moshi.Json;

/**
 * API body request for {@link HomeApi#getVideoFeed}
 */

public class GetVideoFeedBody {
    @Json(name = "token")
    private String token;
    @Json(name = "buildingID")
    private String buildingId;
    @Json(name = "deviceID")
    private String deviceId;
    @Json(name = "origin")
    private final int ORIGIN = 4;

    public GetVideoFeedBody(String deviceId) { this.deviceId = deviceId; }

    public void setToken(String token) { this.token = token; }

    public void setBuildingId(String buildingId) { this.buildingId = buildingId; }
}
