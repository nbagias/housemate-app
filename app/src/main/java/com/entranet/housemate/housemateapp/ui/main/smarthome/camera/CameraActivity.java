package com.entranet.housemate.housemateapp.ui.main.smarthome.camera;

import android.Manifest;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.Snackbar;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;

import com.entranet.housemate.housemateapp.R;
import com.entranet.housemate.housemateapp.data.DataRepository;
import com.entranet.housemate.housemateapp.data.db.entity.DeviceEntity;
import com.entranet.housemate.housemateapp.data.helpers.Constants;

import java.util.List;

import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

public class CameraActivity extends AppCompatActivity implements
        GetVideoUrlCallback,
        LoadDevicesCallback,
        CameraOptionsAdapter.CameraOptionListener {

    private static final int PERMISSION_REQUEST_VIDEO = 10;

    private String url;
    private RecyclerView recycler;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_camera);

        setupUi();

        DataRepository repository = DataRepository.getInstance(getApplicationContext());
        // Load device list.
        repository.getDeviceList(this);
    }

    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }

    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }

    @Override
    public void onVideoUrlReady(final String url) {
        this.url = url;
        // Check permissions.
        int permissionCamera = ContextCompat.checkSelfPermission(this, Manifest.permission.CAMERA);
        int permissionRecordAudio = ContextCompat.checkSelfPermission(this, Manifest.permission.RECORD_AUDIO);
        if (permissionCamera == PackageManager.PERMISSION_GRANTED || permissionRecordAudio == PackageManager.PERMISSION_GRANTED) {
            startStreaming(url);
        } else {
            ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.CAMERA, Manifest.permission.RECORD_AUDIO}, PERMISSION_REQUEST_VIDEO);
        }
    }

    @Override
    public void onVideoUrlUnavailable() {
        Snackbar snackbar = Snackbar.make(findViewById(R.id.camera_container), "Could not get camera feed. Try again.", Snackbar.LENGTH_SHORT);
        snackbar.show();
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        if (requestCode == PERMISSION_REQUEST_VIDEO) {
            // Request for camera permission.
            if (grantResults.length == 2 && grantResults[0] == PackageManager.PERMISSION_GRANTED
                    && grantResults[1] == PackageManager.PERMISSION_GRANTED) {
                startStreaming(url);
            } else {
                // Permission request was denied.
                Snackbar.make(findViewById(R.id.camera_container), "Camera permission request was denied.", Snackbar.LENGTH_SHORT).show();
            }
        }
    }

    @Override
    public void onDeviceListLoaded(List<DeviceEntity> devices) {
        runOnUiThread(() -> recycler.setAdapter(new CameraOptionsAdapter(devices, this)));
    }

    @Override
    public void onCameraOptionSelected(String deviceId, boolean isCall) {
        DataRepository repository = DataRepository.getInstance(getApplicationContext());
        repository.getVideoFeed(deviceId, isCall, this);
    }

    private void startStreaming(String url) {
        Intent intent = new Intent(this, StreamingActivity.class);
        intent.putExtra(Constants.EXTRA_STREAMING_URL, url);
        startActivity(intent);
    }

    private void setupUi() {
        Toolbar toolbar = findViewById(R.id.toolbar);
        toolbar.setTitle("");
        setSupportActionBar(toolbar);
        ActionBar actionBar = getSupportActionBar();
        if (actionBar != null) actionBar.setDisplayHomeAsUpEnabled(true);

        recycler = findViewById(R.id.camera_recycler);
        recycler.addItemDecoration(new DividerItemDecoration(this, DividerItemDecoration.VERTICAL));
        recycler.setLayoutManager(new LinearLayoutManager(this));
        recycler.setHasFixedSize(true);
    }
}
