package com.entranet.housemate.housemateapp.ui.main.custom;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.entranet.housemate.housemateapp.R;
import com.entranet.housemate.housemateapp.data.helpers.ListMenu;
import com.entranet.housemate.housemateapp.ui.main.smarthome.SmartHomeFragment.OnSmartHomeListener;

import java.util.List;

/**
 * {@link RecyclerView.Adapter} that can display a {@link ListMenu.MenuItem} and makes a call to the
 * specified {@link OnSmartHomeListener}.
 */
public class MenuItemAdapter extends RecyclerView.Adapter<MenuItemAdapter.ViewHolder> {

    private final List<ListMenu.MenuItem> itemList;
    private int heightRatio;
    private final SelectionsListener listener;

    public MenuItemAdapter(List<ListMenu.MenuItem> items, SelectionsListener listener) {
        this.itemList = items;
        this.heightRatio = items.size();
        this.listener = listener;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.fragment_tab_menuitem, parent, false);
        // 100 is for adjustment (3/5 ratio is not really helping with the margins)
        view.getLayoutParams().height = (parent.getHeight() - 100) / heightRatio;
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull final ViewHolder holder, int position) {
        ListMenu.MenuItem item = itemList.get(position);
        holder.item = item;
        holder.icon =  item.getIcon();
        holder.textView.setText(item.getTitle());
        holder.textView.setCompoundDrawablesWithIntrinsicBounds(holder.icon, 0, 0, 0);
        holder.view.setOnClickListener(v -> {
            if (null != listener) {
                // Notify the active callbacks interface (the activity, if the
                // fragment is attached to one) that an item has been selected.
                listener.onMenuItemSelected(holder.item);
            }
        });
    }

    @Override
    public int getItemCount() {
        return itemList.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        private final View view;
        private final TextView textView;
        private int icon;
        private ListMenu.MenuItem item;

        ViewHolder(View view) {
            super(view);
            this.view = view;
            textView = view.findViewById(R.id.menu_item);
        }

        @Override
        public String toString() {
            return super.toString() + " '" + textView.getText() + "'";
        }
    }
}
