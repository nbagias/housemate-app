package com.entranet.housemate.housemateapp.ui.main.actions.adapters;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.entranet.housemate.housemateapp.R;
import com.entranet.housemate.housemateapp.data.db.entity.ActionEntity;
import com.entranet.housemate.housemateapp.data.helpers.Constants;
import com.entranet.housemate.housemateapp.ui.main.actions.AlarmActivity;
import com.suke.widget.SwitchButton;

import java.util.List;

/**
 * Adapter class for populating the action list.
 */

public class ActionAdapter extends RecyclerView.Adapter<ActionAdapter.ActionViewHolder> {

    private static final int EMPTY_VIEW = 10;

    private final List<ActionEntity> actions;
    private final ActionListCallback callback;

    public ActionAdapter(List<ActionEntity> actions, ActionListCallback callback) {
        this.actions = actions;
        this.callback = callback;
    }

    @Override
    public ActionAdapter.ActionViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        if (viewType == EMPTY_VIEW) {
            return new EmptyViewHolder(LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.menuitem_empty, parent, false));
        } else {
            return new ActionViewHolder(LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.menuitem_action, parent, false));
        }
    }

    @Override
    public void onBindViewHolder(ActionAdapter.ActionViewHolder holder, int position) {
        if (!(holder instanceof EmptyViewHolder)) {
            ActionEntity entity = actions.get(position);
            switch (entity.getType()) {
                case Constants.ACTION_TYPE_ALARM:
                    holder.txtTitle.setText(entity.getTime());
                    break;
                case Constants.ACTION_TYPE_ALERT:
                    holder.txtTitle.setText(entity.getText());
                    break;
                case Constants.ACTION_TYPE_SCENARIO:
                    holder.txtTitle.setText(entity.getText());
                    break;
                default:
                    break;
            }
            // Set toggle button initial state.
            holder.switchButton.setChecked(entity.isEnabled());
        }
    }

    @Override
    public int getItemViewType(int position) {
        if (actions.isEmpty()) {
            return EMPTY_VIEW;
        }
        return super.getItemViewType(position);
    }

    @Override
    public int getItemCount() {
        if (actions.isEmpty()){
            return 1;
        } else {
            return actions.size();
        }
    }

    class ActionViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener, View.OnLongClickListener, SwitchButton.OnCheckedChangeListener {
        private final TextView txtTitle;
        private final SwitchButton switchButton;

        ActionViewHolder (View itemView) {
            super(itemView);
            switchButton = itemView.findViewById(R.id.menu_item_action_toggle);
            txtTitle = itemView.findViewById(R.id.menu_item_action_title);
            if (txtTitle == null) return; // For the empty view constructor.
            txtTitle.setOnClickListener(this);
            txtTitle.setOnLongClickListener(this);
            switchButton.setOnCheckedChangeListener(this);
        }

        @Override
        public void onClick(View v) {
            ActionEntity entity = actions.get(getAdapterPosition());
            callback.onActionItemClicked(entity);
        }

        @Override
        public boolean onLongClick(View v) {
            ActionEntity entity = actions.get(getAdapterPosition());
            callback.onActionItemLongClicked(entity);
            return true;
        }

        @Override
        public void onCheckedChanged(SwitchButton view, boolean isChecked) {
            ActionEntity entity = actions.get(getAdapterPosition());
            callback.onActionCheckedChanged(entity, isChecked);
        }
    }

    class EmptyViewHolder extends ActionAdapter.ActionViewHolder {
        EmptyViewHolder(View itemView) {
            super(itemView);
        }
    }

    /**
     *  Callback for click events on action items.
     *  Implemented by {@link AlarmActivity} etc.
     */
    public interface ActionListCallback {
        void onActionItemClicked(ActionEntity action);

        void onActionItemLongClicked(ActionEntity action);

        void onActionCheckedChanged(ActionEntity action, boolean isChecked);
    }
}
