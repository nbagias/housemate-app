package com.entranet.housemate.housemateapp.ui.init.splash;

/**
 * Interface for routing actions from the splash activity, after the status of the user is checked.
 */

public interface CheckUserStatusCallback {

    /**
     * Executes when the user has already signed-in.
     */
    void onUserExists();

    /**
     * Executes when there is no signed-in user.
     */
    void onUserNone();
}
