package com.entranet.housemate.housemateapp.api.switches;

import com.entranet.housemate.housemateapp.api.HomeApi;
import com.entranet.housemate.housemateapp.data.helpers.Constants;
import com.squareup.moshi.Json;

/**
 * API body request for {@link HomeApi#getSwitches}.
 */

public class GetSwitchesBody {

    @Json(name = "token")
    private String token;
    @Json(name = "buildingID")
    private String buildingID;
    @Json(name = "origin")
    private final int ORIGIN = Constants.ORIGIN_HOUSEMATE_APP;

    public GetSwitchesBody(String token, String buildingID) {
        this.token = token;
        this.buildingID = buildingID;
    }
    // Accessors
    public String getToken() { return token; }

    public void setToken(String token) { this.token = token; }

    public String getBuildingID() { return buildingID; }

    public void setBuildingID(String buildingID) { this.buildingID = buildingID; }
}
