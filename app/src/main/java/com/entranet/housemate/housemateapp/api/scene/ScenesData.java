package com.entranet.housemate.housemateapp.api.scene;

import com.entranet.housemate.housemateapp.data.db.entity.SceneEntity;
import com.squareup.moshi.Json;

import java.util.List;

/**
 * The scenes list, nested in the {@link GetScenesResponse}.
 */

public class ScenesData {
    @Json(name = "scenes")
    private List<SceneEntity> scenes;

    public List<SceneEntity> getScenes() { return scenes; }

    public void setScenes(List<SceneEntity> scenes) { this.scenes = scenes; }
}
