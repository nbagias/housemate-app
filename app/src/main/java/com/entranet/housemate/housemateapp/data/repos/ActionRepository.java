package com.entranet.housemate.housemateapp.data.repos;

import android.util.Log;

import com.entranet.housemate.housemateapp.BuildConfig;
import com.entranet.housemate.housemateapp.R;
import com.entranet.housemate.housemateapp.api.ApiResponse;
import com.entranet.housemate.housemateapp.api.HomeApi;
import com.entranet.housemate.housemateapp.api.action.ActionsData;
import com.entranet.housemate.housemateapp.api.action.CreateActionBody;
import com.entranet.housemate.housemateapp.api.action.DeleteActionBody;
import com.entranet.housemate.housemateapp.api.action.GetActionsBody;
import com.entranet.housemate.housemateapp.api.action.GetActionsResponse;
import com.entranet.housemate.housemateapp.api.action.UpdateActionBody;
import com.entranet.housemate.housemateapp.api.camera.GetVideoFeedBody;
import com.entranet.housemate.housemateapp.api.camera.GetVideoFeedResponse;
import com.entranet.housemate.housemateapp.data.AppExecutors;
import com.entranet.housemate.housemateapp.data.db.dao.ActionDao;
import com.entranet.housemate.housemateapp.data.db.entity.ActionEntity;
import com.entranet.housemate.housemateapp.data.db.entity.DeviceEntity;
import com.entranet.housemate.housemateapp.data.db.entity.SceneEntity;
import com.entranet.housemate.housemateapp.data.helpers.Constants;
import com.entranet.housemate.housemateapp.data.helpers.ListMenu;
import com.entranet.housemate.housemateapp.ui.main.actions.callbacks.ActionEditorCallback;
import com.entranet.housemate.housemateapp.ui.main.actions.callbacks.ActionRefreshCallback;
import com.entranet.housemate.housemateapp.ui.main.smarthome.camera.GetVideoUrlCallback;

import java.io.IOException;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import retrofit2.Response;

/**
 * Repository class regarding the ActionEntity operations.
 */

public class ActionRepository {

    private static final String TAG = ActionRepository.class.getSimpleName();

    private final HomeApi homeApi;
    private final ActionDao actionDao;
    private final AppExecutors executors;

    public ActionRepository(HomeApi homeApi, ActionDao actionDao, AppExecutors executors) {
        this.homeApi = homeApi;
        this.actionDao = actionDao;
        this.executors = executors;
    }

    public void add(List<ActionEntity> actions, String buildingID) {
        executors.getDiskIO().execute(() -> {
            // Set the building ID and save.
            for (ActionEntity action: actions) action.setBuildingId(buildingID);
            actionDao.insertActions(actions);
        });
    }

    public List<ActionEntity> load(String type, String buildingId) {
        return actionDao.getActionsByType(type, buildingId);
    }

    public void delete(String type, String actionId, String currentBuildingId, String token, ActionRefreshCallback callback) {
        executors.getDiskIO().execute(() -> {
            // Create request body.
            DeleteActionBody body = new DeleteActionBody(actionId);
            body.setToken(token);
            body.setBuildingID(currentBuildingId);
            try {
                Response<ApiResponse> response = homeApi.deleteAction(body).execute();
                if (response.isSuccessful()) {
                    ApiResponse data = response.body();
                    if (data != null) {
                        if (data.getState() == 0) {
                            if (BuildConfig.DEBUG) Log.d(TAG, "Delete action request successful.");
                            // Now delete locally.
                            actionDao.deleteActionById(actionId);
                            // Return the new list.
                            callback.onActionsRefreshed(actionDao.getActionsByType(type, currentBuildingId));
                        } else {
                            Log.e(TAG, "Request failed. Error description: " + data.getDescription());
                        }
                    } else {
                        Log.e(TAG, "Response body null.");
                    }
                } else {
                    Log.e(TAG, "Delete action request unsuccessful.");
                }
            } catch (IOException e) {
                Log.e(TAG, "Exception raised: "  + e.getMessage());
            }
        });
    }

    public void update(ActionEntity action, String mode , String currentBuildingId, String token, ActionEditorCallback callback) {
        executors.getDiskIO().execute(() -> {
            Response<ApiResponse> response;
            try {
                switch (mode) {
                    case Constants.MODE_CREATE: {
                        // Create body request.
                        CreateActionBody body = new CreateActionBody(action.getType(), action.getText(), action.getTime());
                        body.setDay(action.getDay());
                        body.setDevices(action.getDevices());
                        body.setRepeated(action.isRepeated());
                        body.setEnabled(action.isEnabled());
                        body.setToken(token);
                        body.setBuildingID(currentBuildingId);
                        response = homeApi.createAction(body).execute();
                        break;
                    }
                    case Constants.MODE_EDIT_ENABLED:
                        if (!action.isRepeated() && action.isEnabled()) {
                            rebaseDateForAction(action);

                        }
                        // Deliberately let the execution flow continue.
                        // (Pretty sure this is not found on best practices)
                    case Constants.MODE_EDIT:
                        UpdateActionBody body = new UpdateActionBody(action.getType(), action.getText(), action.getTime());
                        body.setDay(action.getDay());
                        body.setDevices(action.getDevices());
                        body.setRepeated(action.isRepeated());
                        body.setEnabled(action.isEnabled());
                        body.setToken(token);
                        body.setBuildingID(currentBuildingId);
                        // Set the action ID to update.
                        body.setActionId(action.getActionId());
                        response = homeApi.updateAction(body).execute();
                        break;
                    default:
                        return;
                }

                if (response.isSuccessful()) {
                    ApiResponse data = response.body();
                    if (data != null) {
                        if (data.getState() == 0) {
                            Log.d(TAG, "Create action request successful.");
                            if (callback != null) callback.onActionCreated();
                            return;
                        } else {
                            Log.e(TAG, "Request failed. Error description: " + data.getDescription());
                        }
                    } else {
                        Log.e(TAG, "Response body null.");
                    }
                } else {
                    Log.e(TAG, "Create actions request unsuccessful.");
                }
                if (callback != null) callback.onActionCreationFailed(R.string.action_editor_creation_error);
            } catch (IOException e) {
                Log.e(TAG, "Exception raised: "  + e.getMessage());
            } catch (ParseException e) {
                Log.e(TAG, "ParseException raised: "  + e.getMessage());
            }
        });
    }

    private void rebaseDateForAction(ActionEntity action) throws ParseException {
        // Get the current date.
        Calendar currentCal = Calendar.getInstance();
        DateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy HH:mm", Locale.getDefault());
        Date currentDate = currentCal.getTime();
        // Action's date
        Calendar actionCal = Calendar.getInstance();
        Date actionDate = dateFormat.parse(action.getDate() + " " + action.getTime());
        actionCal.setTime(actionDate);
        // Compare it with the action's date.
        if (currentDate.after(actionDate)) {
            // We have to check time too. If the time has passed set it for tomorrow.
            DateFormat dateOnlyFormat = new SimpleDateFormat("dd/MM/yyyy", Locale.getDefault());
            SimpleDateFormat timeFormat = new SimpleDateFormat("HH:mm", Locale.getDefault());
            // Compare time.
            Date actionTime = timeFormat.parse(action.getTime());
            Date currentTime = timeFormat.parse(timeFormat.format(currentCal.getTime()));
            // Change only the time.
            currentCal.set(Calendar.HOUR_OF_DAY, actionCal.get(Calendar.HOUR_OF_DAY));
            currentCal.set(Calendar.MINUTE, actionCal.get(Calendar.MINUTE));
            if (currentTime.after(actionTime)) {
                currentCal.add(Calendar.DATE, 1);
            }
            action.setDate(dateOnlyFormat.format(currentCal.getTime()));
        }
    }

    public void refresh(String type, String buildingId, String token, ActionRefreshCallback callback) {
        executors.getDiskIO().execute(() -> {
            // Create the body request.
            GetActionsBody body = new GetActionsBody(type);
            body.setBuildingID(buildingId);
            body.setToken(token);

            try {
                Response<GetActionsResponse> response = homeApi.getActions(body).execute();
                if (response.isSuccessful()) {
                    GetActionsResponse data = response.body();
                    if (data != null) {
                        if (data.getState() == Constants.API_RESPONSE_STATE_SUCCESSFUL) {
                            ActionsData actionsData = data.getData();
                            List<ActionEntity> actions = actionsData.getActions();
                            for (ActionEntity action: actions) action.setBuildingId(buildingId);
                            actionDao.insertActions(actions);
                            if (BuildConfig.DEBUG) Log.d(TAG, "Get actions request successful.");
                            // Query again to avoid clearing the duplicates.
                            callback.onActionsRefreshed(actionDao.getActionsByType(type, buildingId));
                        } else {
                            Log.e(TAG, "Request failed. Error description: " + data.getDescription());
                        }
                    } else {
                        Log.e(TAG, "Response body null.");
                    }
                } else {
                    Log.e(TAG, "Get actions request unsuccessful.");
                }
            } catch (IOException e) {
                Log.e(TAG, "Exception raised: "  + e.getMessage());
            }

        });
    }

    public void getDevicesForAction(List<DeviceEntity> devices,
                                    String mode,
                                    String actionId,
                                    boolean isInitial,
                                    ActionEditorCallback callback) {

        // Modify list if necessary.
        ActionEntity action = actionDao.getActionById(actionId);
        List<String> deviceIds = new ArrayList<>();
        if (Constants.MODE_EDIT.equals(mode)) {
            if (action == null) {
                Log.e(TAG, "No such action exists");
                return;
            }
            deviceIds.addAll(action.getDevices());
        }
        List<ListMenu.DeviceItem> deviceItems = new ArrayList<>();
        for (DeviceEntity device: devices) {
            deviceItems.add(new ListMenu.DeviceItem(device, deviceIds.contains(device.getId())));
        }
        // Default to selected if there is only one device in CREATE mode.
        if (Constants.MODE_CREATE.equals(mode) && devices.size() == 1) {
            deviceItems.get(0).setSelected(true);
        }
        // Route to caller.
        callback.onDeviceLoaded(deviceItems, isInitial, action);
    }

    public void getScenesForAction(List<SceneEntity> scenes,
                                   String mode,
                                   String actionId,
                                   boolean isInitial,
                                   ActionEditorCallback callback) {
        List<ListMenu.SceneItem> sceneItems = new ArrayList<>();
        ActionEntity action = null;
        if (mode.equals(Constants.MODE_EDIT)) {
            action = actionDao.getActionById(actionId);
            for (SceneEntity scene : scenes) {
                sceneItems.add(new ListMenu.SceneItem(scene, scene.getName().equals(action.getText())));
            }
        } else {
            for (SceneEntity scene : scenes) {
                sceneItems.add(new ListMenu.SceneItem(scene, false));
            }
            // If there is only one scene make it default.
            if (scenes.size() == 1) sceneItems.get(0).setSelected(true);
        }
        // Route to caller.
        callback.onScenesLoaded(sceneItems, isInitial, action);
    }

    public void getVideoFeed(String deviceId, boolean isCall, String buildingId, String token, GetVideoUrlCallback callback) {
        executors.getDiskIO().execute(() -> {
            GetVideoFeedBody body = new GetVideoFeedBody(deviceId);
            body.setBuildingId(buildingId);
            body.setToken(token);
            Response<GetVideoFeedResponse> response;
            try {
                if (isCall) {
                    response = homeApi.getVideoCall(body).execute();
                } else {
                    response = homeApi.getVideoFeed(body).execute();
                }
                if (response.isSuccessful()) {
                    GetVideoFeedResponse data = response.body();
                    if (data != null) {
                        if (data.getState() == Constants.API_RESPONSE_STATE_SUCCESSFUL) {
                            if (BuildConfig.DEBUG) Log.d(TAG, "Video feed request successful: " + response.toString());
                            // Extract the url.
                            String url = data.getUrl();
                            // Return the url to the caller.
                            callback.onVideoUrlReady(url);
                            return;
                        } else {
                            Log.e(TAG, "Request failed. Error description: " + data.getDescription());
                        }
                    } else {
                        Log.e(TAG, "Response body null.");
                    }
                } else {
                    Log.e(TAG, "Video feed request unsuccessful.");
                }
                callback.onVideoUrlUnavailable();
            } catch (IOException e) {
                Log.e(TAG, "Exception raised: "  + e.getMessage());
            }
        });
    }

    public void truncate() { executors.getDiskIO().execute(actionDao::deleteAll);    }

}
