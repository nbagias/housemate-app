package com.entranet.housemate.housemateapp.api.action;

import com.entranet.housemate.housemateapp.api.HomeApi;
import com.entranet.housemate.housemateapp.data.db.entity.Day;
import com.entranet.housemate.housemateapp.data.helpers.Constants;
import com.squareup.moshi.Json;

import java.util.List;

/**
 * API body request for {@link HomeApi#createAction}.
 */

public class CreateActionBody {
    @Json(name = "token")
    private String token;
    @Json(name = "buildingID")
    private String buildingID;
    @Json(name = "origin")
    private final int ORIGIN = Constants.ORIGIN_HOUSEMATE_APP;
    @Json(name = "day")
    private Day day;
    @Json(name = "type")
    private String type;
    @Json(name = "text")
    private String text;
    @Json(name = "time")
    private String time;
    @Json(name = "repeat")
    private boolean isRepeated;
    @Json(name = "enabled")
    private boolean isEnabled;
    @Json(name = "devices")
    private List<String> devices;

    public CreateActionBody(String type, String text, String time) {
        this.type = type;
        this.text = text;
        this.time = time;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public void setBuildingID(String buildingID) {
        this.buildingID = buildingID;
    }

    public Day getDay() {
        return day;
    }

    public void setDay(Day day) {
        this.day = day;
    }

    public boolean isRepeated() {
        return isRepeated;
    }

    public void setRepeated(boolean repeated) {
        this.isRepeated = repeated;
    }

    public List<String> getDevices() {
        return devices;
    }

    public void setDevices(List<String> devices) {
        this.devices = devices;
    }

    public void setEnabled(boolean enabled) { isEnabled = enabled; }
}
