package com.entranet.housemate.housemateapp.data.db.entity;

import android.arch.persistence.room.ColumnInfo;
import android.arch.persistence.room.Entity;
import android.arch.persistence.room.ForeignKey;
import android.arch.persistence.room.Index;
import android.arch.persistence.room.PrimaryKey;
import android.support.annotation.NonNull;

import com.squareup.moshi.Json;

/**
 * Entity class for storing all the switches.
 *
 * Used by Moshi too.
 */
@Entity(tableName = "switches",
        indices = {@Index(value = "building_id")},
        foreignKeys = @ForeignKey(entity = BuildingEntity.class,
                parentColumns = "id",
                childColumns = "building_id",
                onDelete = ForeignKey.CASCADE))
public class SwitchEntity {
    @NonNull
    @PrimaryKey
    @ColumnInfo(name = "switch_id")
    @Json(name = "switchID")
    private String switchId;
    @ColumnInfo(name = "building_id")
    private String buildingId;
    private String name;
    private String type;
    private String state;
    private String extra;

    // Accessors
    @NonNull
    public String getSwitchId() { return switchId; }

    public void setSwitchId(@NonNull String switchId) { this.switchId = switchId; }

    public String getName() { return name; }

    public void setName(String name) { this.name = name; }

    public String getType() { return type; }

    public void setType(String type) { this.type = type; }

    public String getState() { return state; }

    public void setState(String state) { this.state = state; }

    public String getExtra() { return extra; }

    public void setExtra(String extra) { this.extra = extra; }

    public String getBuildingId() { return buildingId; }

    public void setBuildingId(String buildingId) { this.buildingId = buildingId; }
}
