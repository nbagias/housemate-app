package com.entranet.housemate.housemateapp.api.login;

import com.squareup.moshi.Json;

/**
 * Model of the API response for the login operation.
 */

public class LoginResponse {
    @Json(name = "state")
    private Integer state;
    @Json(name = "description")
    private String description;
    @Json(name = "data")
    private LoginData data;

    public LoginData getData() { return data; }

    public void setData(LoginData data) { this.data = data; }

    public String getDescription() { return description; }

    public void setDescription(String description) { this.description = description; }

    public Integer getState() { return state; }

    public void setState(Integer state) { this.state = state; }

    public String getToken() { return data.getToken(); }
}
