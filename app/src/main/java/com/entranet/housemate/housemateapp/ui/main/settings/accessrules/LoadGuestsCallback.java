package com.entranet.housemate.housemateapp.ui.main.settings.accessrules;

import com.entranet.housemate.housemateapp.data.db.entity.GuestEntity;

import java.util.List;

/**
 * Interface for routing action back to caller.
 */

public interface LoadGuestsCallback {

    void onGuestsLoaded(List<GuestEntity> guests);
}
