package com.entranet.housemate.housemateapp.data.db.entity;

import android.arch.persistence.room.ColumnInfo;
import android.arch.persistence.room.Embedded;
import android.arch.persistence.room.Entity;
import android.arch.persistence.room.PrimaryKey;
import android.support.annotation.NonNull;

import com.entranet.housemate.housemateapp.api.building.BuildingPermissions;

/**
 * Entity class for storing guests information.
 */

@Entity(tableName = "guests")
public class GuestEntity {
    @NonNull
    @PrimaryKey
    private String email;
    @ColumnInfo(name = "building_id")
    private String buildingId;
    @Embedded
    private BuildingPermissions permissions;

    // Accessors
    public String getBuildingId() { return buildingId; }

    public void setBuildingId(String buildingId) { this.buildingId = buildingId; }

    @NonNull
    public String getEmail() { return email; }

    public void setEmail(@NonNull String email) { this.email = email; }

    public BuildingPermissions getPermissions() { return permissions; }

    public void setPermissions(BuildingPermissions permissions) { this.permissions = permissions; }

    @Override
    public String toString() { return this.getEmail(); }
}
