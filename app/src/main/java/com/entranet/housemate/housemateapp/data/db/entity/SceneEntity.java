package com.entranet.housemate.housemateapp.data.db.entity;

import android.arch.persistence.room.ColumnInfo;
import android.arch.persistence.room.Entity;
import android.arch.persistence.room.ForeignKey;
import android.arch.persistence.room.Index;
import android.arch.persistence.room.PrimaryKey;
import android.support.annotation.NonNull;

import com.entranet.housemate.housemateapp.data.db.converters.SceneConverter;
import com.squareup.moshi.Json;

import java.util.List;

/**
 * Entity class for storing all the scenes.
 *
 * Used by Moshi too.
 *
 * Using {@link SceneConverter}.
 */

@Entity(tableName = "scenes",
        indices = {@Index(value = "building_id")},
        foreignKeys = @ForeignKey(entity = BuildingEntity.class,
            parentColumns = "id",
            childColumns = "building_id",
            onDelete = ForeignKey.CASCADE))
public class SceneEntity {
    @NonNull
    @PrimaryKey
    private String name;
    @ColumnInfo(name = "building_id")
    private String buildingId;
    @Json(name = "switches")
    private List<SwitchEntity> switches;

    // Accessors
    @NonNull
    public String getName() { return name; }

    public void setName(@NonNull String name) { this.name = name; }

    public void setSwitches(List<SwitchEntity> switches) { this.switches = switches; }

    public List<SwitchEntity> getSwitches() { return switches; }

    public String getBuildingId() { return buildingId; }

    public void setBuildingId(String buildingId) { this.buildingId = buildingId; }
}
