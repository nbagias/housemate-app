package com.entranet.housemate.housemateapp.api.login;

import com.squareup.moshi.Json;

public class LoginData {

    @Json(name = "token")
    private String token;

    String getToken() { return token; }

    public void setToken(String token) { this.token = token; }
}
