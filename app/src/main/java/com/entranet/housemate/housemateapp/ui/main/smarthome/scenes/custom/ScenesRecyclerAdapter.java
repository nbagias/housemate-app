package com.entranet.housemate.housemateapp.ui.main.smarthome.scenes.custom;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.entranet.housemate.housemateapp.R;

import java.util.List;

/**
 *  Recycler adapter for populating scenes.
 */

public class ScenesRecyclerAdapter extends RecyclerView.Adapter<ScenesRecyclerAdapter.SceneGroupViewHolder> {

    public static final int EMPTY_VIEW = 10;
    private final List<String> scenes;
    private final SceneItemListener callback;

    public ScenesRecyclerAdapter(List<String> scenes, SceneItemListener callback) {
        this.scenes = scenes;
        this.callback = callback;
    }

    @NonNull
    @Override
    public SceneGroupViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        if (viewType == EMPTY_VIEW) {
            return new EmptyViewHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.menuitem_empty, parent, false));
        } else {
            return new SceneGroupViewHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.menuitem_scenes, parent, false));
        }
    }

    @Override
    public void onBindViewHolder(@NonNull SceneGroupViewHolder holder, int position) {
        if (!(holder instanceof EmptyViewHolder)) {
            final String sceneName = scenes.get(position);
            holder.tvSceneName.setText(sceneName);
            holder.progressBar.setVisibility(View.INVISIBLE);
        }
    }

    @Override
    public int getItemCount() {
        if (scenes.isEmpty()){
            return 1;
        } else {
            return scenes.size();
        }
    }

    @Override
    public int getItemViewType(int position) {
        if (scenes.isEmpty()) {
            return EMPTY_VIEW;
        }
        return super.getItemViewType(position);
    }

    class EmptyViewHolder extends ScenesRecyclerAdapter.SceneGroupViewHolder {
        EmptyViewHolder(View itemView) {
            super(itemView);
        }
    }

    public void updateLoadingState(String name) {
        for (int i = 0; i < scenes.size(); i++) {
            if (scenes.get(i).equals(name)) {
                notifyItemChanged(i);
                break;
            }
        }
    }

    class SceneGroupViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener,
            View.OnLongClickListener {

        final TextView tvSceneName;
        final ProgressBar progressBar;

        SceneGroupViewHolder(View itemView) {
            super(itemView);
            tvSceneName = itemView.findViewById(R.id.menu_item_scene_name);
            progressBar = itemView.findViewById(R.id.menu_item_scene_loading);
            if (tvSceneName == null) return;
            tvSceneName.setOnClickListener(this);
            tvSceneName.setOnLongClickListener(this);
        }

        @Override
        public void onClick(View v) {
            progressBar.setVisibility(View.VISIBLE);
            callback.onSceneClicked(tvSceneName.getText().toString());
        }

        @Override
        public boolean onLongClick(View v) {
            callback.onSceneLongClicked(v, tvSceneName.getText().toString());
            return true;
        }
    }

    public interface SceneItemListener {
        void onSceneClicked(String sceneName);

        void onSceneLongClicked(View view, String sceneName);
    }

}
