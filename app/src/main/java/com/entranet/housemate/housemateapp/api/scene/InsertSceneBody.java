package com.entranet.housemate.housemateapp.api.scene;

import com.entranet.housemate.housemateapp.api.HomeApi;
import com.entranet.housemate.housemateapp.data.helpers.Constants;
import com.squareup.moshi.Json;

/**
 * API body request for {@link HomeApi#insertScene}.
 */

public class InsertSceneBody {
    @Json(name = "token")
    private String token;
    @Json(name = "buildingID")
    private String buildingID;
    @Json(name = "name")
    private String name;
    @Json(name = "switchID")
    private String switchID;
    @Json(name = "state")
    private String state;
    @Json(name = "extra")
    private String extra;
    @Json(name = "origin")
    private final int ORIGIN = Constants.ORIGIN_HOUSEMATE_APP;

    public InsertSceneBody(String name, String switchID, String state, String extra) {
        this.name = name;
        this.switchID = switchID;
        this.state = state;
        this.extra = extra;
    }

    public void setToken(String token) { this.token = token; }

    public void setBuildingID(String buildingID) { this.buildingID = buildingID; }
}
