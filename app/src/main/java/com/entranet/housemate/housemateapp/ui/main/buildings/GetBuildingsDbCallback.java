package com.entranet.housemate.housemateapp.ui.main.buildings;

import com.entranet.housemate.housemateapp.data.db.entity.BuildingEntity;

import java.util.List;

/**
 * Interface for routing actions after we fetch the buildings list from the {@link com.entranet.housemate.housemateapp.data.db.AppDatabase}.
 */

public interface GetBuildingsDbCallback {

    void onBuildingsExist(List<BuildingEntity> buildings);

    void onBuildingsNone();
}
