package com.entranet.housemate.housemateapp.api.action;

import com.entranet.housemate.housemateapp.api.HomeApi;
import com.entranet.housemate.housemateapp.data.helpers.Constants;
import com.squareup.moshi.Json;

/**
 * API request body for {@link HomeApi#deleteAction}.
 */

public class DeleteActionBody {
    @Json(name = "token")
    private String token;
    @Json(name = "buildingID")
    private String buildingID;
    @Json(name = "origin")
    private final int ORIGIN = Constants.ORIGIN_HOUSEMATE_APP;
    @Json(name = "actionID")
    private String actionId;

    public DeleteActionBody(String actionId) { this.actionId = actionId; }

    public void setToken(String token) { this.token = token; }

    public void setBuildingID(String buildingID) { this.buildingID = buildingID; }
}
