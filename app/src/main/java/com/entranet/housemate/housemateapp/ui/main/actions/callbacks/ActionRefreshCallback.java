package com.entranet.housemate.housemateapp.ui.main.actions.callbacks;

import com.entranet.housemate.housemateapp.data.db.entity.ActionEntity;

import java.util.List;

/**
 * Interface for routing action to the caller.
 */

public interface ActionRefreshCallback {
    void onActionsRefreshed(List<ActionEntity> actions);
}
