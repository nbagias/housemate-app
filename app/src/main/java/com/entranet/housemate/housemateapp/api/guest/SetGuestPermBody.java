package com.entranet.housemate.housemateapp.api.guest;

import com.entranet.housemate.housemateapp.api.AdminApi;
import com.entranet.housemate.housemateapp.api.building.BuildingPermissions;
import com.entranet.housemate.housemateapp.data.helpers.Constants;
import com.squareup.moshi.Json;

/**
 * API request body for {@link AdminApi#setPermissionForGuest}.
 */

public class SetGuestPermBody {
    @Json(name = "token")
    private String token;
    @Json(name = "buildingID")
    private String buildingID;
    @Json(name = "origin")
    private final int ORIGIN = Constants.ORIGIN_HOUSEMATE_APP;
    @Json(name = "email")
    private String email;
    @Json(name = "permissions")
    private BuildingPermissions permissions;

    public SetGuestPermBody(String email, BuildingPermissions permissions) {
        this.email = email;
        this.permissions = permissions;
    }

    public String getToken() { return token; }

    public void setToken(String token) { this.token = token; }

    public String getBuildingID() { return buildingID; }

    public void setBuildingID(String buildingID) { this.buildingID = buildingID; }

}
