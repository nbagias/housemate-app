package com.entranet.housemate.housemateapp.ui.main.settings.location;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;

import com.entranet.housemate.housemateapp.R;
import com.entranet.housemate.housemateapp.data.DataRepository;
import com.entranet.housemate.housemateapp.data.helpers.Constants;
import com.entranet.housemate.housemateapp.data.helpers.ListMenu;
import com.google.android.gms.common.GooglePlayServicesNotAvailableException;
import com.google.android.gms.common.GooglePlayServicesRepairableException;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.location.places.AutocompleteFilter;
import com.google.android.gms.location.places.Place;
import com.google.android.gms.location.places.ui.PlaceAutocomplete;

import java.util.List;

import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

public class LocationActivity extends AppCompatActivity implements
        LocationSettingsAdapter.LocationSettingsListener,
        TimezoneFragment.OnTimezoneSelectionListener,
        LocationSettingsCallback {

    private static final String TAG = LocationActivity.class.getSimpleName();

    private DataRepository repository;
    private RecyclerView recyclerView;
    private final List<ListMenu.SettingsItem> settingsItems = ListMenu.getLocationSettingsList();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_location);

        Toolbar toolbar = findViewById(R.id.toolbar);
        toolbar.setTitle("");
        setSupportActionBar(toolbar);
        ActionBar actionBar = getSupportActionBar();
        if (actionBar != null) actionBar.setDisplayHomeAsUpEnabled(true);

        recyclerView = findViewById(R.id.location_settings_list);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        recyclerView.setHasFixedSize(true);
        recyclerView.addItemDecoration(new DividerItemDecoration(this, DividerItemDecoration.VERTICAL));

        repository = DataRepository.getInstance(getApplicationContext());
        repository.loadLocationSettings(this);

    }

    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }

    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == 1) {
            if (resultCode == RESULT_OK) {
                Place place = PlaceAutocomplete.getPlace(this, data);
                Log.i(TAG, "Place: " + place.getName());
                // Update UI.
                LocationSettingsAdapter adapter = (LocationSettingsAdapter) recyclerView.getAdapter();
                adapter.updateAdapterList(place.getName().toString(), Constants.LOCATION_WEATHER);
                // Update database.
                repository.setLocation(place.getName().toString());
            } else if (resultCode == PlaceAutocomplete.RESULT_ERROR) {
                Status status = PlaceAutocomplete.getStatus(this, data);
                Log.i(TAG, "Location search error: " + status.getStatusMessage());

            } else if (resultCode == RESULT_CANCELED) {
                // The user canceled the operation.
                Log.i(TAG, "Location search cancelled");

            }
        }
    }

    @Override
    public void onLocationSettingSelected(ListMenu.SettingsItem item) {
        switch (item.getId()) {
            case Constants.LOCATION_WEATHER:
                showSearchDialog();
                break;
            case Constants.LOCATION_TIMEZONE:
                showTimezoneDialog();
                break;
            default:
                break;
        }
    }

    @Override
    public void onSetDefaultLocation(int location) {
        ListMenu.SettingsItem weatherLocation = settingsItems.get(Constants.LOCATION_WEATHER);
        weatherLocation.setValue(getString(location));
    }

    @Override
    public void onSetDefaultTimezone(int timezone) {
        ListMenu.SettingsItem defaultTimezone = settingsItems.get(Constants.LOCATION_TIMEZONE);
        defaultTimezone.setValue(getString(timezone));
    }

    @Override
    public void onSetCurrentLocation(String location) {
        ListMenu.SettingsItem weatherLocation = settingsItems.get(Constants.LOCATION_WEATHER);
        weatherLocation.setValue(location);
    }

    @Override
    public void onSetCurrentTimezone(String timezone) {
        ListMenu.SettingsItem defaultTimezone = settingsItems.get(Constants.LOCATION_TIMEZONE);
        defaultTimezone.setValue(timezone);
    }

    @Override
    public void onLoadSettingsCompleted() {
        // Update UI.
        runOnUiThread(() -> recyclerView.setAdapter(new LocationSettingsAdapter(settingsItems, LocationActivity.this)));
    }

    @Override
    public void onTimezoneSelected(String selectedTimezone) {
        // Update adapter.
        LocationSettingsAdapter adapter = (LocationSettingsAdapter) recyclerView.getAdapter();
        adapter.updateAdapterList(selectedTimezone, Constants.LOCATION_TIMEZONE);
        // Update DB/API.
        repository.setTimezone(selectedTimezone);
    }

    private void showSearchDialog() {
        try {
            AutocompleteFilter typeFilter = new AutocompleteFilter.Builder()
                    .setTypeFilter(AutocompleteFilter.TYPE_FILTER_CITIES)
                    .build();
            Intent intent = new PlaceAutocomplete.IntentBuilder(PlaceAutocomplete.MODE_OVERLAY)
                    .setFilter(typeFilter)
                    .build(LocationActivity.this);
            startActivityForResult(intent, 1);
        } catch (GooglePlayServicesRepairableException | GooglePlayServicesNotAvailableException e) {
            // TODO: Handle the error.
        }
    }

    private void showTimezoneDialog() {
        FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
        DialogFragment timezoneFragment = TimezoneFragment.newInstance();
        timezoneFragment.show(ft, TimezoneFragment.class.getSimpleName());
    }
}