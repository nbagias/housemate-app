package com.entranet.housemate.housemateapp.api.building;

import com.squareup.moshi.Json;

import java.util.List;

public class BuildingData {

    @Json(name = "buildings")
    private List<Building> buildings = null;

    public List<Building> getBuildings() { return buildings; }

    public void setBuildings(List<Building> buildings) { this.buildings = buildings; }
}
