package com.entranet.housemate.housemateapp.api.switches;

import com.entranet.housemate.housemateapp.api.HomeApi;
import com.entranet.housemate.housemateapp.data.helpers.Constants;
import com.squareup.moshi.Json;

/**
 * API request body for {@link HomeApi#updateSwitchName}.
 */

public class UpdateSwitchNameBody {
    @Json(name = "token")
    private String token;
    @Json(name = "buildingID")
    private String buildingID;
    @Json(name = "origin")
    private final int ORIGIN = Constants.ORIGIN_HOUSEMATE_APP;
    @Json(name = "switchID")
    private String switchID;
    @Json(name = "name")
    private String name;

    public UpdateSwitchNameBody(String switchID, String name) {
        this.switchID = switchID;
        this.name = name;
    }

    public void setToken(String token) { this.token = token; }

    public void setBuildingID(String buildingID) { this.buildingID = buildingID; }
}
