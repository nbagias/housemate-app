package com.entranet.housemate.housemateapp.data.db.converters;

import android.arch.persistence.room.TypeConverter;
import android.util.Log;

import com.entranet.housemate.housemateapp.data.db.entity.SwitchEntity;
import com.squareup.moshi.JsonAdapter;
import com.squareup.moshi.Moshi;
import com.squareup.moshi.Types;

import java.io.IOException;
import java.lang.reflect.Type;
import java.util.Collections;
import java.util.List;

/**
 * Type converter class for storing the list of switches for each scene.
 */

public class SceneConverter {

    private static final String TAG = SceneConverter.class.getSimpleName();

    @TypeConverter
    public static List<SwitchEntity> fromString(String data) {
        if (data != null) {
            try {
                Moshi moshi = new Moshi.Builder().build();
                Type listData = Types.newParameterizedType(List.class, SwitchEntity.class);
                JsonAdapter<List<SwitchEntity>> jsonAdapter = moshi.adapter(listData);
                return jsonAdapter.fromJson(data);
            } catch (IOException e) {
                Log.e(TAG, "Could not convert json to list switch entities.");
            }
        }
        return Collections.emptyList();
    }

    @TypeConverter
    public static String fromArrayList(List<SwitchEntity> list) {
        Moshi moshi = new Moshi.Builder().build();
        Type listData = Types.newParameterizedType(List.class, SwitchEntity.class);
        JsonAdapter<List<SwitchEntity>> jsonAdapter = moshi.adapter(listData);
        return jsonAdapter.toJson(list);
    }
}
