package com.entranet.housemate.housemateapp.ui.main.custom;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

import com.entranet.housemate.housemateapp.ui.main.actions.ActionsFragment;
import com.entranet.housemate.housemateapp.ui.main.settings.SettingsFragment;
import com.entranet.housemate.housemateapp.ui.main.smarthome.SmartHomeFragment;

/**
 * Pager adapter for populating viewpager with our three fragments.
 */

public class MainMenuPagerAdapter extends FragmentPagerAdapter {

    public MainMenuPagerAdapter(FragmentManager fm) {
        super(fm);
    }

    @Override
    public Fragment getItem(int position) {
        switch (position) {
            case 0:
                return SmartHomeFragment.newInstance(position);
            case 1:
                return ActionsFragment.newInstance(position);
            case 2:
                return SettingsFragment.newInstance(position);
            default:
                break;
        }
        return SmartHomeFragment.newInstance(position);
    }

    @Override
    public int getCount() {
        // We want three tabs.
        return 3;
    }
}
