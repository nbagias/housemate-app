package com.entranet.housemate.housemateapp.api.switches;

import com.entranet.housemate.housemateapp.api.HomeApi;
import com.entranet.housemate.housemateapp.data.helpers.Constants;
import com.squareup.moshi.Json;

/**
 *  API body request for {@link HomeApi#updateSwitchState}.
 */

public class UpdateSwitchStateBody {
    @Json(name = "token")
    private String token;
    @Json(name = "buildingID")
    private String buildingID;
    @Json(name = "origin")
    private final int ORIGIN = Constants.ORIGIN_HOUSEMATE_APP;
    @Json(name = "switchID")
    private String switchID;
    @Json(name = "state")
    private String state;
    @Json(name = "extra")
    private String extra;

    public UpdateSwitchStateBody(String switchID, String state, String extra) {
        this.switchID = switchID;
        this.state = state;
        this.extra = extra;
    }

    public void setBuildingID(String buildingID) { this.buildingID = buildingID; }

    public void setToken(String token) { this.token = token; }
}
