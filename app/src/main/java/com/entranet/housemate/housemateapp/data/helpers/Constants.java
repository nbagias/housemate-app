package com.entranet.housemate.housemateapp.data.helpers;

import android.support.annotation.IntDef;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

/**
 * Class for storing global constants.
 */

public class Constants {

    public static final String EXTRA_STREAMING_URL = "streamingUrl";

    private Constants() {}

    public static final String HOUSEMATE_HOSTNAME = "api547642.entranet.us";
    public static final String HOUSEMATE_SERVER = "https://" + HOUSEMATE_HOSTNAME;

    public static final int THERMOSTAT_MIN_VALUE = 0;
    public static final int THERMOSTAT_MAX_VALUE = 30;

    public static final String SWITCH_STATE_ON = "on";
    public static final String SWITCH_STATE_OFF = "off";

    public static final String LIST_ITEM_SELECTED = "";
    public static final String LIST_ITEM_UNSELECTED = "no";

    public static final String ACTION_MODE = "actionMode";
    public static final String ACTION_TYPE = "actionType";
    public static final String ACTION_ID = "actionId";

    public static final String ACTION_TYPE_ALARM = "alarm";
    public static final String ACTION_TYPE_ALERT = "alert";
    public static final String ACTION_TYPE_SCENARIO = "scene";

    public static final String MODE_EDIT = "editMode";
    public static final String MODE_CREATE = "createMode";
    public static final String MODE_EDIT_ENABLED = "editEnabledMode";

    public static final int REMOVE_ALL_SCENES = 0;
    public static final int REMOVE_SCENE = 1;

    public static final int EDIT_ACTION_REQUEST = 1;
    public static final int CREATE_ACTION_REQUEST = 2;

    public static final int LOCATION_WEATHER = 0;
    public static final int LOCATION_TIMEZONE = 1;

    public static final int API_RESPONSE_STATE_SUCCESSFUL = 0;
    public static final int USER_STATUS_ACTIVE = 1;
    public static final int ORIGIN_HOUSEMATE_APP = 1;

    // Main menu enumeration.
    // TODO Replace with annotation types
    public static final int SWITCHES = 1;
    public static final int SCENES = 2;
    public static final int CAMERA = 3;
    public static final int ALARM = 4;
    public static final int ALERT = 5;
    public static final int SCENARIOS = 6;
    public static final int ACCESS_RULES = 7;
    public static final int LOCATION = 8;
    public static final int NEWS = 9;
    public static final int NEW_DEVICE = 10;
    public static final int LOGOUT = 11;

    @IntDef({SWITCHES, SCENES,CAMERA,ALARM,ALERT,SCENARIOS,ACCESS_RULES, LOCATION, NEWS, NEW_DEVICE, LOGOUT})
    @Retention(RetentionPolicy.SOURCE)
    public @interface MenuItems {}
}
