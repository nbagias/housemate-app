package com.entranet.housemate.housemateapp.ui.main.actions.custom;

import android.app.DatePickerDialog;
import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.DialogFragment;
import android.widget.DatePicker;

import com.entranet.housemate.housemateapp.R;

import java.util.Calendar;

/**
 */
public class DatePickerFragment extends DialogFragment implements DatePickerDialog.OnDateSetListener {

    private DatePickerListener listener;

    public DatePickerFragment() {
        // Required empty public constructor
    }

    @NonNull
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        // Use the current date as the default date in the picker
        final Calendar c = Calendar.getInstance();
        int year = c.get(Calendar.YEAR);
        int month = c.get(Calendar.MONTH);
        int day = c.get(Calendar.DAY_OF_MONTH);
        DatePickerDialog dialog = new DatePickerDialog(getActivity(), R.style.AppTheme_DialogPicker, this, year, month, day);
        // Prevent user from selecting past dates.
        dialog.getDatePicker().setMinDate(System.currentTimeMillis());
        return dialog;
    }

    public void onDateSet(DatePicker view, int year, int month, int day) {
        if (listener != null) {
            listener.onDatePickerResult(year, month, day);
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof DatePickerListener) {
            listener = (DatePickerListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement DatePickerListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        listener = null;
    }

    /**
     */
    public interface DatePickerListener {
        void onDatePickerResult(int year, int month, int day);
    }
}
