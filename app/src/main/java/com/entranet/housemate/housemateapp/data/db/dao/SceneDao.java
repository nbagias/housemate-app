package com.entranet.housemate.housemateapp.data.db.dao;

import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.OnConflictStrategy;
import android.arch.persistence.room.Query;

import com.entranet.housemate.housemateapp.data.db.entity.SceneEntity;

import java.util.List;

/**
 * Dao class for accessing the {@link SceneEntity} table.
 */

@Dao
public interface SceneDao {

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    void insertScene(SceneEntity... scene);

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    void insertAllScenes(List<SceneEntity> sceneEntities);

    @Query("SELECT * FROM scenes WHERE building_id=:buildingId")
    List<SceneEntity> getScenes(String buildingId);

    @Query("SELECT name FROM scenes WHERE building_id=:buildingId")
    List<String> getSceneNames(String buildingId);

    @Query("SELECT * FROM scenes WHERE name=:sceneName AND building_id=:buildingId")
    SceneEntity getSceneByName(String sceneName, String buildingId);

    @Query("DELETE FROM scenes")
    void deleteAll();

    @Query("DELETE FROM scenes WHERE name=:sceneName")
    void deleteSceneByName(String sceneName);
}
