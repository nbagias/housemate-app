package com.entranet.housemate.housemateapp.ui.main.actions;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.widget.Button;

import com.entranet.housemate.housemateapp.R;
import com.entranet.housemate.housemateapp.data.DataRepository;
import com.entranet.housemate.housemateapp.data.db.entity.ActionEntity;
import com.entranet.housemate.housemateapp.data.helpers.Constants;
import com.entranet.housemate.housemateapp.ui.main.actions.adapters.ActionAdapter;
import com.entranet.housemate.housemateapp.ui.main.actions.callbacks.ActionRefreshCallback;

import java.util.List;

import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

public class AlarmActivity extends AppCompatActivity implements
        ActionAdapter.ActionListCallback,
        ActionRefreshCallback {

    private DataRepository repository;
    private SwipeRefreshLayout refreshLayout;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_alarm);

        repository = DataRepository.getInstance(getApplicationContext());

        setupUi();
    }

    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }

    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }

    @Override
    public void onActionItemClicked(ActionEntity action) {
        Intent intent = new Intent(this, ActionEditorActivity.class);
        intent.putExtra(Constants.ACTION_MODE, Constants.MODE_EDIT);
        intent.putExtra(Constants.ACTION_TYPE, action.getType());
        intent.putExtra(Constants.ACTION_ID, action.getActionId());
        startActivityForResult(intent, Constants.EDIT_ACTION_REQUEST);
    }

    @Override
    public void onActionItemLongClicked(final ActionEntity action) {
        AlertDialog dialog = new AlertDialog.Builder(this)
                .setIcon(android.R.drawable.ic_delete)
                .setTitle(R.string.remove_action_dialog_title)
                .setPositiveButton(android.R.string.yes,
                        (dialog1, whichButton) -> repository.deleteAction(action.getType(), action.getActionId(), AlarmActivity.this)
                )
                .setNegativeButton(android.R.string.no,
                        (dialog1, whichButton) -> {
                        }
                ).create();
        dialog.show();
    }

    @Override
    public void onActionCheckedChanged(ActionEntity action, boolean isChecked) {
        action.setEnabled(isChecked);
        // We have to use add action in order to pass a different mode.
        repository.addAction(action, Constants.MODE_EDIT_ENABLED, null);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == Constants.EDIT_ACTION_REQUEST
                || requestCode == Constants.CREATE_ACTION_REQUEST
                || resultCode == RESULT_OK) {

            repository.refreshActions(Constants.ACTION_TYPE_ALARM, this);
        }
    }

    @Override
    public void onActionsRefreshed(List<ActionEntity> actions) {
        runOnUiThread(() -> {
            refreshLayout.setRefreshing(false);
            RecyclerView alarmsList = findViewById(R.id.alarm_recycler);
            alarmsList.setAdapter(new ActionAdapter(actions,this));
        });
    }

    private void setupUi() {
        Toolbar toolbar = findViewById(R.id.toolbar);
        toolbar.setTitle("");
        setSupportActionBar(toolbar);
        ActionBar actionBar = getSupportActionBar();
        if (actionBar != null) actionBar.setDisplayHomeAsUpEnabled(true);

        final RecyclerView alarmsList = findViewById(R.id.alarm_recycler);
        alarmsList.setLayoutManager(new LinearLayoutManager(this));

        new Thread(() -> {
            final List<ActionEntity> alarms = repository.getActions(Constants.ACTION_TYPE_ALARM);
            runOnUiThread(() -> alarmsList.setAdapter(new ActionAdapter(alarms, AlarmActivity.this)));
        }).start();

        Button createAlarm = findViewById(R.id.bottom_button);
        createAlarm.setText(R.string.create_button_title);
        createAlarm.setOnClickListener(v -> {
            Intent intent = new Intent(AlarmActivity.this, ActionEditorActivity.class);
            intent.putExtra(Constants.ACTION_TYPE, Constants.ACTION_TYPE_ALARM);
            intent.putExtra(Constants.ACTION_MODE, Constants.MODE_CREATE);
            startActivityForResult(intent, Constants.CREATE_ACTION_REQUEST);
        });

        refreshLayout = findViewById(R.id.alarm_swipe_container);
        refreshLayout.setOnRefreshListener(refreshListener);
    }

    private SwipeRefreshLayout.OnRefreshListener refreshListener = new SwipeRefreshLayout.OnRefreshListener() {
        @Override
        public void onRefresh() {
            refreshLayout.setRefreshing(true);
            repository.refreshActions(Constants.ACTION_TYPE_ALARM,AlarmActivity.this);
        }
    };

}
