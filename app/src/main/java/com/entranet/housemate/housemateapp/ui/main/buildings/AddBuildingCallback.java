package com.entranet.housemate.housemateapp.ui.main.buildings;

/**
 * Interface for routing action back to the {@link BuildingFragment}
 */

public interface AddBuildingCallback {

    void onAddBuildingSuccess();

    void onAddBuildingFailure(String errorMessage);
}
