package com.entranet.housemate.housemateapp.data.db.entity;

import com.squareup.moshi.Json;

/**
 * Embedded object representing the weekdays for repetition of actions.
 */

public class Day {
    // Specific date.
    @Json(name = "date")
    private String date;
    // Weekdays.
    @Json(name = "Monday")
    private boolean monday;
    @Json(name = "Tuesday")
    private boolean tuesday;
    @Json(name = "Wednesday")
    private boolean wednesday;
    @Json(name = "Thursday")
    private boolean thursday;
    @Json(name = "Friday")
    private boolean friday;
    @Json(name = "Saturday")
    private boolean saturday;
    @Json(name = "Sunday")
    private boolean sunday;

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public boolean isMonday() {
        return monday;
    }

    public void setMonday(boolean monday) {
        this.monday = monday;
    }

    public boolean isTuesday() {
        return tuesday;
    }

    public void setTuesday(boolean tuesday) {
        this.tuesday = tuesday;
    }

    public boolean isWednesday() {
        return wednesday;
    }

    public void setWednesday(boolean wednesday) {
        this.wednesday = wednesday;
    }

    public boolean isThursday() {
        return thursday;
    }

    public void setThursday(boolean thursday) {
        this.thursday = thursday;
    }

    public boolean isFriday() {
        return friday;
    }

    public void setFriday(boolean friday) {
        this.friday = friday;
    }

    public boolean isSaturday() {
        return saturday;
    }

    public void setSaturday(boolean saturday) {
        this.saturday = saturday;
    }

    public boolean isSunday() {
        return sunday;
    }

    public void setSunday(boolean sunday) {
        this.sunday = sunday;
    }
}
