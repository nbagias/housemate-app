package com.entranet.housemate.housemateapp.ui.main.custom;

import com.entranet.housemate.housemateapp.data.helpers.ListMenu;

/**
 * Parent interface for all the tab sections.
 * Useful for having only one {@link MenuItemAdapter}
 */
public interface SelectionsListener {
    void onMenuItemSelected(ListMenu.MenuItem item);
}
