package com.entranet.housemate.housemateapp.api.building;

import android.arch.persistence.room.ColumnInfo;

import com.squareup.moshi.Json;

/**
 * Sub-part of the {@link GetBuildingsResponse}
 */

public class BuildingSettings {

    @Json(name = "region")
    private String timezone;
    @Json(name = "city")
    private String location;
    @Json(name = "numberOfNews")
    @ColumnInfo(name = "num_of_news")
    private int numOfNews;

    public String getTimezone() { return timezone; }

    public void setTimezone(String timezone) { this.timezone = timezone; }

    public String getLocation() { return location; }

    public void setLocation(String location) { this.location = location; }

    public int getNumOfNews() { return numOfNews; }

    public void setNumOfNews(int numOfNews) { this.numOfNews = numOfNews; }
}
