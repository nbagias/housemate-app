package com.entranet.housemate.housemateapp.data.helpers;

import android.app.Activity;
import android.content.Context;
import android.text.TextUtils;
import android.util.Log;
import android.view.inputmethod.InputMethodManager;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Helper class for validating user input.
 */

public class InputHelper {

    public static final int PIN_NUMBER_OF_DIGITS = 4;
    private static final int BUILDING_ID_NUMBER_OF_CHARACTERS = 8;
    private static final int PASSWORD_MINIMUM_CHARACTERS = 6;
    // Should contain at least one digit, one upper/lower case.
    private static final String PASSWORD_PATTERN = "^(?=.*[0-9])(?=.*[a-z])(?=.*[A-Z])(?=\\S+$).{"
            + PASSWORD_MINIMUM_CHARACTERS + ",}$";
    private static final String BUILDING_ID_PATTERN = "^[a-zA-Z0-9]*$";

    private static final int SCENE_MINIMUM_CHARACTERS = 5;

    private static final String TAG = InputHelper.class.getSimpleName();

    private InputHelper(){}

    public static boolean isValidEmail(String email) {
        return !TextUtils.isEmpty(email) && android.util.Patterns.EMAIL_ADDRESS.matcher(email).matches();
    }

    public static boolean isValidPassword(String password) {
        // Check for emptiness
        if (TextUtils.isEmpty(password)) return false;
        // Check the provided pattern.
        Pattern pattern = Pattern.compile(PASSWORD_PATTERN);
        Matcher matcher = pattern.matcher(password);
        return matcher.matches();
    }

    public static boolean isValidPin(String pin) {
        // Make sure it's not empty and has 4 characters.
        return !TextUtils.isEmpty(pin) && pin.length() == PIN_NUMBER_OF_DIGITS;
    }

    public static boolean isValidSceneName(String sceneName) {
        // Make sure it's not empty and has the minimum characters.
        return !TextUtils.isEmpty(sceneName) && sceneName.length() >= SCENE_MINIMUM_CHARACTERS;
    }

    public static boolean isValidBuildingId(String buildingId) {
        // Make sure it's not empty and has 8 characters.
        if (TextUtils.isEmpty(buildingId) || buildingId.length() != BUILDING_ID_NUMBER_OF_CHARACTERS) {
            return false;
        }
        // Check regex.
        Pattern pattern = Pattern.compile(BUILDING_ID_PATTERN);
        Matcher matcher = pattern.matcher(buildingId);
        return matcher.matches();
    }

    /**
     * Hides the soft keyboard.
     * It is used after a successful validation of the register form
     * and is mostly useful when the user selected the ime option.
     *
     * @param activity parent activity to get window token
     */
    public static void hideKeyboard(Activity activity) {
        InputMethodManager imm = (InputMethodManager) activity.getSystemService(Context.INPUT_METHOD_SERVICE);
        if (activity.getCurrentFocus() != null && imm != null) {
            imm.hideSoftInputFromWindow(activity.getCurrentFocus().getApplicationWindowToken(), 0);
        }
    }

    public static String md5(final String s) {
        final String MD5 = "MD5";
        try {
            // Create MD5 Hash
            MessageDigest digest = java.security.MessageDigest.getInstance(MD5);
            digest.update(s.getBytes());
            byte[] messageDigest = digest.digest();

            // Create Hex String
            StringBuilder hexString = new StringBuilder();
            for (byte aMessageDigest : messageDigest) {
                StringBuilder h = new StringBuilder(Integer.toHexString(0xFF & aMessageDigest));
                while (h.length() < 2)
                    h.insert(0, "0");
                hexString.append(h);
            }
            return hexString.toString();

        } catch (NoSuchAlgorithmException e) {
            Log.e(TAG, "md5 encryption failed. NoSuchAlgorithmException: " + e.getMessage());
        }
        return "";
    }
}
