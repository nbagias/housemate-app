package com.entranet.housemate.housemateapp.api.login;

import com.entranet.housemate.housemateapp.data.helpers.Constants;
import com.squareup.moshi.Json;

import static com.entranet.housemate.housemateapp.data.helpers.InputHelper.md5;

/**
 * Class representing the credentials of the user.
 * Used in retrofit's body request.
 */

public class CredentialsBody {
    @Json(name = "email")
    private String email;
    @Json(name = "password")
    private String password;
    @Json(name = "origin")
    protected static final int ORIGIN = Constants.ORIGIN_HOUSEMATE_APP;
    // This parameter is optional.
    @Json(name = "notificationToken")
    private String firebaseToken;

    public CredentialsBody(String email, String password) {
        this.email = email;
        this.password = md5(password);
    }

    /**
     * Returns the password.
     *
     * @return the password
     */
    public String getPassword() {
        return password;
    }

    /**
     * Sets the password provided by the user.
     * @param password the password
     */
    public void setPassword(String password) {
        this.password = password;
    }

    /**
     * Returns the email.
     * @return the email
     */
    public String getEmail() {
        return email;
    }

    /**
     * Sets the email provided by the user.
     * @param email the email
     */
    public void setEmail(String email) {
        this.email = email;
    }

    public void setFirebaseToken(String firebaseToken) {
        this.firebaseToken = firebaseToken;
    }
}
