package com.entranet.housemate.housemateapp.ui.main.smarthome.switches;

import android.content.Context;
import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.ImageButton;
import android.widget.TextView;

import com.entranet.housemate.housemateapp.R;
import com.entranet.housemate.housemateapp.data.DataRepository;
import com.entranet.housemate.housemateapp.data.db.entity.SwitchEntity;
import com.entranet.housemate.housemateapp.data.helpers.Constants;
import com.entranet.housemate.housemateapp.ui.main.smarthome.scenes.LoadSwitchesCallback;
import com.entranet.housemate.housemateapp.ui.main.smarthome.switches.custom.SwitchesAdapter;

import java.util.List;

import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

public class SwitchesActivity extends AppCompatActivity implements
        EditSwitchFragment.OnEditSwitchListener,
        SwitchesAdapter.SwitchItemListener,
        LoadSwitchesCallback {

    private DataRepository repository;
    // UI elements
    private RecyclerView recyclerView;
    private SwipeRefreshLayout refreshLayout;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_switches);
        // UI related setup
        setupView();
        // Get a reference to our data repository.
        repository = DataRepository.getInstance(getApplicationContext());
        // Load the list with switches.
        repository.getSwitches(this);
    }

    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }

    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }

    @Override
    public void onRefreshStarted() { runOnUiThread(() -> refreshLayout.setRefreshing(true)); }

    @Override
    public void onRefreshSuccess() { runOnUiThread(() -> refreshLayout.setRefreshing(false)); }

    @Override
    public void onRefreshFailure() {
        runOnUiThread(() -> refreshLayout.setRefreshing(false));
        // Popup error message.
        Snackbar snackbar = Snackbar.make(findViewById(R.id.switches_thermostat_container), R.string.switches_msg_refresh_failed, Snackbar.LENGTH_SHORT);
        snackbar.show();
    }

    @Override
    public void onRenameSwitch(String newName, String switchId) {
        if (newName.isEmpty()) {
            Snackbar snackbar = Snackbar.make(findViewById(R.id.switches_thermostat_container), R.string.switches_msg_invalid_name, Snackbar.LENGTH_SHORT);
            snackbar.show();
        } else {
            repository.changeSwitchName(newName, switchId);
            SwitchesAdapter adapter = (SwitchesAdapter) recyclerView.getAdapter();
            adapter.changeSwitchName(newName, switchId);
        }
    }

    @Override
    public void onLoadSwitchesSuccess(List<SwitchEntity> switchesList) {
        final SwitchesAdapter adapter = new SwitchesAdapter(switchesList, this);
        runOnUiThread(() -> recyclerView.setAdapter(adapter));
    }

    @Override
    public void onSetThermostatValue(final int value, boolean hasThermostatPart) {
        final TextView txtTemperature = findViewById(R.id.switches_thermostat_value);
        runOnUiThread(() -> txtTemperature.setText(String.valueOf(value)));
    }

    @Override
    public void onSetThermostatDisabled() {
        ImageButton btnIncrease = findViewById(R.id.switches_thermostat_increase);
        btnIncrease.setEnabled(false);
        ImageButton btnDecrease = findViewById(R.id.switches_thermostat_decrease);
        btnDecrease.setEnabled(false);
    }

    @Override
    public void onLoadSwitchesFailure() {
        Snackbar snackbar = Snackbar.make(findViewById(R.id.switches_thermostat_container), R.string.switches_msg_load_failed, Snackbar.LENGTH_SHORT);
        snackbar.show();
    }

    @Override
    public void onSwitchStateChanged(SwitchEntity switchEntity) {
        repository.updateSwitchState(switchEntity);
    }

    @Override
    public void onSwitchTextClicked(SwitchEntity switchEntity) {
        // Launch edit switch fragment.
        EditSwitchFragment fragment = EditSwitchFragment.newInstance(switchEntity.getName(), switchEntity.getSwitchId());
        FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
        fragment.show(ft, EditSwitchFragment.class.getSimpleName());
    }

    private void setupView() {
        Toolbar toolbar = findViewById(R.id.toolbar);
        toolbar.setTitle("");
        setSupportActionBar(toolbar);
        ActionBar actionBar = getSupportActionBar();
        if (actionBar != null) actionBar.setDisplayHomeAsUpEnabled(true);
        // Set button listener.
        ImageButton btnIncrease = findViewById(R.id.switches_thermostat_increase);
        btnIncrease.setOnClickListener(clickListener);
        ImageButton btnDecrease = findViewById(R.id.switches_thermostat_decrease);
        btnDecrease.setOnClickListener(clickListener);
        // Setup recycler view.
        recyclerView = findViewById(R.id.switches_recycler);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        recyclerView.setHasFixedSize(true);

        refreshLayout = findViewById(R.id.switches_swipe_container);
        refreshLayout.setOnRefreshListener(refreshListener);
    }


    private SwipeRefreshLayout.OnRefreshListener refreshListener = new SwipeRefreshLayout.OnRefreshListener() {
        @Override
        public void onRefresh() { repository.refreshSwitches(SwitchesActivity.this); }
    };

    private View.OnClickListener clickListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            // Get the thermostat's value.
            TextView txtTemperature = findViewById(R.id.switches_thermostat_value);
            String temp = txtTemperature.getText().toString();
            int value = Integer.parseInt(temp);
            switch (v.getId()) {
                case R.id.switches_thermostat_decrease:
                    if (value == Constants.THERMOSTAT_MIN_VALUE) return;
                    value--;
                    break;
                case R.id.switches_thermostat_increase:
                    if (value == Constants.THERMOSTAT_MAX_VALUE) return;
                    value++;
                    break;
                default:
                    return;
            }
            // Update repository.
            repository.updateThermostat(value);
            // Update UI.
            txtTemperature.setText(String.valueOf(value));
        }
    };
}
