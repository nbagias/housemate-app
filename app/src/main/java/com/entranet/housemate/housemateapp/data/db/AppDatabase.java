package com.entranet.housemate.housemateapp.data.db;

import android.arch.persistence.room.Database;
import android.arch.persistence.room.Room;
import android.arch.persistence.room.RoomDatabase;
import android.arch.persistence.room.TypeConverters;
import android.content.Context;

import com.entranet.housemate.housemateapp.data.db.converters.ActionConverter;
import com.entranet.housemate.housemateapp.data.db.converters.SceneConverter;
import com.entranet.housemate.housemateapp.data.db.dao.ActionDao;
import com.entranet.housemate.housemateapp.data.db.dao.BuildingDao;
import com.entranet.housemate.housemateapp.data.db.dao.SceneDao;
import com.entranet.housemate.housemateapp.data.db.dao.SwitchDao;
import com.entranet.housemate.housemateapp.data.db.dao.UserDao;
import com.entranet.housemate.housemateapp.data.db.entity.ActionEntity;
import com.entranet.housemate.housemateapp.data.db.entity.BuildingEntity;
import com.entranet.housemate.housemateapp.data.db.entity.DeviceEntity;
import com.entranet.housemate.housemateapp.data.db.entity.GuestEntity;
import com.entranet.housemate.housemateapp.data.db.entity.SceneEntity;
import com.entranet.housemate.housemateapp.data.db.entity.SwitchEntity;
import com.entranet.housemate.housemateapp.data.db.entity.UserEntity;

/**
 * Room database for the whole app.
 * Following singleton pattern to reduce expensive calls.
 */
@Database(entities = {UserEntity.class, BuildingEntity.class,
        SwitchEntity.class, SceneEntity.class, GuestEntity.class,
        ActionEntity.class, DeviceEntity.class}, version = 13)
@TypeConverters({SceneConverter.class, ActionConverter.class})
public abstract class AppDatabase extends RoomDatabase {

    private static AppDatabase instance;
    private static final String DATABASE_FILENAME = "housemate_v2.db";

    public abstract UserDao userDao();
    public abstract BuildingDao buildingDao();
    public abstract SwitchDao switchDao();
    public abstract SceneDao sceneDao();
    public abstract ActionDao actionDao();

    public static synchronized AppDatabase getInstance(Context appContext) {
        if (instance == null) {
            instance = Room.databaseBuilder(appContext, AppDatabase.class, DATABASE_FILENAME)
                            .fallbackToDestructiveMigration() // DEVELOPMENT ONLY
                            .build();
        }
        return instance;
    }
}
