package com.entranet.housemate.housemateapp.data.db.entity;

import android.arch.persistence.room.ColumnInfo;
import android.arch.persistence.room.Entity;
import android.arch.persistence.room.PrimaryKey;

@Entity(tableName = "users")
public class UserEntity {
    @PrimaryKey(autoGenerate = true)
    private int id;

    @ColumnInfo(name = "token")
    private String token;

    @ColumnInfo(name = "email")
    private String email;

    @ColumnInfo(name = "status")
    private int status;

    @ColumnInfo(name = "pin")
    private String pin;

    @ColumnInfo(name = "has_building")
    private int hasBuilding;

    public int getId() {
        return id;
    }

    public void setId(int uId) {
        this.id = uId;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public int getHasBuilding() {
        return hasBuilding;
    }

    public void setHasBuilding(int hasBuilding) {
        this.hasBuilding = hasBuilding;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPin() {
        return pin;
    }

    public void setPin(String pin) {
        this.pin = pin;
    }
}
