package com.entranet.housemate.housemateapp.data.db.dao;

import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.OnConflictStrategy;
import android.arch.persistence.room.Query;

import com.entranet.housemate.housemateapp.data.db.entity.ActionEntity;

import java.util.List;

/**
 * Dao class for accessing the {@link ActionEntity} table.
 */
@Dao
public interface ActionDao {

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    void insertActions(List<ActionEntity> actions);

    @Query("SELECT * FROM actions WHERE building_id=:buildingId AND type=:type")
    List<ActionEntity> getActionsByType(String type, String buildingId);

    @Query("SELECT * FROM actions WHERE action_id=:actionId")
    ActionEntity getActionById(String actionId);

    @Query("DELETE FROM actions WHERE action_id=:actionId")
    void deleteActionById(String actionId);

    @Query("SELECT devices FROM actions WHERE action_id=:actionId")
    List<String> getDevicesByActionId(String actionId);

    @Query("DELETE FROM actions")
    void deleteAll();
}
