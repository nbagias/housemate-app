package com.entranet.housemate.housemateapp.ui.init.pin;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.inputmethod.EditorInfo;
import android.widget.EditText;
import android.widget.TextView;

import com.entranet.housemate.housemateapp.BuildConfig;
import com.entranet.housemate.housemateapp.R;
import com.entranet.housemate.housemateapp.data.DataRepository;
import com.entranet.housemate.housemateapp.data.helpers.InputHelper;
import com.entranet.housemate.housemateapp.ui.main.MainActivity;

import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

public class PinActivity extends AppCompatActivity implements CheckPinCallback {

    private static final String TAG = PinActivity.class.getSimpleName();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        if (!isTaskRoot()) {
            finish();
            return;
        }
        setContentView(R.layout.activity_pin);
        // Check whether there is a pin set or not.
        DataRepository repository = DataRepository.getInstance(getApplicationContext());
        repository.checkPinStatus(this);
    }

    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }

    @Override
    public void onUserHasPin() {
        // Set corresponding title.
        final TextView label = findViewById(R.id.pin_label);
        runOnUiThread(() -> label.setText(R.string.pin_enter_hint));
        // Add text change listener.
        EditText pinEditText = findViewById(R.id.pin_edittext);
        pinEditText.addTextChangedListener(textWatcher);
    }

    @Override
    public void onUserHasNoPin() {
        final TextView label = findViewById(R.id.pin_label);
        runOnUiThread(() -> label.setText(R.string.pin_create_hint));
        // Add ime listener.
        EditText editText = findViewById(R.id.pin_edittext);
        editText.setOnEditorActionListener(imeListener);
    }

    @Override
    public void onPinWrong() {
        final EditText editText = findViewById(R.id.pin_edittext);
        runOnUiThread(() -> editText.setError("Wrong PIN"));
    }

    @Override
    public void onPinRight() {
        // Disable user input.
        final EditText editText = findViewById(R.id.pin_edittext);
        runOnUiThread(() -> editText.setEnabled(false));
        // Redirect.
        startMain();
    }

    private TextWatcher textWatcher = new TextWatcher() {
        @Override
        public void beforeTextChanged(CharSequence s, int start, int count, int after) {}

        @Override
        public void onTextChanged(CharSequence s, int start, int before, int count) {
            if (BuildConfig.DEBUG) Log.d(TAG, "User entered: " + s);
            if (s.length() == InputHelper.PIN_NUMBER_OF_DIGITS) {
                DataRepository repository = DataRepository.getInstance(getApplicationContext());
                repository.checkPinCode(s.toString(), PinActivity.this);
            }
        }

        @Override
        public void afterTextChanged(Editable s) {}
    };

    private TextView.OnEditorActionListener imeListener = (v, actionId, event) -> {
        if (actionId == EditorInfo.IME_ACTION_DONE) {
            String input = v.getText().toString();
            if (validatePin(input)) {
                // Save PIN.
                DataRepository repository = DataRepository.getInstance(getApplicationContext());
                repository.setUserPin(input);
                // Redirect.
                startMain();
                return true;
            }
        }
        return false;
    };


    /**
     * Validates the provided pin and sets the error field appropriately.
     * Also, requests/clear focus for better user experience.
     *
     * @param pin the provided pin
     * @return true if valid, false otherwise
     */
    private boolean validatePin(String pin) {
        EditText editText = findViewById(R.id.pin_edittext);
        if (!InputHelper.isValidPin(pin)) {
            editText.setError(getString(R.string.register_error_pin_invalid));
            return false;
        }
        return true;
    }

    private void startMain() {
        // Start main panel.
        Intent intent = new Intent(this, MainActivity.class);
        startActivity(intent);
        // Exit
        finish();
    }
}
