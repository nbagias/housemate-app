package com.entranet.housemate.housemateapp.data.db.entity;

import android.arch.persistence.room.ColumnInfo;
import android.arch.persistence.room.Entity;
import android.arch.persistence.room.PrimaryKey;
import android.support.annotation.NonNull;

import com.squareup.moshi.Json;

/**
 * Entity class for storing devices.
 */

@Entity(tableName = "devices")
public class DeviceEntity {
    @NonNull
    @PrimaryKey
    @Json(name = "deviceID")
    private String id;
    @ColumnInfo(name = "building_id")
    private String buildingId;
    private String name;
    @ColumnInfo(name = "software_version")
    private String softwareVersion;

    // Accessors
    @NonNull
    public String getId() { return id; }

    public void setId(@NonNull String id) { this.id = id; }

    public String getName() { return name; }

    public void setName(String name) { this.name = name; }

    public String getSoftwareVersion() { return softwareVersion; }

    public void setSoftwareVersion(String softwareVersion) { this.softwareVersion = softwareVersion; }

    public String getBuildingId() { return buildingId; }

    public void setBuildingId(String buildingId) {this.buildingId = buildingId; }
}
