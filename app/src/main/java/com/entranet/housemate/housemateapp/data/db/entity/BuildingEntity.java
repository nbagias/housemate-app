package com.entranet.housemate.housemateapp.data.db.entity;

import android.arch.persistence.room.ColumnInfo;
import android.arch.persistence.room.Embedded;
import android.arch.persistence.room.Entity;
import android.arch.persistence.room.PrimaryKey;
import android.support.annotation.NonNull;

import com.entranet.housemate.housemateapp.api.building.BuildingPermissions;
import com.entranet.housemate.housemateapp.api.building.BuildingSettings;

/**
 * Class representing the building as entity.
 * It is a high-level structure parenting most of the application data logic.
 */
@Entity(tableName = "buildings")
public class BuildingEntity {
    @PrimaryKey
    @NonNull
    private String id;
    private String name;
    @ColumnInfo(name = "master")
    private int isMaster;
    @Embedded
    private BuildingPermissions permissions;
    @Embedded
    private BuildingSettings settings;
    @ColumnInfo(name = "is_default")
    private Boolean isDefault;

    // Accessors
    public String getName() { return name; }

    public void setName(String name) { this.name = name; }

    public int getIsMaster() { return isMaster; }

    public void setIsMaster(int isMaster) { this.isMaster = isMaster; }
    @NonNull
    public String getId() { return id; }

    public void setId(@NonNull String id) { this.id = id; }

    public BuildingPermissions getPermissions() { return permissions; }

    public void setPermissions(BuildingPermissions permissions) { this.permissions = permissions; }

    public BuildingSettings getSettings() { return settings; }

    public void setSettings(BuildingSettings settings) { this.settings = settings; }

    public Boolean getDefault() { return isDefault; }

    public void setDefault(Boolean isDefault) { this.isDefault = isDefault; }
}
