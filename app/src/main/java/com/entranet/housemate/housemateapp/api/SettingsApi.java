package com.entranet.housemate.housemateapp.api;

import com.entranet.housemate.housemateapp.api.settings.NumberOfNewsBody;
import com.entranet.housemate.housemateapp.api.settings.SetLocationBody;
import com.entranet.housemate.housemateapp.api.settings.SetTimezoneBody;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.POST;

/**
 * SettingsApi API is a subset of Housemate API. It only contains operations related to settings.
 */

public interface SettingsApi {

    @POST("/housemate/news/setNumberOfNews")
    Call<ApiResponse> setNumberOfNews(@Body NumberOfNewsBody body);

    @POST("/housemate/settings/setRegion")
    Call<ApiResponse> setTimezone(@Body SetTimezoneBody body);

    @POST("/housemate/settings/setCity")
    Call<ApiResponse> setLocation(@Body SetLocationBody body);
}
