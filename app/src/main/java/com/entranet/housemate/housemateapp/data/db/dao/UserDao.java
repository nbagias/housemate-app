package com.entranet.housemate.housemateapp.data.db.dao;

import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.OnConflictStrategy;
import android.arch.persistence.room.Query;

import com.entranet.housemate.housemateapp.data.db.entity.UserEntity;

/**
 * Dao class for accessing the {@link UserEntity} table.
 */

@Dao
public interface UserDao {

    @Query("SELECT * FROM users WHERE status = 1")
    UserEntity getActiveUser();

    @Query("SELECT pin FROM users WHERE status = 1")
    String getPin();

    @Query("UPDATE users SET pin=:pin WHERE status = 1")
    void setPin(String pin);

    @Query("DELETE FROM users")
    void deleteAll();

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    void insertUser(UserEntity user);
}
