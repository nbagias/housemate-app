package com.entranet.housemate.housemateapp.ui.main.smarthome.camera;

/**
 * Interface for routing action back to the activity.
 */

public interface GetVideoUrlCallback {
    void onVideoUrlReady(String url);

    void onVideoUrlUnavailable();
}
