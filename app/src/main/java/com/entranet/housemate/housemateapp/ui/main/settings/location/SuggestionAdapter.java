package com.entranet.housemate.housemateapp.ui.main.settings.location;

import android.content.Context;
import android.support.annotation.NonNull;
import android.widget.ArrayAdapter;
import android.widget.Filter;

import java.util.ArrayList;
import java.util.List;

public class SuggestionAdapter extends ArrayAdapter<String> {

    private final List<String> allLocations;
    private ArrayList<String> locations;

    private LocationFilter locationFilter = new LocationFilter();

    SuggestionAdapter(@NonNull Context context, int resource, @NonNull ArrayList<String> objects) {
        super(context, resource, objects);

        locations = objects;
        allLocations = new ArrayList<>(objects);
    }

    @Override
    public int getCount() { return locations.size(); }

    @NonNull
    @Override
    public Filter getFilter() { return locationFilter; }

    public class LocationFilter extends Filter {

        @Override
        protected FilterResults performFiltering(CharSequence prefix) {
            FilterResults results = new FilterResults();

            if (prefix == null || prefix.length() == 0) {
                synchronized (this) {
                    results.values = allLocations;
                    results.count = allLocations.size();
                }
            } else {
                final String searchStrLowerCase = prefix.toString().toLowerCase();

                ArrayList<String> matchValues = new ArrayList<>();

                for (String dataItem : allLocations) {
                    if (dataItem.toLowerCase().contains(searchStrLowerCase)) {
                        matchValues.add(dataItem);
                    }
                }

                results.values = matchValues;
                results.count = matchValues.size();
            }

            return results;
        }

        @Override
        protected void publishResults(CharSequence constraint, FilterResults results) {
            if (results.values != null) {
                locations.clear();
                locations.addAll((ArrayList<String>) results.values);
            } else {
                locations = null;
            }
            if (results.count > 0) {
                notifyDataSetChanged();
            } else {
                notifyDataSetInvalidated();
            }
        }

    }


}
