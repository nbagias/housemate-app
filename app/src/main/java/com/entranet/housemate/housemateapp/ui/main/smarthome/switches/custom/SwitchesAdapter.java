package com.entranet.housemate.housemateapp.ui.main.smarthome.switches.custom;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.entranet.housemate.housemateapp.R;
import com.entranet.housemate.housemateapp.data.db.entity.SwitchEntity;
import com.entranet.housemate.housemateapp.data.helpers.Constants;
import com.suke.widget.SwitchButton;

import java.util.List;

/**
 * Adapter for populating the recycler view with {@link SwitchEntity} objects.
 *
 */

public class SwitchesAdapter extends RecyclerView.Adapter<SwitchesAdapter.RecyclerViewHolder> {

    private List<SwitchEntity> switchEntities;
    private final SwitchItemListener listener;

    public SwitchesAdapter(List<SwitchEntity> switchEntities, SwitchItemListener listener) {
        this.switchEntities = switchEntities;
        this.listener = listener;
    }

    @Override
    public RecyclerViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new RecyclerViewHolder(LayoutInflater.from(parent.getContext())
                .inflate(R.layout.menuitem_devices, parent, false));
    }

    @Override
    public void onBindViewHolder(RecyclerViewHolder holder, int position) {
        final SwitchEntity entity = switchEntities.get(position);
        // Set switch's state.
        if (Constants.SWITCH_STATE_ON.equals(entity.getState())) {
            holder.toggleButton.setChecked(true);
        } else {
            holder.toggleButton.setChecked(false);
        }
        // Set switch's name.
        holder.textView.setText(entity.getName());
    }

    @Override
    public int getItemCount() { return switchEntities.size(); }

    public void changeSwitchName(String name, String switchId) {
        for (int i=0; i<switchEntities.size(); i++) {
            if (switchEntities.get(i).getSwitchId().equals(switchId)) {
                switchEntities.get(i).setName(name);
                notifyItemChanged(i);
                break;
            }
        }
    }

    public void setList(List<SwitchEntity> switchEntities) {
        this.switchEntities = switchEntities;
        notifyDataSetChanged();
    }

    class RecyclerViewHolder extends RecyclerView.ViewHolder{
        private final TextView textView;
        private final SwitchButton toggleButton;

        RecyclerViewHolder(View itemView) {
            super(itemView);
            textView = itemView.findViewById(R.id.devices_menu_item);
            toggleButton = itemView.findViewById(R.id.devices_toggle_button);
            textView.setOnClickListener(v -> listener.onSwitchTextClicked(switchEntities.get(getAdapterPosition())));

            toggleButton.setOnCheckedChangeListener((view, isChecked) -> {
                SwitchEntity entity = switchEntities.get(getAdapterPosition());
                if (isChecked) {
                    entity.setState(Constants.SWITCH_STATE_ON);
                } else {
                    entity.setState(Constants.SWITCH_STATE_OFF);
                }
                listener.onSwitchStateChanged(entity);
            });
        }
    }

    /**
     * Interface for routing action upon item events.
     */
    public interface SwitchItemListener {

        void onSwitchStateChanged(SwitchEntity switchEntity);

        void onSwitchTextClicked(SwitchEntity switchEntity);
    }
}
