package com.entranet.housemate.housemateapp.api.action;

import com.entranet.housemate.housemateapp.api.HomeApi;
import com.entranet.housemate.housemateapp.data.db.entity.ActionEntity;
import com.squareup.moshi.Json;

import java.util.List;

/**
 * Part of the API response for {@link HomeApi#getActions}.
 */

public class ActionsData {
    @Json(name = "actions")
    private List<ActionEntity> actions;

    public List<ActionEntity> getActions() {
        return actions;
    }

    public void setActions(List<ActionEntity> actions) {
        this.actions = actions;
    }
}
