package com.entranet.housemate.housemateapp.data.db.entity;

import android.arch.persistence.room.ColumnInfo;
import android.arch.persistence.room.Embedded;
import android.arch.persistence.room.Entity;
import android.arch.persistence.room.PrimaryKey;
import android.support.annotation.NonNull;

import com.squareup.moshi.Json;

import java.util.List;

/**
 * Entity class for storing actions.
 */
@Entity(tableName = "actions")
public class ActionEntity {
    @NonNull
    @PrimaryKey
    @ColumnInfo(name = "action_id")
    @Json(name = "id")
    private String actionId;
    @ColumnInfo(name = "building_id")
    private String buildingId;
    private String type;
    private String text;
    private String time;
    @Json(name = "repeat")
    private boolean repeated;
    @Json(name = "comfirmed") // Yes, there is a typo in the API.
    private boolean confirmed;
    private boolean enabled;
    @Embedded
    private Day day;

    private List<String> devices;

    // Accessors
    public String getType() { return type; }

    public void setType(String type) { this.type = type; }

    public String getText() { return text; }

    public void setText(String text) { this.text = text; }

    public String getTime() { return time; }

    public void setTime(String time) { this.time = time; }

    public boolean isRepeated() { return repeated; }

    public void setRepeated(boolean repeated) { this.repeated = repeated; }

    public Day getDay() { return day; }

    public void setDay(Day day) { this.day = day; }

    public String getDate() { return day.getDate(); }

    public void setDate(String date) { day.setDate(date); }

    public String getBuildingId() { return buildingId; }

    public void setBuildingId(String buildingId) { this.buildingId = buildingId; }

    public boolean isConfirmed() { return confirmed; }

    public void setConfirmed(boolean confirmed) { this.confirmed = confirmed; }

    @NonNull
    public String getActionId() { return actionId; }

    public void setActionId(@NonNull String actionId) { this.actionId = actionId; }

    public boolean isEnabled() { return enabled; }

    public void setEnabled(boolean enabled) { this.enabled = enabled; }

    public List<String> getDevices() { return devices; }

    public void setDevices(List<String> devices) { this.devices = devices; }
}
