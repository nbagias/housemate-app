package com.entranet.housemate.housemateapp.ui.main.smarthome.scenes.custom;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.entranet.housemate.housemateapp.R;
import com.entranet.housemate.housemateapp.data.db.entity.SwitchEntity;
import com.entranet.housemate.housemateapp.data.helpers.Constants;
import com.entranet.housemate.housemateapp.ui.main.smarthome.scenes.SceneEditorFragment;
import com.suke.widget.SwitchButton;

import java.util.ArrayList;
import java.util.List;

/**
 * SelectSwitchesAdapter populates the {@link SceneEditorFragment}.
 */

public class SelectSwitchesAdapter extends RecyclerView.Adapter<SelectSwitchesAdapter.SwitchViewHolder>  {

    private final List<SwitchEntity> switchesList;

    public SelectSwitchesAdapter(List<SwitchEntity> switchesList) {
        this.switchesList = switchesList;
    }

    public List<SwitchEntity> getSelectedList() {
        // Create a list with only the selected devices.
        List<SwitchEntity> selectedList = new ArrayList<>(switchesList);
        for (SwitchEntity entity: switchesList) {
            if (Constants.LIST_ITEM_UNSELECTED.equals(entity.getExtra())) {
                selectedList.remove(entity);
            }
        }
        return selectedList;
    }

    @Override
    public SelectSwitchesAdapter.SwitchViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new SwitchViewHolder(LayoutInflater.from(parent.getContext())
                .inflate(R.layout.list_item_device, parent, false));
    }

    @Override
    public void onBindViewHolder(final SelectSwitchesAdapter.SwitchViewHolder holder, int position) {
        final SwitchEntity switchEntity = switchesList.get(position);
        // Set the device's name as text.
        holder.textView.setText(switchEntity.getName());
        // We are going to use the extra field in the SwitchEntity class to save whether it is
        // selected by the user. Only unselected switches will get "dirty" (SELECTED = "")
        // and they will be filtered out at getSelectedList.
        // We could use the view holders to retrieve the -already there- info but let's avoid out of scope issues.
        holder.textView.setOnClickListener(v -> {
            holder.isSelected = !holder.isSelected;
            if (holder.isSelected) {
                holder.imgSelected.setVisibility(View.VISIBLE);
                switchEntity.setExtra(Constants.LIST_ITEM_SELECTED);
            } else {
                holder.imgSelected.setVisibility(View.INVISIBLE);
                switchEntity.setExtra(Constants.LIST_ITEM_UNSELECTED);
            }
        });
        if (switchEntity.getExtra().equals(Constants.LIST_ITEM_SELECTED)) {
            holder.imgSelected.setVisibility(View.VISIBLE);
            holder.isSelected = true;
        } else {
            holder.imgSelected.setVisibility(View.INVISIBLE);
            holder.isSelected = false;
        }
        // Show the current state for each device.
        if (switchEntity.getState().equals(Constants.SWITCH_STATE_ON)) {
            holder.toggleButton.setChecked(true);
        } else {
            holder.toggleButton.setChecked(false);
        }
    }

    @Override
    public int getItemCount() { return switchesList.size(); }

    class SwitchViewHolder extends RecyclerView.ViewHolder {
        private final TextView textView;
        private final SwitchButton toggleButton;
        private final ImageView imgSelected;
        private boolean isSelected;

        SwitchViewHolder(View itemView) {
            super(itemView);
            imgSelected = itemView.findViewById(R.id.list_item_device_selected);
            textView = itemView.findViewById(R.id.list_item_device_name);
            toggleButton = itemView.findViewById(R.id.list_item_device_toggle);
            toggleButton.setOnCheckedChangeListener((view, isChecked) -> {
                SwitchEntity switchEntity = switchesList.get(getAdapterPosition());
                if (isChecked) {
                    switchEntity.setState(Constants.SWITCH_STATE_ON);
                } else {
                    switchEntity.setState(Constants.SWITCH_STATE_OFF);
                }
            });
        }
    }
}
