package com.entranet.housemate.housemateapp.ui.main.settings.location;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.DialogFragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.ListView;

import com.entranet.housemate.housemateapp.R;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.TimeZone;

/**
 * Use the {@link TimezoneFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class TimezoneFragment extends DialogFragment {

    private OnTimezoneSelectionListener selectionListener;

    public TimezoneFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment.
     *
     * @return A new instance of fragment TimezoneFragment.
     */
    @NonNull
    public static TimezoneFragment newInstance() {
        return new TimezoneFragment();
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_timezone, container, false);
        // Fill the list with the available timezones.
        ListView list = view.findViewById(R.id.timezone_list);
        List<String> timezones = Arrays.asList(TimeZone.getAvailableIDs());
        ArrayAdapter<String> adapter = new ArrayAdapter<>(getContext(), android.R.layout.simple_list_item_1, timezones);
        list.setAdapter(adapter);
        list.setOnItemClickListener(itemListListener);

        SuggestionAdapter dropdownAdapter = new SuggestionAdapter(getContext(),
                android.R.layout.simple_dropdown_item_1line,
                new ArrayList<>(timezones));

        AutoCompleteTextView textView = view.findViewById(R.id.timezone_search_text_view);
        textView.setAdapter(dropdownAdapter);
        textView.setOnItemClickListener(itemListListener);

        return view;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnTimezoneSelectionListener) {
            selectionListener = (OnTimezoneSelectionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnTimezoneSelectionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        selectionListener = null;
    }

    private final AdapterView.OnItemClickListener itemListListener = new AdapterView.OnItemClickListener() {
        @Override
        public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
            if (selectionListener != null) {
                // Convert item to String.
                String selectedTimezone = (String) parent.getItemAtPosition(position);
                // Pass value to listener.
                selectionListener.onTimezoneSelected(selectedTimezone);
            }
            dismiss();
        }
    };

    /**
     * This interface must be implemented by activities that contain this
     * fragment. In our case this is {@link LocationActivity}.
     *
     * It returns user's selection so that we can save it to the database
     * and update the UI.
     */
    public interface OnTimezoneSelectionListener {
        void onTimezoneSelected(String selectedTimezone);
    }
}
