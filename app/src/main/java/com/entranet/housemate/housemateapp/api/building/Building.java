package com.entranet.housemate.housemateapp.api.building;

import com.entranet.housemate.housemateapp.data.db.entity.ActionEntity;
import com.entranet.housemate.housemateapp.data.db.entity.DeviceEntity;
import com.entranet.housemate.housemateapp.data.db.entity.GuestEntity;
import com.entranet.housemate.housemateapp.data.db.entity.SceneEntity;
import com.entranet.housemate.housemateapp.data.db.entity.SwitchEntity;
import com.squareup.moshi.Json;

import java.util.List;

/**
 * Class representing the building in API's response format.
 */

public class Building {
    @Json(name = "buildingID")
    private String buildingID;
    @Json(name = "name")
    private String name;
    @Json(name = "switches")
    private List<SwitchEntity> switches;
    @Json(name = "scenes")
    private List<SceneEntity> scenes;
    @Json(name = "devices")
    private List<DeviceEntity> devices;
    @Json(name = "actions")
    private List<ActionEntity> actions;
    @Json(name = "isMaster")
    private Boolean isMaster;
    @Json(name = "permissions")
    private BuildingPermissions permissions;
    @Json(name = "guests")
    private List<GuestEntity> guests;
    @Json(name = "settings")
    private BuildingSettings settings;

    public String getBuildingID() {
        return buildingID;
    }

    public void setBuildingID(String buildingID) {
        this.buildingID = buildingID;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<SwitchEntity> getSwitches() {
        return switches;
    }

    public void setSwitches(List<SwitchEntity> switches) {
        this.switches = switches;
    }

    public List<SceneEntity> getScenes() {
        return scenes;
    }

    public void setScenes(List<SceneEntity> scenes) {
        this.scenes = scenes;
    }

    public List<DeviceEntity> getDevices() {
        return devices;
    }

    public void setDevices(List<DeviceEntity> devices) {
        this.devices = devices;
    }

    public List<ActionEntity> getActions() {
        return actions;
    }

    public void setActions(List<ActionEntity> actions) {
        this.actions = actions;
    }

    public Boolean getIsMaster() {
        return isMaster;
    }

    public void setIsMaster(Boolean isMaster) {
        this.isMaster = isMaster;
    }

    public BuildingPermissions getPermissions() {
        return permissions;
    }

    public void setPermissions(BuildingPermissions permissions) {
        this.permissions = permissions;
    }

    public List<GuestEntity> getGuests() {
        return guests;
    }

    public void setGuests(List<GuestEntity> guests) {
        this.guests = guests;
    }

    public BuildingSettings getSettings() {
        return settings;
    }

    public void setSettings(BuildingSettings settings) {
        this.settings = settings;
    }

}
