package com.entranet.housemate.housemateapp.ui.main.smarthome.camera;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.entranet.housemate.housemateapp.R;
import com.entranet.housemate.housemateapp.data.db.entity.DeviceEntity;

import java.util.List;

/**
 * Adapter class for populating {@link CameraActivity} with devices.
 */

public class CameraOptionsAdapter extends  RecyclerView.Adapter<CameraOptionsAdapter.CameraOptionViewHolder> {

    private List<DeviceEntity> devices;
    private final CameraOptionListener listener;

    CameraOptionsAdapter(List<DeviceEntity> devices, CameraOptionListener listener) {
        this.devices = devices;
        this.listener = listener;
    }

    @Override
    public CameraOptionViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new CameraOptionViewHolder(LayoutInflater.from(parent.getContext())
                .inflate(R.layout.menuitem_camera, parent, false));
    }

    @Override
    public void onBindViewHolder(CameraOptionViewHolder holder, int position) {
        DeviceEntity device = devices.get(position);
        holder.txtTitle.setText(device.getName());
    }

    @Override
    public int getItemCount() { return devices.size(); }

    class CameraOptionViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        private final TextView txtTitle;
        private final TextView txtCall;
        private final TextView txtVideoCall;

        CameraOptionViewHolder(View itemView) {
            super(itemView);
            txtTitle = itemView.findViewById(R.id.menu_item_camera_title);
            txtCall = itemView.findViewById(R.id.menu_item_camera_call);
            txtVideoCall = itemView.findViewById(R.id.menu_item_camera_video_call);

            txtCall.setOnClickListener(this);
            txtVideoCall.setOnClickListener(this);
        }

        @Override
        public void onClick(View v) {
            DeviceEntity entity = devices.get(getAdapterPosition());
            if (v.getId() == R.id.menu_item_camera_call) {
                listener.onCameraOptionSelected(entity.getId(), true);
            } else if (v.getId() == R.id.menu_item_camera_video_call) {
                listener.onCameraOptionSelected(entity.getId(), false);
            }
        }
    }

    /**
     * Interface for routing action upon item events.
     */
    public interface CameraOptionListener {

        void onCameraOptionSelected(String deviceId, boolean isCall);
    }

}
