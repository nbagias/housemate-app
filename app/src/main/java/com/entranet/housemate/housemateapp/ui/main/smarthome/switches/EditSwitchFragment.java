package com.entranet.housemate.housemateapp.ui.main.smarthome.switches;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.TextInputEditText;
import android.support.v4.app.DialogFragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import com.entranet.housemate.housemateapp.R;
import com.entranet.housemate.housemateapp.data.db.entity.SwitchEntity;

/**
 *  {@link DialogFragment} for renaming a {@link SwitchEntity}.
 */
public class EditSwitchFragment extends DialogFragment implements View.OnClickListener {

    private OnEditSwitchListener listener;

    private static final String EXTRA_SWITCH_CURRENT_NAME = "currentName";
    private static final String EXTRA_SWITCH_ID = "switchId";

    private String name;
    private String switchId;

    private TextInputEditText txtNewName;

    public EditSwitchFragment() {
        // Required empty public constructor
    }

    public static EditSwitchFragment newInstance(String currentName, String id) {
        EditSwitchFragment fragment = new EditSwitchFragment();
        Bundle bundle = new Bundle();
        bundle.putString(EXTRA_SWITCH_CURRENT_NAME, currentName);
        bundle.putString(EXTRA_SWITCH_ID, id);
        fragment.setArguments(bundle);
        return fragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        Bundle bundle = getArguments();
        if (bundle != null) {
            switchId = bundle.getString(EXTRA_SWITCH_ID);
            name = bundle.getString(EXTRA_SWITCH_CURRENT_NAME);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_dialog_edit_switch, container, false);
        // Set the title dynamically.
        TextView txtTitle = view.findViewById(R.id.edit_switch_title);
        txtTitle.setText(getResources().getString(R.string.edit_switch_title, name));

        txtNewName = view.findViewById(R.id.edit_switch_edit_text);
        // Set button listener.
        Button btnRename = view.findViewById(R.id.edit_switch_button);
        btnRename.setOnClickListener(this);

        return view;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnEditSwitchListener) {
            listener = (OnEditSwitchListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnEditSceneListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        listener = null;
    }

    @Override
    public void onClick(View v) {
        if (listener != null) {
            if (txtNewName != null) {
                String newName = txtNewName.getText().toString().trim();
                listener.onRenameSwitch(newName, switchId);
                dismiss();
            }
        }
    }

    /**
     * Interface for routing action back to the caller.
     * Currently this is a {@link SwitchesActivity}.
     */
    public interface OnEditSwitchListener {

        void onRenameSwitch(String newName, String switchId);
    }
}
