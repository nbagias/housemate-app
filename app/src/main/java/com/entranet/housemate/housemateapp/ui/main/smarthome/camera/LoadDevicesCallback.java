package com.entranet.housemate.housemateapp.ui.main.smarthome.camera;

import com.entranet.housemate.housemateapp.data.db.entity.DeviceEntity;

import java.util.List;

/**
 * Interface for returning device list to caller.
 */

public interface LoadDevicesCallback {
    void onDeviceListLoaded(List<DeviceEntity> devices);
}
