package com.entranet.housemate.housemateapp.ui.main.settings.news;

import android.content.Context;
import android.os.Bundle;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.Button;

import com.entranet.housemate.housemateapp.R;
import com.entranet.housemate.housemateapp.data.DataRepository;

import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

public class NewsActivity extends AppCompatActivity implements NewsCallback {

    private static final String NUMBER_PICKER_KEY = "numberPickerKey";
    private static final String[] NUMBER_PICKER_VALUES = new String[] {"1" , "2", "3", "4", "5"};
    private static final int NUMBER_PICKER_MIN = 1;
    private static final int NUMBER_PICKER_MAX = 5;

    private CustomNumberPicker numberPicker;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_news);

        Toolbar toolbar = findViewById(R.id.toolbar);
        toolbar.setTitle("");
        setSupportActionBar(toolbar);
        ActionBar actionBar = getSupportActionBar();
        if (actionBar != null) actionBar.setDisplayHomeAsUpEnabled(true);

        numberPicker = findViewById(R.id.news_number_picker);
        numberPicker.setMinValue(NUMBER_PICKER_MIN);
        numberPicker.setMaxValue(NUMBER_PICKER_MAX);
        numberPicker.setDisplayedValues(NUMBER_PICKER_VALUES);

        Button btnSave = findViewById(R.id.bottom_button);
        btnSave.setText(R.string.save_button_title);
        btnSave.setOnClickListener(saveButtonListener);

        if (savedInstanceState == null) {
            DataRepository repository = DataRepository.getInstance(getApplicationContext());
            repository.getNumberOfNews(this);
        }
    }

    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }

    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        // Save the current value of the number picker.
        outState.putInt(NUMBER_PICKER_KEY, numberPicker.getValue());
        super.onSaveInstanceState(outState);
    }

    @Override
    protected void onRestoreInstanceState(Bundle savedInstanceState) {
        // Restore its value.
        numberPicker.setValue(savedInstanceState.getInt(NUMBER_PICKER_KEY));
    }

    private final View.OnClickListener saveButtonListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            Button btnSave = (Button) v;
            btnSave.setEnabled(false);

            int selectedValue = numberPicker.getValue();
            Log.d("NumberPicker", "User selected " + selectedValue + " as number of headlines.");

            DataRepository repository = DataRepository.getInstance(getApplicationContext());
            repository.setNumberOfNews(selectedValue);
            // Close the activity.
            finish();
        }
    };

    @Override
    public void onSetNumberPicker(int num) {
        // Set its stored value.
        numberPicker.setValue(num);
    }
}
