package com.entranet.housemate.housemateapp.api.building;

import com.entranet.housemate.housemateapp.api.AdminApi;

/**
 * API body request for {@link AdminApi#addUserToBuilding}.
 */

public class AddUserToBuildingBody {

    private String buildingID;
    private String buildingName;
    private String token;
    private String email;

    public AddUserToBuildingBody(String buildingID, String token, String email) {
        this.buildingID = buildingID;
        this.token = token;
        this.email = email;
    }

    // Accessors
    public String getEmail() { return email; }

    public void setEmail(String email) { this.email = email; }

    public String getToken() { return token; }

    public void setToken(String token) { this.token = token; }

    public String getBuildingID() { return buildingID; }

    public void setBuildingID(String buildingID) { this.buildingID = buildingID; }

    public String getBuildingName() { return buildingName; }

    public void setBuildingName(String buildingName) { this.buildingName = buildingName;}
}
