package com.entranet.housemate.housemateapp.ui.init.register;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.TextInputLayout;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;

import com.entranet.housemate.housemateapp.R;
import com.entranet.housemate.housemateapp.data.helpers.InputHelper;

/**
 * This {@link Fragment} contains the register UI logic.
 * The validations are performed here but the actual registration
 * is performed in the listener's side, by implementing the
 * {@link OnRegisterListener} interface.
 */
public class RegisterFragment extends Fragment implements View.OnClickListener {
    // Listener for communicating with parent activity
    private OnRegisterListener listener;
    // UI elements
    private EditText textEmail;
    private EditText textPassword;
    private TextInputLayout inputLayoutEmail;
    private TextInputLayout inputLayoutPassword;
    private Button btnRegister;

    public RegisterFragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        final View view = inflater.inflate(R.layout.fragment_register, container, false);
        // Get references for the UI elements.
        textEmail = view.findViewById(R.id.register_edittext_email);
        inputLayoutEmail = view.findViewById(R.id.register_container_email);
        textPassword = view.findViewById(R.id.register_edittext_password);
        inputLayoutPassword = view.findViewById(R.id.register_container_password);
        // Set a listener on the register button.
        btnRegister = view.findViewById(R.id.register_button);
        btnRegister.setOnClickListener(this);

        return view;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnRegisterListener) {
            listener = (OnRegisterListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnRegisterListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        listener = null;
    }

    @Override
    public void onClick(View v) {
        // Check abnormal conditions.
        if (v.getId() != R.id.register_button) return;
        if (listener == null) return;
        // Prompt register operation
        registerUser();
    }

    /**
     * Performs the operation of validating and sending the info
     * to the listener. Also, wrapping the operation  in a function
     * avoids duplicate code (ime/button).
     */
    private void registerUser() {
        // Disable button. Snackbar listener will enable it, if needed.
        btnRegister.setEnabled(false);
        // Validate email.
        String email = textEmail.getText().toString().trim();
        if (validateEmail(email)) {
            // Validate password.
            String password = textPassword.getText().toString().trim();
            if (validatePassword(password)) {
                // Pass the info to listener activity.
                listener.onRegisterClicked(email, password);
                return;
            }
        }
        // Hide the keyboard, for better ux.
        InputHelper.hideKeyboard(getActivity());
        // Enable again.
        btnRegister.setEnabled(true);
    }

    /**
     * Validates the provided email and sets the error field appropriately.
     * Also, requests/clear focus for better user experience.
     *
     * @param email the provided email
     * @return true if valid, false otherwise
     */
    private boolean validateEmail(String email) {
        if (!InputHelper.isValidEmail(email)) {
            inputLayoutEmail.setError(getString(R.string.register_error_email_invalid));
            inputLayoutEmail.requestFocus();
            return false;
        } else {
            // Remove previous errors, if any.
            inputLayoutEmail.setError("");
            inputLayoutEmail.clearFocus();
            return true;
        }
    }

    /**
     * Validates the provided password and sets the error field appropriately.
     * Also, requests/clear focus for better user experience.
     *
     * @param password the provided password
     * @return true if valid, false otherwise
     */
    private boolean validatePassword(String password) {
        if (!InputHelper.isValidPassword(password)) {
            inputLayoutPassword.setError(getString(R.string.register_error_password_invalid));
            inputLayoutPassword.requestFocus();
            return false;
        } else {
            // Remove previous errors, if any.
            inputLayoutPassword.setError("");
            inputLayoutPassword.clearFocus();
            return true;
        }
    }

    /**
     * This interface must be implemented by StarterActivity, in order to
     * perform the register operation.
     *
     */
    public interface OnRegisterListener {
        void onRegisterClicked(String email, String password);
    }
}
