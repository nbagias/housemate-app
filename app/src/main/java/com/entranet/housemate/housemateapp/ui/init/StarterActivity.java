package com.entranet.housemate.housemateapp.ui.init;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.support.design.widget.TabLayout;
import android.support.v4.view.ViewPager;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.widget.Button;

import com.entranet.housemate.housemateapp.R;
import com.entranet.housemate.housemateapp.api.building.BuildingData;
import com.entranet.housemate.housemateapp.api.login.CredentialsBody;
import com.entranet.housemate.housemateapp.data.DataRepository;
import com.entranet.housemate.housemateapp.ui.init.custom.ViewPagerAdapter;
import com.entranet.housemate.housemateapp.ui.init.login.LoginFragment;
import com.entranet.housemate.housemateapp.ui.init.register.RegisterFragment;
import com.entranet.housemate.housemateapp.ui.main.MainActivity;

import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

/**
 * Starter Activity is responsible for the Login / Register operations.
 *
 * It consists of a {@link ViewPager} with two {@link android.support.v4.app.Fragment}.
 *
 * The fragments are responsible for input validation.
 * Valid results are returned here.
 * The activity routes the flow through callbacks.
 *
 */
public class StarterActivity extends AppCompatActivity implements RegisterFragment.OnRegisterListener,
        LoginFragment.OnLoginListener, StarterOpsInterface {

    private DataRepository repository;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_starter);
        // Setup action bar
        Toolbar toolbar = findViewById(R.id.starter_toolbar);
        setupToolbar(toolbar);
        // Setup viewpager
        ViewPager viewPager = findViewById(R.id.starter_viewpager);
        setupViewPager(viewPager);
        // Hook tab layout with viewpager
        TabLayout tabLayout = findViewById(R.id.starter_tabs);
        tabLayout.setupWithViewPager(viewPager);
        // Get a reference to data repository to do the legwork.
        repository = DataRepository.getInstance(getApplicationContext());
    }

    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }

    @Override
    public void onRegisterClicked(String email, String password) {
        // First create the register parameters as an object
        CredentialsBody credentials = new CredentialsBody(email, password);
        // Start the register operation.
        repository.registerUser(credentials, this);
    }

    @Override
    public void onLoginClicked(String email, String password) {
        // Create the login parameters as an object.
        CredentialsBody credentials = new CredentialsBody(email, password);
        // Start the login operation.
        repository.loginUser(credentials, this);
    }

    @Override
    public void onRegisterFailure() {
        // Output the error
        Snackbar snackbar = Snackbar.make(findViewById(R.id.register_coordinator), R.string.register_error_message, Snackbar.LENGTH_LONG);
        // Add a callback to enable the button on dismiss. It gets disabled right after getting clicked.
        // Note: disabling it here (onShown) is not enough to avoid glitches when user spams it, even if it is tempting to do.
        snackbar.addCallback(new Snackbar.Callback() {
            @Override
            public void onDismissed(Snackbar snackbar, int event) {
                Button btnRegister = findViewById(R.id.register_button);
                // Let's be careful since its an element from a fragment in a viewpager.
                if (btnRegister != null)
                    btnRegister.setEnabled(true);
            }
        });
        snackbar.show();
    }

    @Override
    public void onLoginSuccess(BuildingData data) { repository.saveBuildingInfo(data, this); }

    @Override
    public void onLoginFailure(int resId) {
        // Output the error
        Snackbar snackbar = Snackbar.make(findViewById(R.id.login_coordinator), resId, Snackbar.LENGTH_LONG);
        // Add a callback to enable the button on dismiss. It gets disabled right after getting clicked.
        // Note: disabling it here (onShown) is not enough to avoid glitches when user spams it, even if it is tempting to do.
        snackbar.addCallback(new Snackbar.Callback() {
            @Override
            public void onDismissed(Snackbar snackbar, int event) {
                Button btnLogin = findViewById(R.id.login_button);
                if (btnLogin != null)
                    btnLogin.setEnabled(true);
                Button btnRegister = findViewById(R.id.register_button);
                if (btnRegister != null)
                    btnRegister.setEnabled(true);
            }
        });
        snackbar.show();
    }

    @Override
    public void onSaveBuildingsCompleted() {
        Intent mainIntent = new Intent(this, MainActivity.class);
        startActivity(mainIntent);
        finish();
    }

    /**
     * Sets the adapter for the provided {@link ViewPager}.
     * We are using a fragment adapter to serve as content for the two tabs.
     * @param viewPager the viewpager to set
     */
    private void setupViewPager(ViewPager viewPager) {
        ViewPagerAdapter adapter = new ViewPagerAdapter(getSupportFragmentManager());
        adapter.addFragment(new LoginFragment(), getString(R.string.starter_tab_login_title));
        adapter.addFragment(new RegisterFragment(), getString(R.string.starter_tab_register_title));
        viewPager.setAdapter(adapter);
    }

    /**
     * Sets the provided toolbar as support action bar.
     * Also, removes the textView for it.
     * @param toolbar layout's toolbar
     */
    private void setupToolbar(Toolbar toolbar) {
        setSupportActionBar(toolbar);
        ActionBar supportActionBar = getSupportActionBar();
        if (supportActionBar != null) {
            supportActionBar.setDisplayShowTitleEnabled(false);
        }
    }
}
