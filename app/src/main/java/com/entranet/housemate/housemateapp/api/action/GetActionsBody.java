package com.entranet.housemate.housemateapp.api.action;

import com.entranet.housemate.housemateapp.api.HomeApi;
import com.entranet.housemate.housemateapp.data.helpers.Constants;
import com.squareup.moshi.Json;

/**
 * API body request for {@link HomeApi#getActions}
 */

public class GetActionsBody {
    @Json(name = "token")
    private String token;
    @Json(name = "buildingID")
    private String buildingID;
    @Json(name = "origin")
    private final int ORIGIN = Constants.ORIGIN_HOUSEMATE_APP;
    @Json(name = "type")
    private String type;

    public GetActionsBody(String type) { this.type = type; }

    public void setToken(String token) { this.token = token; }

    public void setBuildingID(String buildingID) { this.buildingID = buildingID; }
}
