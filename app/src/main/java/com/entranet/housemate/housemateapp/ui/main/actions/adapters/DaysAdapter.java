package com.entranet.housemate.housemateapp.ui.main.actions.adapters;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import com.entranet.housemate.housemateapp.R;
import com.entranet.housemate.housemateapp.data.db.entity.Day;
import com.entranet.housemate.housemateapp.data.helpers.ListMenu;

import java.util.Calendar;
import java.util.List;

/**
 * Adapter class for populating a (static) list of weekdays.
 */

public class DaysAdapter extends RecyclerView.Adapter<DaysAdapter.DayViewHolder> {

    private final List<ListMenu.DayItem> weekDays;

    public DaysAdapter (List<ListMenu.DayItem> weekDays) {
        this.weekDays = weekDays;
    }

    @Override
    public DayViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new DayViewHolder(LayoutInflater.from(parent.getContext())
                .inflate(R.layout.menuitem_day, parent, false));
    }

    @Override
    public void onBindViewHolder(DayViewHolder holder, int position) {
        ListMenu.DayItem item = weekDays.get(position);
        // Initialize UI element according to the item's attributes.
        holder.button.setText(item.getTitle());
        holder.button.setBackgroundResource(item.getIcon());
        holder.button.setActivated(item.isSelected());
    }

    @Override
    public int getItemCount() {
        return weekDays.size();
    }

    public void updateMenu(Day actionDay) {
        // Set selection for each day.
        for (ListMenu.DayItem day: weekDays) {
            switch (day.getId()) {
                case Calendar.MONDAY:
                    day.setSelected(actionDay.isMonday());
                    break;
                case Calendar.TUESDAY:
                    day.setSelected(actionDay.isTuesday());
                    break;
                case Calendar.WEDNESDAY:
                    day.setSelected(actionDay.isWednesday());
                    break;
                case Calendar.THURSDAY:
                    day.setSelected(actionDay.isThursday());
                    break;
                case Calendar.FRIDAY:
                    day.setSelected(actionDay.isFriday());
                    break;
                case Calendar.SATURDAY:
                    day.setSelected(actionDay.isSaturday());
                    break;
                case Calendar.SUNDAY:
                    day.setSelected(actionDay.isSunday());
                    break;
                default:
                    break;
            }
        }
        // Update UI.
        notifyDataSetChanged();
    }

    public Day getActionDay() {
        // First check if there is at least one day selected.
        boolean isAtLeastOneSelected = false;
        for (ListMenu.DayItem item: weekDays) {
            if (item.isSelected()) {
                isAtLeastOneSelected = true;
                break;
            }
        }
        if (!isAtLeastOneSelected) return null;

        Day day = new Day();
        // Set selection for each day.
        for (ListMenu.DayItem item: weekDays) {
            switch (item.getId()) {
                case Calendar.MONDAY:
                    day.setMonday(item.isSelected());
                    break;
                case Calendar.TUESDAY:
                    day.setTuesday(item.isSelected());
                    break;
                case Calendar.WEDNESDAY:
                    day.setWednesday(item.isSelected());
                    break;
                case Calendar.THURSDAY:
                    day.setThursday(item.isSelected());
                    break;
                case Calendar.FRIDAY:
                    day.setFriday(item.isSelected());
                    break;
                case Calendar.SATURDAY:
                    day.setSaturday(item.isSelected());
                    break;
                case Calendar.SUNDAY:
                    day.setSunday(item.isSelected());
                    break;
                default:
                    break;
            }
        }
        return day;
    }

    public boolean[] getCurrentState() {
        boolean[] selectedState = new boolean[weekDays.size()];
        for (int i = 0; i < weekDays.size(); i++) {
            selectedState[i] = weekDays.get(i).isSelected();
        }
        return selectedState;
    }

    public void updateMenu(boolean[] selectedState) {
        for (int i = 0; i < weekDays.size(); i++) {
            weekDays.get(i).setSelected(selectedState[i]);
        }
        notifyDataSetChanged();
    }

    class DayViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        private final Button button;

        DayViewHolder (View itemView) {
            super(itemView);
            button = itemView.findViewById(R.id.menu_item_day);
            button.setOnClickListener(this);
        }

        @Override
        public void onClick(View v) {
            ListMenu.DayItem item = weekDays.get(getAdapterPosition());
            if (v.isActivated()) {
                v.setActivated(false);
                item.setSelected(false);
            } else {
                v.setActivated(true);
                item.setSelected(true);
            }
        }
    }
}
