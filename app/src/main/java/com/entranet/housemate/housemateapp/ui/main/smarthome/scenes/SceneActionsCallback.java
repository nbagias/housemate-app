package com.entranet.housemate.housemateapp.ui.main.smarthome.scenes;

import java.util.List;

/**
 * Interface for handling actions upon scenes asynchronously.
 */

public interface SceneActionsCallback {

    void onShowSceneActivated(String name);

    void onShowSceneActivationFailed(String name);

    void onScenesLoaded(List<String> scenes);

}
