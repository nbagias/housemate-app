package com.entranet.housemate.housemateapp.ui.main.actions.adapters;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckedTextView;

import com.entranet.housemate.housemateapp.R;
import com.entranet.housemate.housemateapp.data.db.entity.SceneEntity;
import com.entranet.housemate.housemateapp.data.helpers.ListMenu;

import java.util.List;

/**
 * Adapter class for populating actor's list.
 * Menu items are wrapped {@link SceneEntity}.
 */

public class SceneOptionsAdapter extends  RecyclerView.Adapter<SceneOptionsAdapter.ActorItemViewHolder> {

    private final List<ListMenu.SceneItem> scenes;
    private final SceneOptionsListener listener;

    public SceneOptionsAdapter(List<ListMenu.SceneItem> scenes, SceneOptionsListener listener) {
        this.scenes = scenes;
        this.listener = listener;
    }

    @NonNull
    @Override
    public ActorItemViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new ActorItemViewHolder(LayoutInflater.from(parent.getContext())
                .inflate(R.layout.menuitem_actor, parent, false));
    }

    @Override
    public void onBindViewHolder(@NonNull ActorItemViewHolder holder, int position) {
        ListMenu.SceneItem item = scenes.get(position);
        holder.cxtTitle.setText(item.getName());
        holder.cxtTitle.setChecked(item.isSelected());
    }

    @Override
    public int getItemCount() { return scenes.size(); }

    public SceneEntity getSelectedScene() {
        for (ListMenu.SceneItem item : scenes) {
            if (item.isSelected()) {
                return item.getScene();
            }
        }
        return null;
    }

    public void updateMenu(boolean[] actorsSelection) {
        for (int i = 0; i < scenes.size(); i++) {
            scenes.get(i).setSelected(actorsSelection[i]);
        }
    }

    public boolean[] getCurrentState() {
        boolean[] selectedState = new boolean[scenes.size()];
        for (int i = 0; i < scenes.size(); i++) {
            selectedState[i] = scenes.get(i).isSelected();
        }
        return selectedState;
    }

    class ActorItemViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        private final CheckedTextView cxtTitle;

        ActorItemViewHolder(View itemView) {
            super(itemView);
            cxtTitle = itemView.findViewById(R.id.menu_item_actor_title);
            cxtTitle.setOnClickListener(this);
        }

        @Override
        public void onClick(View v) {
            // Enforce one selection only policy.
            updateMenu(getAdapterPosition(), cxtTitle.isChecked());

            listener.onSceneSelected(cxtTitle.getText().toString());
            // Update UI.
            notifyDataSetChanged();
        }

        private void updateMenu(int position, boolean isChecked) {
            // If this is an unselection, don't bother checking the others.
            // Otherwise, check them.
            ListMenu.SceneItem entity = scenes.get(position);
            if (isChecked) {
                entity.setSelected(false);
            } else {
                entity.setSelected(true);
                for (ListMenu.SceneItem item : scenes) {
                    if (!item.equals(entity) && item.isSelected()) {
                        item.setSelected(false);
                    }
                }
            }
        }
    }

    public interface SceneOptionsListener {
        void onSceneSelected(String sceneName);
    }
}
