package com.entranet.housemate.housemateapp.api.guest;

import com.entranet.housemate.housemateapp.api.AdminApi;
import com.entranet.housemate.housemateapp.data.helpers.Constants;
import com.squareup.moshi.Json;

/**
 * API body request for {@link AdminApi#deleteGuest}
 */

public class DeleteGuestBody {
    @Json(name = "token")
    private String token;
    @Json(name = "buildingID")
    private String buildingID;
    @Json(name = "origin")
    private final int ORIGIN = Constants.ORIGIN_HOUSEMATE_APP;
    @Json(name = "guestEmail")
    private String email;

    public DeleteGuestBody(String email) { this.email = email; }

    public String getToken() { return token; }

    public void setToken(String token) { this.token = token; }

    public String getBuildingID() { return buildingID; }

    public void setBuildingID(String buildingID) { this.buildingID = buildingID; }
}
