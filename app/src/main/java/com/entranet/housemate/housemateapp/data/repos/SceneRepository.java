package com.entranet.housemate.housemateapp.data.repos;

import android.util.Log;

import com.entranet.housemate.housemateapp.BuildConfig;
import com.entranet.housemate.housemateapp.api.ApiResponse;
import com.entranet.housemate.housemateapp.api.HomeApi;
import com.entranet.housemate.housemateapp.api.scene.ActivateSceneBody;
import com.entranet.housemate.housemateapp.api.scene.DeleteSceneBody;
import com.entranet.housemate.housemateapp.api.scene.InsertSceneBody;
import com.entranet.housemate.housemateapp.data.AppExecutors;
import com.entranet.housemate.housemateapp.data.db.dao.SceneDao;
import com.entranet.housemate.housemateapp.data.db.entity.SceneEntity;
import com.entranet.housemate.housemateapp.data.db.entity.SwitchEntity;
import com.entranet.housemate.housemateapp.data.helpers.Constants;
import com.entranet.housemate.housemateapp.ui.main.smarthome.scenes.SaveSceneCallback;
import com.entranet.housemate.housemateapp.ui.main.smarthome.scenes.SceneActionsCallback;

import java.io.IOException;
import java.util.List;

import retrofit2.Call;
import retrofit2.Response;

/**
 * Repository class for accessing storage on the {@link SceneEntity} operations.
 */

public class SceneRepository {

    private static final String TAG = SceneRepository.class.getSimpleName();
    private final HomeApi homeApi;
    private final SceneDao sceneDao;
    private final AppExecutors executors;

    public SceneRepository(HomeApi homeApi, SceneDao sceneDao, AppExecutors executors) {
        this.homeApi = homeApi;
        this.sceneDao = sceneDao;
        this.executors = executors;
    }

    public void add(List<SceneEntity> scenes, String buildingID) {
        executors.getDiskIO().execute(() -> {
            // Set the building ID and save.
            for (SceneEntity scene: scenes) scene.setBuildingId(buildingID);
            sceneDao.insertAllScenes(scenes);
        });
    }

    public SceneEntity getByName(String sceneName, String currentBuildingId) {
        return sceneDao.getSceneByName(sceneName, currentBuildingId);
    }

    public void load(String currentBuildingId, SceneActionsCallback callback) {
        executors.getDiskIO().execute(() -> {
            List<String> scenes = sceneDao.getSceneNames(currentBuildingId);
            callback.onScenesLoaded(scenes);
        });
    }

    public void deleteAll(String token, String currentBuildingId) {
        executors.getDiskIO().execute(() -> {
            List<String> sceneNames = sceneDao.getSceneNames(currentBuildingId);
            for (String sceneName : sceneNames) {
                delete(sceneName, token, currentBuildingId);
            }
        });
    }

    public void delete(String sceneName, String token, String currentBuildingId) {
        // First remove locally.
        deleteFromDb(sceneName);
        // Then, update the server.
        deleteFromApi(sceneName, token, currentBuildingId);
    }

    public void activate(String sceneName, String token, String buildingId, SceneActionsCallback callback) {
        executors.getDiskIO().execute(() -> {
            try {
                ActivateSceneBody body = new ActivateSceneBody(sceneName);
                body.setBuildingID(buildingId);
                body.setToken(token);

                Call<ApiResponse> call = homeApi.activateScene(body);
                Response<ApiResponse> response = call.execute();
                if (response != null) {
                    ApiResponse sceneResponse = response.body();
                    if (sceneResponse != null) {
                        if (sceneResponse.getState() == Constants.API_RESPONSE_STATE_SUCCESSFUL) {
                            Log.d(TAG, "Scene activated.");
                            callback.onShowSceneActivated(sceneName);
                            return;
                        } else {
                            Log.d(TAG, "Error description: " + sceneResponse.getDescription());
                        }
                    }
                }
                Log.e(TAG, "Scene failed to activate");
                callback.onShowSceneActivationFailed(sceneName);
            } catch (IOException e) {
                Log.e(TAG, "Scene failed to activate. Exception: " + e.getMessage());
                callback.onShowSceneActivationFailed(sceneName);
            }
        });
    }

    public void saveScene(String sceneName,
                          List<SwitchEntity> sceneList,
                          String mode,
                          String token,
                          String buildingId,
                          SaveSceneCallback callback) {
        // Save the scene to the local database.
        executors.getDiskIO().execute(() -> {
            if (Constants.MODE_EDIT.equals(mode)) {
                // If we are in edit mode we should first delete the previous one.
                // There is currently no other way (for the API at least).
                // For the database it doesn't matter since it will be replaced.
                deleteFromApi(sceneName, token, buildingId);
            }

            SceneEntity scene = new SceneEntity();
            scene.setName(sceneName);
            scene.setSwitches(sceneList);
            scene.setBuildingId(buildingId);
            // Issue the database transaction
            sceneDao.insertScene(scene);

            // Update the server.
            // Right now scene insertion requires one request per scene part.
            // The common identifier is the name.
            try {
                for (SwitchEntity scenePart : sceneList) {
                    InsertSceneBody body = new InsertSceneBody(sceneName,
                            scenePart.getSwitchId(),
                            scenePart.getState(),
                            scenePart.getExtra());
                    body.setToken(token);
                    body.setBuildingID(buildingId);

                    Response<ApiResponse> response = homeApi.insertScene(body).execute();
                    if (response.isSuccessful()) {
                        ApiResponse sceneResponse = response.body();
                        if (sceneResponse != null) {
                            if (sceneResponse.getState() == Constants.API_RESPONSE_STATE_SUCCESSFUL) {
                                if (BuildConfig.DEBUG) Log.d(TAG, "Scene insertion part success");
                                // Route action to callback.
                                callback.onSaveSceneSuccess(sceneName);
                            } else {
                                Log.e(TAG, "Insert scene request failed. Error description: " + sceneResponse.getDescription());
                            }
                        } else {
                            Log.e(TAG, "Insert scene response body is null");
                        }
                    } else {
                        Log.e(TAG, "Insert scene request for " + sceneName + " failed");
                    }
                }
            } catch (IOException e) {
                Log.e(TAG, "Exception raised: " + e.getMessage());
                callback.onSaveSceneFailure();
            }
        });
    }

    public List<SceneEntity> get(String currentBuildingId) {
        return sceneDao.getScenes(currentBuildingId);
    }

    private void deleteFromApi(String sceneName, String token, String currentBuildingId) {
        try {
            DeleteSceneBody body = new DeleteSceneBody(sceneName);
            body.setBuildingID(currentBuildingId);
            body.setToken(token);

            Response<ApiResponse> response = homeApi.deleteScene(body).execute();
            if (response.isSuccessful()) {
                ApiResponse apiResponse = response.body();
                if (apiResponse != null) {
                    if (apiResponse.getState() == Constants.API_RESPONSE_STATE_SUCCESSFUL) {
                        if (BuildConfig.DEBUG) Log.d(TAG, "Scene removed");
                    } else {
                        Log.e(TAG, "Error description: " + apiResponse.getDescription());
                    }
                } else {
                    Log.e(TAG, "Response body null on scene removal");
                }
            } else {
                Log.e(TAG, "Scene deletion failed");
            }
        } catch (IOException e) {
            Log.e(TAG, "Exception raised: " + e.getMessage());
        }
    }

    private void deleteFromDb(String sceneName) { sceneDao.deleteSceneByName(sceneName); }

    public void truncate() { executors.getDiskIO().execute(sceneDao::deleteAll); }
}
