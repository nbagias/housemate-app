package com.entranet.housemate.housemateapp.ui.main.smarthome.scenes;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.TextInputEditText;
import android.support.design.widget.TextInputLayout;
import android.support.v4.app.DialogFragment;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.TextView;

import com.entranet.housemate.housemateapp.R;
import com.entranet.housemate.housemateapp.data.DataRepository;
import com.entranet.housemate.housemateapp.data.db.entity.SwitchEntity;
import com.entranet.housemate.housemateapp.data.helpers.Constants;
import com.entranet.housemate.housemateapp.data.helpers.InputHelper;
import com.entranet.housemate.housemateapp.ui.main.smarthome.scenes.custom.SelectSwitchesAdapter;

import java.util.List;

/**
 *
 */
public class SceneEditorFragment extends DialogFragment implements LoadSwitchesCallback, SaveSceneCallback {

    private static final String TAG = SceneEditorFragment.class.getSimpleName();
    private static final String ARG_FRAGMENT_MODE = "fragmentMode";
    private static final String ARG_FRAGMENT_SCENE = "sceneName";

    private OnSaveNewSceneListener listener;

    private DataRepository repository ;
    private boolean isThermostatSelected;
    private String mode;
    private String sceneName;

    // UI elements
    private RecyclerView recyclerSwitches;
    private TextInputEditText edtSceneName;
    private TextInputLayout edtSceneNameLayout;
    private TextView txtThermostat;
    private TextView lblThermostat;

    public SceneEditorFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of this fragment.
     *
     * @return A new instance of fragment SceneEditorFragment.
     */
    @NonNull
    public static SceneEditorFragment newInstance(String mode, @Nullable String name) {
        SceneEditorFragment fragment = new SceneEditorFragment();
        Bundle arg = new Bundle();
        arg.putString(ARG_FRAGMENT_MODE, mode);
        if (name != null) arg.putString(ARG_FRAGMENT_SCENE, name);
        fragment.setArguments(arg);
        return fragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Bundle arg = getArguments();
        if (arg != null) {
            mode = arg.getString(ARG_FRAGMENT_MODE);
            if (arg.containsKey(ARG_FRAGMENT_SCENE)) {
                sceneName = arg.getString(ARG_FRAGMENT_SCENE);
            }
        } else {
            // Default to add
            mode = Constants.MODE_CREATE;
        }
        // Get a reference to repository.
        repository = DataRepository.getInstance(getActivity().getApplicationContext());
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_dialog_scene_editor, container, false);

        Button btnSave = view.findViewById(R.id.scene_editor_button_save);
        btnSave.setOnClickListener(clickListener);
        lblThermostat = view.findViewById(R.id.scene_editor_thermostat_label);
        lblThermostat.setOnClickListener(clickListener);
        ImageButton btnIncrease = view.findViewById(R.id.scene_editor_thermostat_increase);
        btnIncrease.setOnClickListener(clickListener);
        ImageButton btnDecrease = view.findViewById(R.id.scene_editor_thermostat_decrease);
        btnDecrease.setOnClickListener(clickListener);

        edtSceneName = view.findViewById(R.id.scene_editor_edit_text);
        edtSceneNameLayout = view.findViewById(R.id.scene_editor_input_layout);

        recyclerSwitches = view.findViewById(R.id.scene_editor_recycler);
        recyclerSwitches.setLayoutManager(new LinearLayoutManager(getActivity()));
        recyclerSwitches.setHasFixedSize(true);
        recyclerSwitches.addItemDecoration(new DividerItemDecoration(getActivity(), DividerItemDecoration.VERTICAL));

        txtThermostat = view.findViewById(R.id.scene_editor_thermostat_value);
        if (mode.equals(Constants.MODE_EDIT)) {
            // Set the scene's name as text.
            edtSceneName.setText(sceneName);
            // Load asynchronously all the switches, but in the scene's state.
            repository.getSwitchesForScene(sceneName, this);
        } else {
            // Thermostat starts selected.
            isThermostatSelected = true;
            lblThermostat.setCompoundDrawablesWithIntrinsicBounds(R.drawable.dot_shape, 0, 0, 0);
            // Load asynchronously all the switches.
            repository.getSwitches(this);
        }
        return view;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnSaveNewSceneListener) {
            listener = (OnSaveNewSceneListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnSaveNewSceneListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        listener = null;
    }

    @Override
    public void onLoadSwitchesSuccess(final List<SwitchEntity> switchesList) {
        SelectSwitchesAdapter adapter = new SelectSwitchesAdapter(switchesList);
        recyclerSwitches.setAdapter(adapter);
    }

    @Override
    public void onSetThermostatValue(int value, boolean hasThermostatPart) {
        // Display thermostat value.
        txtThermostat.setText(String.valueOf(value));
        // Decide how to display the thermostat.
        isThermostatSelected = hasThermostatPart;
        if (isThermostatSelected) {
            lblThermostat.setCompoundDrawablesWithIntrinsicBounds(R.drawable.dot_shape, 0, 0, 0);
        } else {
            lblThermostat.setCompoundDrawablesWithIntrinsicBounds(R.drawable.dot_shape_white, 0, 0, 0);
        }
    }

    @Override
    public void onSetThermostatDisabled() {
        // Disable all interaction with the thermostat.
        isThermostatSelected = false;
        lblThermostat.setCompoundDrawablesWithIntrinsicBounds(R.drawable.dot_shape_white, 0, 0, 0);
        lblThermostat.setEnabled(false);
        txtThermostat.setEnabled(false);
    }

    @Override
    public void onLoadSwitchesFailure() { Log.e(TAG, "Could not load switches"); }

    @Override
    public void onRefreshStarted() {

    }

    @Override
    public void onRefreshSuccess() {

    }

    @Override
    public void onRefreshFailure() {

    }


    private View.OnClickListener clickListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            switch (v.getId()) {
                case R.id.scene_editor_button_save:
                    startSavingScene();
                    break;
                case R.id.scene_editor_thermostat_label:
                    isThermostatSelected = !isThermostatSelected;
                    if (isThermostatSelected) {
                        lblThermostat.setCompoundDrawablesWithIntrinsicBounds(R.drawable.dot_shape, 0, 0, 0);
                    } else {
                        lblThermostat.setCompoundDrawablesWithIntrinsicBounds(R.drawable.dot_shape_white, 0, 0, 0);
                    }
                    break;
                case R.id.scene_editor_thermostat_decrease:
                    if (isThermostatSelected) {
                        int currentValue = Integer.parseInt(txtThermostat.getText().toString());
                        if (currentValue == Constants.THERMOSTAT_MIN_VALUE) return;
                        // Decrease by one.
                        currentValue--;
                        txtThermostat.setText(String.valueOf(currentValue));
                    }
                    break;
                case R.id.scene_editor_thermostat_increase:
                    if (isThermostatSelected) {
                        int currentValue = Integer.parseInt(txtThermostat.getText().toString());
                        if (currentValue == Constants.THERMOSTAT_MAX_VALUE) return;
                        // Increase by one.
                        currentValue++;
                        txtThermostat.setText(String.valueOf(currentValue));
                    }
                    break;
                default:
                    break;
            }
        }
    };

    private void startSavingScene() { new Thread(this::saveScene).start(); }

    private void saveScene() {
        String title = edtSceneName.getText().toString().trim();
        if (InputHelper.isValidSceneName(title)) {
            // Get the adapter.
            SelectSwitchesAdapter adapter = (SelectSwitchesAdapter) recyclerSwitches.getAdapter();
            // Now get all the selected devices.
            List<SwitchEntity> selectedList = adapter.getSelectedList();
            // Add the thermostat if selected.
            if (isThermostatSelected) {
                SwitchEntity sceneThermostat = repository.getThermostat();
                sceneThermostat.setExtra(txtThermostat.getText().toString());
                selectedList.add(sceneThermostat);

            }
            // Check for emptiness.
            if (selectedList.isEmpty()) {
                // Set error about list emptiness.
                edtSceneNameLayout.post(() -> edtSceneNameLayout.setError(getString(R.string.add_scene_empty_list)));
                return;
            }
            // Save the new scene.
            repository.saveScene(title, selectedList, mode, this);
        } else {
            // Set error about name emptiness.
            edtSceneNameLayout.post(() -> {
                edtSceneNameLayout.setError(getString(R.string.add_scene_empty_name));
                edtSceneNameLayout.requestFocus();
            });
        }
    }
    @Override
    public void onSaveSceneSuccess(String sceneName) {
        if (listener != null) {
            listener.onSaveSceneFinished(sceneName);
        }
        dismiss();
    }

    @Override
    public void onSaveSceneFailure() {
        if (listener != null) {
            listener.onSaveSceneError();
        }
        dismiss();
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     */
    public interface OnSaveNewSceneListener {
        void onSaveSceneFinished(String sceneName);

        void onSaveSceneError();
    }
}
