package com.entranet.housemate.housemateapp.ui.main.smarthome.scenes;

import com.entranet.housemate.housemateapp.data.db.entity.SwitchEntity;

import java.util.List;

/**
 * Callback interface for loading the switch list asynchronously.
 */

public interface LoadSwitchesCallback {
    void onLoadSwitchesSuccess(List<SwitchEntity> switchesList);

    void onSetThermostatValue(int value, boolean hasThermostatPart);

    void onSetThermostatDisabled();

    void onLoadSwitchesFailure();

    void onRefreshStarted();

    void onRefreshSuccess();

    void onRefreshFailure();
}
