package com.entranet.housemate.housemateapp.api.building;

/**
 * Interface for routing actions based on the get buildings response.
 */

public interface BuildingResponseCallback {

    void onGetBuildingsSuccess();

    void onGetBuildingsFailure(int errorId);
}
