package com.entranet.housemate.housemateapp.ui.main.smarthome.scenes;

/**
 *
 */

public interface SaveSceneCallback {

    void onSaveSceneSuccess(String sceneName);

    void onSaveSceneFailure();
}
