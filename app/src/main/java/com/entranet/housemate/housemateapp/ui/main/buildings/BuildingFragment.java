package com.entranet.housemate.housemateapp.ui.main.buildings;

import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.TextInputEditText;
import android.support.design.widget.TextInputLayout;
import android.support.v4.app.DialogFragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ProgressBar;

import com.entranet.housemate.housemateapp.R;
import com.entranet.housemate.housemateapp.data.DataRepository;
import com.entranet.housemate.housemateapp.data.helpers.InputHelper;

/**
 * BuildingFragment acts as a dialog for prompting user to enter his building ID and name.
 *
 */

public class BuildingFragment extends DialogFragment implements AddBuildingCallback {

    private TextInputEditText txtBuildingId;
    private TextInputEditText txtBuildingName;
    private TextInputLayout inputLayout;
    private ProgressBar progressBar;
    private Button btnSubmit;
    private DataRepository repository;

    @NonNull
    public static BuildingFragment newInstance() {
        return new BuildingFragment();
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        // Get a reference to repository.
        repository = DataRepository.getInstance(getActivity().getApplicationContext());
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_dialog_building, container, false);
        // Get UI references.
        txtBuildingId = view.findViewById(R.id.building_id_edittext);
        txtBuildingName = view.findViewById(R.id.building_name_edittext);
        inputLayout = view.findViewById(R.id.building_id_container);
        progressBar = view.findViewById(R.id.building_progress_bar);
        btnSubmit = view.findViewById(R.id.building_submit);
        // Add listener.
        btnSubmit.setOnClickListener(buildingIdListener);

        return view;
    }

    @Override
    public void onAddBuildingSuccess() {
        // Refresh the buildings, set the callback to the parent activity.
        repository.getBuildingsFromDb((GetBuildingsDbCallback) getActivity());
        // Exit.
        dismiss();
    }

    @Override
    public void onAddBuildingFailure(final String message) {
        Handler handler = new Handler(Looper.getMainLooper());
        handler.post(() -> {
            // Display error
            if (inputLayout != null) {
                inputLayout.setError(message);
            }
            // Hide progress bar.
            progressBar.setVisibility(View.INVISIBLE);
            // Re-enable button.
            btnSubmit.setEnabled(true);
        });
    }

    private final View.OnClickListener buildingIdListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            String buildingId = txtBuildingId.getText().toString().trim();
            if (InputHelper.isValidBuildingId(buildingId)) {
                // Get the name, don't perform any checks, since it is optional.
                String buildingName = txtBuildingName.getText().toString().trim();
                // Disable the submit button.
                btnSubmit.setEnabled(false);
                // Show progress.
                progressBar.setVisibility(View.VISIBLE);
                // Make the request.
                DataRepository repo = DataRepository.getInstance(getActivity().getApplicationContext());
                repo.addUserToBuilding(buildingId, buildingName, BuildingFragment.this);
            } else {
                inputLayout.setError(getString(R.string.building_id_input_error));
            }
        }
    };
}
