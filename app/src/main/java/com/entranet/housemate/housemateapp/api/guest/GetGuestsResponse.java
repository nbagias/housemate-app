package com.entranet.housemate.housemateapp.api.guest;

import com.entranet.housemate.housemateapp.api.AdminApi;
import com.entranet.housemate.housemateapp.data.db.entity.GuestEntity;
import com.squareup.moshi.Json;

import java.util.List;

/**
 * API response for {@link AdminApi#getGuests}}
 */

public class GetGuestsResponse {
    @Json(name = "state")
    private Integer state;
    @Json(name = "description")
    private String description;
    @Json(name = "data")
    private List<GuestEntity> data;

    public Integer getState() {
        return state;
    }

    public void setState(Integer state) {
        this.state = state;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public void setData(List<GuestEntity> data) {
        this.data = data;
    }

    public List<GuestEntity> getData() {
        return data;
    }
}
