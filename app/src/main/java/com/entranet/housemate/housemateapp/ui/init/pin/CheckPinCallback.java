package com.entranet.housemate.housemateapp.ui.init.pin;

/**
 * Interface for pin-related operations.
 */

public interface CheckPinCallback {

    /**
     * Runs when the user has set a pin.
     */
    void onUserHasPin();

    /**
     * Runs when the user hasn't set a pin (i.e first time login).
     */
    void onUserHasNoPin();

    /**
     * Runs when the user enters a wrong pin.
     */
    void onPinWrong();

    /**
     * Runs when the user enters the valid pin.
     */
    void onPinRight();
}
