package com.entranet.housemate.housemateapp.api.scene;

import com.squareup.moshi.Json;

/**
 * Class for modelling the API response when requesting the list of scenes.
 */

public class GetScenesResponse {
    @Json(name = "state")
    private Integer state;
    @Json(name = "description")
    private String description;
    @Json(name = "data")
    private ScenesData data;

    public Integer getState() { return state; }

    public void setState(Integer state) { this.state = state; }

    public String getDescription() { return description; }

    public void setDescription(String description) { this.description = description; }

    public ScenesData getData() { return data; }

    public void setData(ScenesData data) { this.data = data; }
}
